#!/usr/bin/env python

from distutils.core import setup

from iquaview_lib import __version__, PLUGIN_NAME

setup(name=PLUGIN_NAME,
      version=__version__,
      description='IQUAview lib',
      author='Iqua Robotics',
      author_email='software@iquarobotics.com',
      packages=[PLUGIN_NAME,
                '{}.auv_configs'.format(PLUGIN_NAME),
                '{}.baseclasses'.format(PLUGIN_NAME),
                '{}.canvasdrawer'.format(PLUGIN_NAME),
                '{}.cola2api'.format(PLUGIN_NAME),
                '{}.connection'.format(PLUGIN_NAME),
                '{}.connection.settings'.format(PLUGIN_NAME),
                '{}.connection.sftp'.format(PLUGIN_NAME),
                '{}.entity'.format(PLUGIN_NAME),
                '{}.ui'.format(PLUGIN_NAME),
                '{}.utils'.format(PLUGIN_NAME),
                '{}.vehicle'.format(PLUGIN_NAME),
                '{}.vessel'.format(PLUGIN_NAME),
                '{}.xmlconfighandler'.format(PLUGIN_NAME),
                '{}.xmlconfighandler.updaters'.format(PLUGIN_NAME)],
      include_package_data=True,
      package_data={PLUGIN_NAME: ['auv_configs/*.xml']},
      install_requires=[
          'lxml',
          'pyopenssl>=24.0.0',
          'paramiko>=2.7.2',
          'pysftp',
          'dataclasses'
      ],

      )
