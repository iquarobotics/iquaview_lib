# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


"""
 Helper classes to get a valid xml file
"""

import os.path
from copy import copy

from PyQt5.QtCore import QFileInfo
from PyQt5.QtWidgets import QDialog, QMessageBox, QFileDialog

from iquaview_lib.config import Config
from iquaview_lib.ui.ui_auvconfigxml import Ui_AUVconfigDialog
from iquaview_lib.xmlconfighandler.updaters.update_to_2_0 import update_to_2_0
from iquaview_lib.xmlconfighandler.xmlconfigparser import XMLConfigParser

VERSION_ORDER = ['1.0', '2.0']


class ConfigCheckXML(QDialog, Ui_AUVconfigDialog):
    def __init__(self, config: Config):
        """
        Constructor
        :param config: configuration
        :type config: Config
        """
        super().__init__()
        self.setupUi(self)
        self.config = config
        if config.settings['last_auv_config_xml'] is not None:
            self.config_filename = config.settings['last_auv_config_xml']
        else:
            self.config_filename = ""
        self.auv_config_lineEdit.setText(config.settings['last_auv_config_xml'])

        self.auv_config_pushButton.clicked.connect(self.load_auv_config)
        self.buttonBox.accepted.connect(self.on_accept)

        self.check_xml()

    def check_xml(self):
        """
        Check if xml exist and updates to newer version if needed
        """
        if self.exists():
            custom_xml_filename = f"{self.resolve_filename(self.config.settings['last_auv_config_xml'])}"
            version = self.get_xml_version(custom_xml_filename)
            if version is None:
                version = '1.0'

            position = VERSION_ORDER.index(version)
            while position < (len(VERSION_ORDER) - 1):
                self.convert_to_newer_version(custom_xml_filename, VERSION_ORDER[position + 1])
                position += 1

    @staticmethod
    def convert_to_newer_version(xml_filename, new_version):
        """
        Convert xml from old version to a newer version
        :param xml_filename: xml filename
        :type xml_filename: str
        :param new_version: new version number
        :type new_version: str
        """
        if new_version == '2.0':
            update_to_2_0(xml_filename)

    @staticmethod
    def get_xml_version(xml_filename):
        """
        Returns xml version. In case it does not exist returns None
        :param xml_filename: xml filename
        :type xml_filename: str
        :return: Returns xml version
        :rtype: str/None
        """
        config_parser = XMLConfigParser(xml_filename)
        # get version
        version_element = config_parser.first_match(config_parser.root, "version")
        if version_element is not None:
            version = version_element.text
        else:
            version = version_element
        return version

    def exists(self):
        """Check if config file exists."""
        exists = os.path.exists(self.resolve_filename(self.config_filename))
        return exists

    @staticmethod
    def resolve_filename(filename):
        """
        Resolve filename
        :param filename: file to resolve
        :type filename: str
        :return: filename resolved
        :rtype: str
        """
        return os.path.abspath(os.path.expanduser(filename))

    def load_auv_config(self):
        """ Open dialog to load an auv config file."""
        configuration_filename, __ = QFileDialog.getOpenFileName(self, 'AUV configuration',
                                                                 self.resolve_filename(
                                                                     self.config.settings['last_auv_config_xml']),
                                                                 "AUV configuration(*.xml) ;; All files (*.*)")
        if configuration_filename:
            file_info = QFileInfo(configuration_filename)
            filename = file_info.canonicalFilePath()

            self.auv_config_lineEdit.setText(str(filename))

    def on_accept(self):
        """ On accept config check xml"""
        exists = os.path.exists(self.resolve_filename(self.auv_config_lineEdit.text()))
        if exists:

            # update config
            self.config.settings['last_auv_config_xml'] = self.auv_config_lineEdit.text()
            self.config.settings['last_auv_config_xml'] = copy(self.config.settings['last_auv_config_xml'])

            self.config.save()
            self.accept()

        else:
            QMessageBox.critical(self.parent(),
                                 "AUV configuration XML Error",
                                 "AUV configuration: " + self.auv_config_lineEdit.text() + " no exist.",
                                 QMessageBox.Close)
