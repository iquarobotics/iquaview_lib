# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


"""
Helper for parsing,reading and writing config xml files
"""

from lxml import etree


class XMLConfigParser:
    def __init__(self, config_file_name, rm_comments=True):
        """
        Constructor
        :param config_file_name: configuration filename
        :type config_file_name: str
        :param rm_comments: discard commments
        :type rm_comments: bool
        """
        self.config_name = config_file_name
        # parser to remove comments
        parser = etree.XMLParser(remove_blank_text=True, remove_comments=rm_comments)
        # tree
        self.tree = etree.parse(config_file_name, parser=parser)
        self.root = self.tree.getroot()

    def first_match(self, branch, match_name):
        """
        returns only the first match
        :param branch: branch where search
        :type branch: etree.Element
        :param match_name: name to find
        :type match_name: str
        :return: returns first match in the branch
        :rtype: xml.etree.ElementTree.Element
        """
        return branch.find(match_name)

    def all_matches(self, branch, match_name):
        """
        returns a list of matching Elements
        :param branch: branch where search
        :type branch: etree.Element
        :param match_name: name to find
        :type match_name: str
        :return: returns all matches in the branch
        :rtype: list
        """
        return branch.findall(match_name)

    def write(self):
        """
        write self.tree on XML config file
        """
        self.tree.write(self.config_name, xml_declaration=True, encoding='UTF-8', pretty_print=True)
