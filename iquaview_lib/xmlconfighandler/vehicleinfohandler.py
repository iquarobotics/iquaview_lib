# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


"""
 Helper classes to read the xml structure associated to vehicle info in the AUV config file
"""

from iquaview_lib.xmlconfighandler.xmlconfigparser import XMLConfigParser


class VehicleInfoHandler:
    def __init__(self, xml_filename):
        """
        Constructor
        :param xml_filename: xml filename
        :type xml_filename: str
        """

        # last auv config xml
        self.configParser = XMLConfigParser(xml_filename)

    def read_configuration(self):
        """

        get Vehicle Info
        :return: returns first match in the branch
        :rtype: etree.Element
        """
        xml_vehicle_info = self.configParser.first_match(self.configParser.root, "vehicle_info")

        return xml_vehicle_info

    def write(self):
        """write vehicle info on XML"""
        self.configParser.write()
