# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


import logging
import os.path
from copy import copy

from lxml import etree

from iquaview_lib.xmlconfighandler.checklisthandler import CheckListHandler
from iquaview_lib.xmlconfighandler.missionactionshandler import MissionActionsHandler
from iquaview_lib.xmlconfighandler.rosparamsreader import RosParamsReader
from iquaview_lib.xmlconfighandler.vehicledatahandler import VehicleDataHandler
from iquaview_lib.xmlconfighandler.vehicleinfohandler import VehicleInfoHandler
from iquaview_lib.xmlconfighandler.xmlconfigparser import XMLConfigParser

logger = logging.getLogger(__name__)


def update_to_2_0(xml_filename):
    """
    Update auv configuration xml to version 2.0
    :param xml_filename:  current xml filename
    :return:
    """
    vi_reader = VehicleInfoHandler(xml_filename)
    custom_configuration = vi_reader.read_configuration()
    vehicle_type = vi_reader.configParser.first_match(custom_configuration, 'vehicle_type')
    default_xml_filename = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    if vehicle_type.text == 'sparus2':
        default_xml_filename = default_xml_filename + '/auv_configs/sparus2_configuration.xml'
    else:
        default_xml_filename = default_xml_filename + '/auv_configs/girona500_configuration.xml'

    update_vehicle_info(default_xml_filename, xml_filename)
    update_vehicle_data(default_xml_filename, xml_filename)
    update_ros_params(default_xml_filename, xml_filename)
    update_mission_actions(default_xml_filename, xml_filename)
    update_check_lists(default_xml_filename, xml_filename)

    config_parser = XMLConfigParser(xml_filename)
    # get version
    version_element = config_parser.first_match(config_parser.root, "version")
    if version_element is None:
        version_element = etree.SubElement(config_parser.root, "version")
        version_element.text = '2.0'

        config_parser.write()


def update_vehicle_info(default_xml_filename, xml_filename):
    """
    Update vehicle info
    :param default_xml_filename: default xml filename
    :type default_xml_filename: str
    :param xml_filename: custom xml filename
    :type xml_filename: str
    """
    vi_reader = VehicleInfoHandler(xml_filename)
    custom_configuration = vi_reader.read_configuration()

    default_vi_reader = VehicleInfoHandler(default_xml_filename)
    default_configuration = default_vi_reader.read_configuration()

    # remove old tags
    remove_tag(custom_configuration, vi_reader, 'vehicle_code')
    # add new tags
    check_and_add_new_elements(default_configuration, custom_configuration, vi_reader)


def update_vehicle_data(default_xml_filename, xml_filename):
    """
    Update vehicle data
    :param default_xml_filename: default xml filename
    :type default_xml_filename: str
    :param xml_filename: custom xml filename
    :type xml_filename: str
    """

    # vehicle data
    default_vehicledata_reader = VehicleDataHandler(default_xml_filename)
    custom_vehicledata_reader = VehicleDataHandler(xml_filename)

    default_vd_topics = default_vehicledata_reader.read_topics()
    default_vd_services = default_vehicledata_reader.read_services()
    default_vd_cs_launch_list = default_vehicledata_reader.read_controlstation_launch_list()
    default_vd_v_launch_list = default_vehicledata_reader.read_vehicle_launch_list()

    custom_vd_topics = custom_vehicledata_reader.read_topics()
    custom_vd_services = custom_vehicledata_reader.read_services()
    custom_vd_cs_launch_list = custom_vehicledata_reader.read_controlstation_launch_list()
    custom_vd_v_launch_list = custom_vehicledata_reader.read_vehicle_launch_list()

    remove_tag(custom_vd_topics, custom_vehicledata_reader, 'topic', 'watchdog')
    remove_tag(custom_vd_topics, custom_vehicledata_reader, 'topic', 'vehicle_status')
    remove_tag(custom_vd_topics, custom_vehicledata_reader, 'topic', 'goto status')
    remove_tag(custom_vd_topics, custom_vehicledata_reader, 'topic', 'thruster setpoints')

    remove_tag(custom_vd_services, custom_vehicledata_reader, 'service', 'reset timeout')

    check_and_add_new_elements(default_vd_topics, custom_vd_topics, custom_vehicledata_reader, True)
    check_and_add_new_elements(default_vd_services, custom_vd_services, custom_vehicledata_reader, True)
    check_and_add_new_elements(default_vd_cs_launch_list, custom_vd_cs_launch_list, custom_vehicledata_reader, True)
    check_and_add_new_elements(default_vd_v_launch_list, custom_vd_v_launch_list, custom_vehicledata_reader, True)


def update_ros_params(default_xml_filename, xml_filename):
    """
    Update ros params
    :param default_xml_filename: default xml filename
    :type default_xml_filename: str
    :param xml_filename: custom xml filename
    :type xml_filename: str
    """
    default_ros_param_reader = RosParamsReader(default_xml_filename, None, None, None)
    custom_ros_param_reader = RosParamsReader(xml_filename, None, None, None)

    default_section_list = default_ros_param_reader.read_configuration(get_ros_param=False)
    custom_section_list = custom_ros_param_reader.read_configuration(get_ros_param=False)

    configParser = XMLConfigParser(xml_filename)
    ros_params = configParser.first_match(configParser.root, "ros_params")
    first_section = configParser.first_match(ros_params, "section")
    action_id = configParser.first_match(first_section, "action_id")

    if action_id is not None:
        # move action_id from section to param
        for section in custom_section_list:
            for param in section.get_params():
                param.action_id = copy(action_id.text)

    params_to_remove = ["/pilot/los_cte/max_surge_velocity", "/pilot/goto/max_surge"]

    remove_ros_params(custom_section_list, params_to_remove)
    add_ros_params(default_section_list, custom_section_list)

    custom_ros_param_reader.write(xml_filename, custom_section_list)


def update_mission_actions(default_xml_filename, xml_filename):
    """
    Update ros params
    :param default_xml_filename: default xml filename
    :type default_xml_filename: str
    :param xml_filename: custom xml filename
    :type xml_filename: str
    """
    default_mission_actions_handler = MissionActionsHandler(default_xml_filename)
    custom_mission_actions_handler = MissionActionsHandler(xml_filename)

    default_actions = default_mission_actions_handler.get_actions()
    custom_actions = custom_mission_actions_handler.get_actions()

    for default_action in default_actions:
        found_action = False
        for custom_action in custom_actions:
            # identify
            default_action_id = default_mission_actions_handler.config_parser.first_match(default_action, 'action_id')
            custom_action_id = default_mission_actions_handler.config_parser.first_match(custom_action, 'action_id')
            if default_action_id.text == custom_action_id.text:
                found_action = True

        if not found_action:
            default_action_name = default_mission_actions_handler.config_parser.first_match(default_action,
                                                                                            'action_name')
            default_action_id = default_mission_actions_handler.config_parser.first_match(default_action,
                                                                                          'action_description')
            default_action_description = default_mission_actions_handler.config_parser.first_match(default_action,
                                                                                                   'action_description')

            mission_actions = custom_mission_actions_handler.config_parser.first_match(
                custom_mission_actions_handler.config_parser.root, "mission_actions")
            new_element = etree.SubElement(mission_actions, "action")

            action_name = etree.SubElement(new_element, "action_name")
            action_name.text = copy(default_action_name.text)

            action_id = etree.SubElement(new_element, "action_id")
            action_id.text = copy(default_action_id.text)

            action_description = etree.SubElement(new_element, "action_description")
            action_description.text = copy(default_action_description.text)

            custom_actions.append(new_element)

    custom_mission_actions_handler.write()


def update_check_lists(default_xml_filename, xml_filename):
    default_checklist_handler = CheckListHandler(default_xml_filename)
    custom_checklist_handler = CheckListHandler(xml_filename)

    default_check_lists = default_checklist_handler.get_check_lists()
    custom_check_lists = custom_checklist_handler.get_check_lists()

    for default_checklist in default_check_lists:
        found_checklist = False
        for custom_checklist in custom_check_lists:
            if default_checklist.get('id') == custom_checklist.get('id'):
                found_checklist = True

        if not found_checklist:
            # items = default_checklist_handler.get_items_from_checklist(default_checklist.get('id'))
            chk_lists = custom_checklist_handler.configParser.first_match(custom_checklist_handler.configParser.root,
                                                                          "check_lists")
            chk_lists.append(default_checklist)

    custom_checklist_handler.write()


def remove_tag(custom_configuration, handler, tag, id=None):
    """
    Remove tag from xml
    :param custom_configuration: custom configuration
    :type custom_configuration: xml.etree.ElementTree.Element
    :param handler: xml config handler
    :type handler: object
    :param tag: xml tag
    :type tag: str
    :param id: xml identifier
    :type id: str
    """

    for custom_item in custom_configuration:
        if custom_item.tag == tag:
            if id:
                if custom_item.get('id') == id:
                    custom_configuration.remove(custom_item)
                    break
            else:
                custom_configuration.remove(custom_item)
                break

    handler.write()


def remove_ros_params(custom_section_list, params_to_remove):
    """
    Remove ros params from xml
    :param custom_section_list:
    :param params_to_remove:
    :return:
    """
    i = 0
    while i < (len(custom_section_list)):
        section = custom_section_list[i]
        if section.description == 'Pilot':
            length = len(section.get_params())
            params = copy(section.get_params())
            for j in range(0, length):
                param = params[j]
                if param.field is not None:
                    # remove
                    if param.field.field_name in params_to_remove:
                        section.remove_param(param)

        if not section.get_params():
            custom_section_list.remove(section)
        else:
            i += 1


def add_ros_params(default_section_list, custom_section_list):
    """
    Add ros params to xml
    :param default_section_list: default section list
    :type default_section_list: list
    :param custom_section_list: custom section list
    :type custom_section_list: list
    :return:
    """
    # add or edit new sections/fields
    for default_section in default_section_list:
        found_section = False
        for section in custom_section_list:
            if default_section.description == section.description:
                found_section = True
                # check fields
                for default_param in default_section.get_params():
                    found_param = False
                    for param in section.get_params():
                        if param.field is not None:
                            if param.field.field_name == '/safety_supervisor/timeout':
                                param.field.field_name = '/safety_supervisor/watchdog_timer/timeout'
                            elif param.field.field_name == '/safety_supervisor/min_modem_update':
                                param.field.field_name = '/safety_supervisor/comms/modem_data_timeout'
                            elif param.field.field_name == '/safety_supervisor/min_wifi_update':
                                param.field.field_name = '/safety_supervisor/teleoperation/teleoperation_link_timeout'
                                param.description = "Teleoperation timeout (seconds)"

                        if default_param.field is not None and param.field is not None:
                            if default_param.field.field_name == param.field.field_name:
                                found_param = True
                        else:
                            if default_param.field_array.field_array_name == param.field_array.field_array_name:
                                found_param = True
                    if not found_param:
                        section.add_param(default_param)
        if not found_section:
            custom_section_list.append(default_section)


def check_and_add_new_elements(default_configuration, custom_configuration, handler, has_id=False):
    """
    Checks for elements that do not exist in the xml and adds them
    :param default_configuration: default auv configuration xml file
    :param custom_configuration: custom auv configuration xml file
    :param handler: handler of the custom file
    :param has_id: True if elements has id, False otherwise
    """
    for default_item in default_configuration:
        found = False
        for custom_item in custom_configuration:
            if default_item.tag == custom_item.tag:
                if has_id:
                    if default_item.get('id') == custom_item.get('id'):
                        found = True
                        break
                else:
                    found = True
                    break
        if not found:
            logger.info(f"{default_item.tag} not found.")
            if has_id:
                new_element = etree.SubElement(custom_configuration, default_item.tag, id=default_item.get('id'))
            else:
                new_element = etree.SubElement(custom_configuration, default_item.tag)
            new_element.text = copy(default_item.text)
            handler.write()
