# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


"""
 Helper classes to read the xml structure associated to vehicle data in the AUV config file
"""

from iquaview_lib.xmlconfighandler.xmlconfigparser import XMLConfigParser


class VehicleDataHandler:
    def __init__(self, config_filename: str):
        """
        Constructor
        :param config_filename: configuration filename
        :type config_filename: str
        """
        self.filename = config_filename
        self.configParser = XMLConfigParser(self.filename)

    def read_topics(self):
        """get Vehicle Data topics"""
        xml_vehicle_data_topics = self.configParser.first_match(self.configParser.root, "vehicle_data_topics")

        return xml_vehicle_data_topics

    def read_services(self):
        """get Vehicle Data services"""
        xml_vehicle_data_services = self.configParser.first_match(self.configParser.root, "vehicle_data_services")

        return xml_vehicle_data_services

    def read_controlstation_launch_list(self):
        """get Launch list"""
        xml_launch_list = self.configParser.first_match(self.configParser.root, "controlstation_launch_list")

        return xml_launch_list

    def read_vehicle_launch_list(self):
        """get Launch list"""
        xml_launch_list = self.configParser.first_match(self.configParser.root, "vehicle_launch_list")

        return xml_launch_list

    def write(self):
        """write vehicle info on XML"""
        self.configParser.write()
