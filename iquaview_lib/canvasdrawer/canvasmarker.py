#!/usr/bin/env python
# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


"""
 Class to define canvas markers to display positions in the map
"""

import logging
import math

from PyQt5.QtCore import Qt, QRectF, QLine, QPoint, QFile, QIODevice, QByteArray
from PyQt5.QtGui import QBrush, QPen, QPainter, QPolygonF, QPainterPath, QColor
from PyQt5.QtSvg import QSvgRenderer
from qgis.core import QgsPointXY, QgsDistanceArea, QgsProject, QgsCoordinateReferenceSystem
from qgis.gui import QgsMapCanvasItem

from iquaview_lib.canvasdrawer.canvastrackstyle import CanvasTrackStyle
from iquaview_lib.utils.calcutils import distance_plane
from iquaview_lib.utils.qgisutils import transform_point

logger = logging.getLogger(__name__)


class CanvasMarker(QgsMapCanvasItem):
    """
    Class to define canvas markers to display positions in the map
    """

    def __init__(self, canvas, style: CanvasTrackStyle, is_icon=False, orientation=True):
        """
        Init of the object CanvasMarker
        :param canvas: Canvas where the marker will be painted
        :type canvas: QgsMapCanvas
        :param configuration_track_style: Configuration options for the Style
        :type configuration_track_style: CanvasTrackStyle
        :param orientation: determines whether the marker will have orientation
        :type orientation: bool
        """

        super().__init__(canvas)
        self.canvas = canvas
        self.size = 20
        self.style = style
        self.is_icon = is_icon
        self.pointbrush = QBrush(QColor(style.color_hex_str))
        self.pointpen = QPen(Qt.black)
        self.pointpen.setWidth(1)
        self.map_pos = QgsPointXY(0.0, 0.0)
        self.heading = 0
        self.orientation = orientation
        self.svg_data = None
        self.svg_color = None
        self.distance_calc = QgsDistanceArea()
        # calculate the distance always in EPSG:4326 and ellipsoid of WGS84
        calc_crs = QgsCoordinateReferenceSystem.fromEpsgId(4326)
        self.distance_calc.setSourceCrs(calc_crs, QgsProject.instance().transformContext())
        self.distance_calc.setEllipsoid('WGS84')
        self.set_svg(style.marker_svg)

    def has_orientation(self):
        return self.orientation

    def set_marker_active(self, active):
        """
        Sets marker active or not
        :param active: Value to set marker_active
        :type active: bool
        """
        self.style.marker_active = active

    def set_svg(self, svg):
        """
        Sets svg
        :param svg: name of the svg
        :type svg: str
        """
        self.style.marker_svg = svg
        if svg is not None and svg:

            # Load the svg file
            svg_file = QFile(svg)
            svg_file.open(QIODevice.ReadOnly)
            self.svg_data = svg_file.readAll()

            # find the base color in the metadata. If it is not found, change_marker_color will have no effect.
            metadata_id = "base_color"
            pos = self.svg_data.indexOf("base_color")
            if pos != -1:
                metadata_value = self.svg_data.mid(pos + len(metadata_id) + 2, 7)
                self.svg_color = bytes(metadata_value).decode()
                self.set_color(QColor(self.style.color_hex_str))
            else:
                self.svg_color = None
        else:
            self.svg_data = None

    def set_size(self, size):
        """
        Sets the size of the marker
        :param size: Desired size of the marker (in pixels)
        :type size: int
        """
        self.size = size

    def set_color(self, color: QColor):
        """
        Sets the color of the marker. If the marker is from a svg, changing the color will only work
        if svg_color was found in the metadata of the svg when constructing this object
        :param color: Desired color of the marker
        :type color: QColor
        """
        self.style.color = QColor(color).name()

        # For markers that render with svg and can change color
        if self.style.marker_svg is not None and self.svg_color is not None:
            self.svg_data.replace(QByteArray().append(self.svg_color), QByteArray().append(color.name()))
            self.svg_color = color.name()

    def set_marker_mode(self, canvas_marker_mode):
        """
        Sets marker_mode. can be 'auto' or 'manual'
        :param canvas_marker_mode: Value to set marker_mode
        :type canvas_marker_mode: str
        """
        self.style.marker_mode = canvas_marker_mode

    def set_marker_scale(self, marker_scale):
        """
        Sets marker scale
        :param marker_scale: Value to set scale
        :type marker_scale: int
        """
        self.style.marker_scale = marker_scale

    def set_changing_scale(self):
        """
        Defines when the marker scale should change
        """
        pos = self.toCanvasCoordinates(self.map_pos)
        self.setPos(pos)
        if math.isnan(pos.x()) or math.isnan(pos.y()):
            logger.debug("Marker is not visible")
            raise ValueError("Marker is not visible")
        if self.style.marker_active:
            mode = self.style.marker_mode

            if mode == 'auto':
                self.set_size(20)
                transform = self.canvas.getCoordinateTransform()
                try:
                    start_point = transform.toMapCoordinates(pos.x(), pos.y())
                except OverflowError as overflow_error:
                    logger.debug(f"Overflow error: {overflow_error}")
                    return  # Don't draw if the position is invalid

                config_crs = QgsCoordinateReferenceSystem.fromEpsgId(4326)
                point_transformed = transform_point(
                    start_point,
                    self.canvas.mapSettings().destinationCrs(),
                    config_crs
                )

                map_end_point_width = self.distance_calc.measureLineProjected(
                    point_transformed,
                    self.style.marker_width,
                    math.radians(90 + math.degrees(self.heading))
                )[1]
                map_end_point_length = self.distance_calc.measureLineProjected(
                    point_transformed,
                    self.style.marker_length,
                    self.heading
                )[1]
                map_end_point_width_canvas_crs = transform_point(
                    map_end_point_width,
                    config_crs,
                    self.canvas.mapSettings().destinationCrs()
                )
                map_end_point_length_canvas_crs = transform_point(
                    map_end_point_length,
                    config_crs,
                    self.canvas.mapSettings().destinationCrs()
                )
                # to canvas coordinates
                canvas_end_point_width = self.toCanvasCoordinates(map_end_point_width_canvas_crs)
                canvas_end_point_length = self.toCanvasCoordinates(map_end_point_length_canvas_crs)

                canvas_start_point = QgsPointXY(self.toCanvasCoordinates(start_point))
                canvas_end_point_width_xy = QgsPointXY(canvas_end_point_width)
                canvas_end_point_length_xy = QgsPointXY(canvas_end_point_length)
                width = distance_plane(canvas_start_point.x(), canvas_start_point.y(),
                                       canvas_end_point_width_xy.x(), canvas_end_point_width_xy.y())
                height = distance_plane(canvas_start_point.x(), canvas_start_point.y(),
                                        canvas_end_point_length_xy.x(), canvas_end_point_length_xy.y())

                if width < self.size and height < self.size:
                    self.style.marker_scale = self.canvas.scale()
                else:
                    self.style.marker_scale = self.canvas.scale() * 2

        else:
            self.style.marker_scale = 400

    def draw_icon(self, painter: QPainter):
        """
        Draws the icon on the canvas
        :param painter: QPainter object
        :type painter: QPainter
        """
        self.set_size(20)
        half_size = self.size / 2.0
        rect = QRectF(0 - half_size, 0 - half_size, self.size, self.size)
        painter.setRenderHint(QPainter.Antialiasing)

        self.pointpen.setColor(Qt.black)
        self.pointpen.setWidth(2)
        self.pointbrush.setColor(QColor(self.style.color))

        painter.setBrush(self.pointbrush)
        painter.setPen(self.pointpen)
        y = 0 - half_size
        x = rect.width() / 2 - half_size
        line = QLine(x, y, x, rect.height() - half_size)
        y = rect.height() / 2 - half_size
        x = 0 - half_size
        line2 = QLine(x, y, rect.width() - half_size, y)

        # Arrow
        p = QPolygonF()
        p.append(QPoint(0 - half_size, 0))
        p.append(QPoint(0, -self.size))
        x = rect.width() - half_size
        p.append(QPoint(x, 0))
        p.append(QPoint(0, 0))

        offsetp = QPolygonF()
        offsetp.append(QPoint(0 - half_size, 0))
        offsetp.append(QPoint(0, -self.size))
        x = rect.width() - half_size
        offsetp.append(QPoint(x, 0))
        offsetp.append(QPoint(0, 0))

        painter.save()
        painter.rotate(math.degrees(self.heading) + self.canvas.rotation())
        if self.orientation:
            path = QPainterPath()
            path.addPolygon(p)
            painter.drawPath(path)
        painter.restore()
        painter.drawEllipse(rect)
        painter.drawLine(line)
        painter.drawLine(line2)

    def draw_svg(self, painter: QPainter):
        """
        Draws the svg on the canvas
        :param painter: QPainter object
        :type painter: QPainter
        """
        pos = self.toCanvasCoordinates(self.map_pos)

        # get rotation
        rotation = self.canvas.rotation()

        transform = self.canvas.getCoordinateTransform()
        start_point = transform.toMapCoordinates(pos.x(), pos.y())

        config_crs = QgsCoordinateReferenceSystem.fromEpsgId(4326)
        point_transformed = transform_point(
            start_point,
            self.canvas.mapSettings().destinationCrs(),
            config_crs
        )

        map_end_point_width = self.distance_calc.measureLineProjected(
            point_transformed,
            self.style.marker_width,
            math.radians(90 + math.degrees(self.heading))
        )[1]
        map_end_point_length = self.distance_calc.measureLineProjected(
            point_transformed,
            self.style.marker_length,
            self.heading
        )[1]
        map_end_point_width_canvas_crs = transform_point(
            map_end_point_width,
            config_crs,
            self.canvas.mapSettings().destinationCrs()
        )
        map_end_point_length_canvas_crs = transform_point(
            map_end_point_length,
            config_crs,
            self.canvas.mapSettings().destinationCrs()
        )
        # to canvas coordinates
        canvas_end_point_width = self.toCanvasCoordinates(map_end_point_width_canvas_crs)
        canvas_end_point_length = self.toCanvasCoordinates(map_end_point_length_canvas_crs)

        if self.is_icon:
            width = self.style.marker_width
            height = self.style.marker_length
        else:
            canvas_start_point = QgsPointXY(self.toCanvasCoordinates(start_point))
            canvas_end_point_width_xy = QgsPointXY(canvas_end_point_width)
            canvas_end_point_length_xy = QgsPointXY(canvas_end_point_length)
            width = distance_plane(canvas_start_point.x(), canvas_start_point.y(),
                                   canvas_end_point_width_xy.x(), canvas_end_point_width_xy.y())
            height = distance_plane(canvas_start_point.x(), canvas_start_point.y(),
                                    canvas_end_point_length_xy.x(), canvas_end_point_length_xy.y())
            if math.isnan(width) or math.isnan(height):
                raise ValueError("Width or height is NaN")

        if width > height:
            self.set_size(int(width))
        else:
            self.set_size(int(height))

        painter.save()

        if width != 0 and height != 0:
            center_x = width / 2.0
            center_y = height / 2.0
            # work out how to shift the image so that it rotates
            #           properly about its center
            # ( x cos a + y sin a - x, -x sin a + y cos a - y)
            rot_radians = math.radians(rotation + math.degrees(self.heading))
            xshift = int(((center_x * math.cos(rot_radians)) +
                          (center_y * math.sin(rot_radians)))
                         - center_x)
            yshift = int(((-center_x * math.sin(rot_radians)) +
                          (center_y * math.cos(rot_radians)))
                         - center_y)

            painter.translate(-width / 2, -height / 2)
            painter.rotate(math.degrees(self.heading) + self.canvas.rotation())
            svg_renderer = QSvgRenderer(self.svg_data)
            if svg_renderer.isValid():
                svg_renderer.render(painter, QRectF(xshift, yshift, width, height))

        painter.restore()

    def paint(self, painter, option=None, widget=None):
        """
        Overrides paint method of QgsMapCanvasItem, paints the marker to the canvas
        :param painter: QPainter object
        :type painter: QPainter
        :param option: describes the parameters needed to draw a QGraphicsItem
        :type option: QStyleOptionGraphicsItem
        :param widget: A concrete widget to paint over, None by default
        :type widget: QWidget
        """
        try:
            self.set_changing_scale()

            if not self.style.marker_active or (
                    not self.is_icon and self.canvas.scale() >= self.style.marker_scale):
                self.draw_icon(painter)
            # svg read correctly
            elif self.svg_data is not None and len(self.svg_data) > 0:
                self.draw_svg(painter)
        except ValueError as e:
            logger.error(e)

    def boundingRect(self):
        """
        Overrides boundingRect method of QgsMapCanvasItem, gives the bounding rectangle of the marker
        :return: Returns the bounding rectangle of the marker
        :rtype: QRectF
        """
        size = self.size * 2
        return QRectF(-size, -size, 2.0 * size, 2.0 * size)

    def updatePosition(self):
        """ Updates the position of the marker """
        self.set_center(self.map_pos, self.heading)

    def set_center(self, map_pos, heading=None):
        """
        Sets the position of the center of the marker
        :param map_pos: new position of the center
        :type map_pos: QgsPointXY
        :param heading: new heading of the marker
        :type heading: float (in Radians)
        """
        if heading is not None:
            self.heading = heading
        self.map_pos = map_pos
        self.setPos(self.toCanvasCoordinates(self.map_pos))

    def set_width(self, width):
        """
        Sets the width of the marker
        :param width: Desired width
        :type width: float
        """
        self.style.marker_width = width

    def set_length(self, length):
        """
        Sets the length of the marker
        :param length: Desired length
        :type length: float
        """
        self.style.marker_length = length

    def get_size(self):
        """
        Returns the size of the marker
        :return: Size of the marker
        :rtype: int
        """
        return self.size

    def get_color(self):
        """
        Returns the color of the marker
        :return: Color of the marker
        :rtype: str
        """
        return self.style.color

    def get_marker_mode(self):
        """
        Returns the marker mode
        :return: Mode of the marker
        :rtype: bool
        """
        return self.style.marker_mode

    def get_center(self):
        """
        Returns the center position of the marker
        :return: center of the marker
        :rtype: QPointF
        """
        return self.pos()

    def get_width(self):
        """
        Returns the width of the marker
        :return: width of the marker
        :rtype: float
        """
        return self.style.marker_width

    def get_length(self):
        """
        Returns the length of the marker
        :return: length of the marker
        :rtype: float
        """
        return self.style.marker_length
