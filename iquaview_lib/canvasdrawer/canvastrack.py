#!/usr/bin/env python
# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


"""
 Widget to configure track appearance (color) and allow to clear track
 history and center track on the map.
"""

import logging
import math
from typing import Union

from PyQt5.QtCore import pyqtSignal, QObject, QSize, QPoint, QLine, Qt, QRectF
from PyQt5.QtGui import QColor, QPixmap, QPainter, QPen, QBrush, QPolygonF, QPainterPath
from PyQt5.QtWidgets import QFileDialog
from qgis.core import QgsRectangle, QgsWkbTypes, QgsGeometry, QgsVectorLayer, QgsFeature, QgsVectorFileWriter, QgsCoordinateReferenceSystem, QgsCoordinateTransform, QgsProject, QgsPointXY
from qgis.gui import QgsRubberBand, QgsMapCanvas

from iquaview_lib.canvasdrawer.canvastrackstyle import CanvasTrackStyle, LINE_GEOMETRY
from iquaview_lib.canvasdrawer.canvasdrawer import CanvasDrawer
from iquaview_lib.canvasdrawer.canvasmarker import CanvasMarker
from iquaview_lib.utils.qgisutils import transform_point
from iquaview_lib.baseclasses.webserverbase import WebServerPoint, WebServerRegister

logger = logging.getLogger(__name__)

TRACK_Z_VALUE = 1
MARKER_Z_VALUE = 2
CONNECTION_LOST_Z_VALUE = 3


class CanvasTrack(CanvasDrawer):
    """
         Class to configure track appearance (color) and allow to clear track
         history and center track on the map.
    """
    position_orientation_signal = pyqtSignal(WebServerPoint)

    def __init__(self, canvas: QgsMapCanvas, style: CanvasTrackStyle, marker: CanvasMarker, parent: QObject = None):
        """ Init of the object CanvasTrack """
        super().__init__(canvas, style, QgsWkbTypes.LineGeometry,  parent)
        self.position = QgsPointXY(0, 0)
        self.marker: Union[CanvasMarker, None] = None
        self.connection_lost_marker = None
        self.has_connection_lost_marker = False

        if style.geometry_type == LINE_GEOMETRY:
            self.geom_type = QgsWkbTypes.LineGeometry
        else:
            self.geom_type = QgsWkbTypes.PointGeometry

        if marker:
            # Add marker
            self.marker = marker
            self.marker.setZValue(MARKER_Z_VALUE)
            self.has_connection_lost_marker = self.marker.style.has_connection_lost_marker
        # Add rubber band
        self.band = QgsRubberBand(self.canvas, self.geom_type)
        self.band.setZValue(TRACK_Z_VALUE)
        self.set_color(QColor(style.color_hex_str))
        if self.geom_type == QgsWkbTypes.PointGeometry:
            self.band.setIcon(QgsRubberBand.ICON_CIRCLE)
            self.band.setIconSize(12)
        else:
            self.band.setWidth(3)

        if self.has_connection_lost_marker:
            connection_lost_config = CanvasTrackStyle()
            connection_lost_config.color = style.color
            connection_lost_config.marker_active = True
            connection_lost_config.marker_svg = ":/resources/connectionLost.svg"
            connection_lost_config.marker_width = float(20)
            connection_lost_config.marker_length = float(20)
            self.connection_lost_marker = CanvasMarker(canvas,
                                                       connection_lost_config,
                                                       is_icon=True,
                                                       orientation=False,
                                                       )
            self.connection_lost_marker.setZValue(CONNECTION_LOST_Z_VALUE)
            self.connection_lost_marker.hide()

    def canvas_crs_changed(self):
        """
        Canvas crs changed. update points to new crs
        """
        trans = QgsCoordinateTransform(self.current_crs,
                                       self.canvas.mapSettings().destinationCrs(),
                                       QgsProject.instance().transformContext())

        geometry: QgsGeometry = self.band.asGeometry()
        if not geometry.isNull():
            if geometry.type() == QgsWkbTypes.LineGeometry:
                list_wp = geometry.asPolyline()
            else:
                list_wp = geometry.asMultiPoint()

            for idx, wp in enumerate(list_wp):
                if wp is not None:
                    pos = trans.transform(wp.x(), wp.y())
                    self.band.movePoint(idx, pos)

        if self.position is not None:
            last_pos = trans.transform(self.position.x(), self.position.y())
            self.position = last_pos
            self.marker.set_center(self.position)

            if self.has_connection_lost_marker:
                self.connection_lost_marker.set_center(self.position, math.radians(-self.canvas.rotation()))

        self.current_crs = self.canvas.mapSettings().destinationCrs()

    def update_z_value(self, z_value):
        """
        Update ZValue. The order will always be 'connection lost marker' > 'marker' > 'band'
        :param z_value: new value
        """
        self.band.setZValue(5000 - z_value)
        self.marker.setZValue(10000 - (z_value * 2 + 1))
        self.connection_lost_marker.setZValue(10000 - (z_value * 2))

    def set_color(self, color: QColor):
        """
        Changes the color of the marker to match that of color_btn. Called when color_btn has its color changed.
        """
        transparent_color = QColor(color)
        transparent_color.setAlpha(80)
        self.style.color_hex_str = transparent_color.name()
        self.band.setColor(transparent_color)
        self.marker.set_color(color)

    def set_style(self, style: CanvasTrackStyle):
        super().set_style(style)
        self.set_color(QColor(style.color_hex_str))
        if style.geometry_type == LINE_GEOMETRY:
            geometry_type = QgsWkbTypes.LineGeometry
        else:
            geometry_type = QgsWkbTypes.PointGeometry
        self.set_geometry_type(geometry_type)

        if style.marker_active:
            self.marker.set_svg(style.marker_svg)
            self.marker.set_width(style.marker_width)
            self.marker.set_length(style.marker_length)
            self.marker.set_marker_mode(style.marker_mode)
            self.marker.set_marker_scale(style.marker_scale)
            self.marker.set_marker_active(True)
        else:
            self.marker.set_svg(None)
            self.marker.set_marker_active(False)

    def set_geometry_type(self, geom_type):
        """
        Changes the geometry type of the band representation
        """
        if geom_type != self.geom_type:
            self.geom_type = geom_type
            geometry: QgsGeometry = self.band.asGeometry()
            destmultipart = self.geom_type == QgsWkbTypes.PointGeometry
            new_geometry = geometry.convertToType(self.geom_type, destmultipart)
            self.clear_track()
            for vertex in new_geometry.vertices():
                self.add_position(QgsPointXY(vertex), update=False)
            if self.geom_type == QgsWkbTypes.PointGeometry:
                self.band.setIcon(QgsRubberBand.ICON_CIRCLE)
                self.band.setIconSize(12)
                self.band.setWidth(1)
            else:
                self.band.setWidth(3)

    def add_position(self, position, update=True):
        """
        Adds position to the band
        :param position: point to be added
        :type position: QgsPointXY
        :param update: Should the map canvas be updated immediately?. Default: True
        :type update: bool
        """
        self.band.addPoint(position, doUpdate=update)

    def track_update_canvas(self, position: QgsPointXY, heading: float, epsg_id: int, update: bool):
        """
        Updates position and heading of the marker if its used and adds the position to the band.
        crs defines the original data CRS
        :param position: Position to update to
        :type position: QgsPointXY
        :param heading: Heading to update to (in Radians)
        :type heading: float
        :param epsg_id: crs EPSG ID of the data
        :type epsg_id: int
        :param update: Update the canvas
        :type update: bool
        """
        if update and self.hidden:
            self.hidden = False
            self.show_band()
            self.show_marker()
        crs = QgsCoordinateReferenceSystem.fromEpsgId(epsg_id)
        new_position = transform_point(position, crs, self.canvas.mapSettings().destinationCrs())
        self.position = new_position
        self.heading = heading
        self.add_position(new_position, update=update)
        self.marker.set_center(new_position, heading)

        if self.has_connection_lost_marker:
            self.connection_lost_marker.set_center(new_position, math.radians(-self.canvas.rotation()))

    def get_center(self):
        return self.position

    def get_rectangle(self):
        return QgsRectangle(self.position, self.position)

    def hide_marker(self):
        """ Hides the marker if it is being used """
        self.marker.hide()

    def hide_connection_lost_marker(self):
        if not self.hidden and self.has_connection_lost_marker:
            self.connection_lost_marker.hide()

    def hide(self):
        super().hide_band()
        self.hide_marker()
        self.hide_connection_lost_marker()

    def show(self, connected: bool):
        super().show_band()
        self.show_marker()
        if not connected:
            self.show_connection_lost_marker()

    def show_marker(self):
        """ Shows the marker if it is being used """
        if not self.hidden:
            self.marker.show()

    def show_connection_lost_marker(self):
        if not self.hidden and self.has_connection_lost_marker:
            self.connection_lost_marker.show()

    def export_track(self):
        """
        Save the track to disk
        """
        layer_name, selected_filter = QFileDialog.getSaveFileName(None, 'Save Track', "",
                                                                  'Shapefile (*.shp);;KML (*.kml);;GPX (*.gpx)')

        if layer_name != '':

            if self.geom_type == QgsWkbTypes.PointGeometry:
                geometric_object = "MultiPoint"
            else:
                geometric_object = "LineString"

            geometric_object += "?crs=" + self.canvas.mapSettings().destinationCrs().authid()

            layer = QgsVectorLayer(
                geometric_object,
                layer_name,
                "memory")
            feature = QgsFeature()
            feature.setGeometry(self.band.asGeometry())
            layer.dataProvider().addFeatures([feature])

            if selected_filter == "Shapefile (*.shp)":

                if not layer_name.endswith('.shp'):
                    layer_name = layer_name + '.shp'
                ret = QgsVectorFileWriter.writeAsVectorFormat(layer, layer_name, "utf-8",
                                                              QgsCoordinateReferenceSystem.fromEpsgId(4326),
                                                              "ESRI Shapefile")
                if ret == QgsVectorFileWriter.NoError:
                    logger.info(layer.name() + " saved to " + layer_name)

            elif selected_filter == "KML (*.kml)":

                if not layer_name.endswith('.kml'):
                    layer_name = layer_name + '.kml'

                QgsVectorFileWriter.writeAsVectorFormat(layer, layer_name, "utf-8",
                                                        QgsCoordinateReferenceSystem.fromEpsgId(4326),
                                                        "KML")
            elif selected_filter == "GPX (*.gpx)":

                if not layer_name.endswith('.gpx'):
                    layer_name = layer_name + '.gpx'
                ds_options = ["GPX_USE_EXTENSIONS=TRUE"]
                QgsVectorFileWriter.writeAsVectorFormat(layer, layer_name, "utf-8",
                                                        QgsCoordinateReferenceSystem.fromEpsgId(4326),
                                                        "GPX",
                                                        datasourceOptions=ds_options)

    def close(self):
        """ Overrides close method from QWidget. Hide bands and markers"""
        super().close()
        self.hide_marker()
        self.hide_connection_lost_marker()

    def webserver_register(self, name: str):
        return WebServerRegister(name, self.get_style().color_hex_str, self.marker.has_orientation())

    def set_marker_size(self, width: float, length: float):
        self.marker.set_width(width)
        self.marker.set_length(length)

    def can_export(self) -> bool:
        return True

    def generate_icon(self, size: QSize, margin: QSize = QSize(0, 0)) -> QPixmap:
        height = size.height()
        width = size.width()
        left = 0 + margin.width()
        top = 0 + margin.height()
        color = QColor(self.style.color_hex_str)
        init_y_circle = (height / 3.3 + margin.height()) if self.marker.orientation else (top + 5)
        middle_y_circle = init_y_circle + width / 2
        rect = QRectF(left, init_y_circle, width, width)
        pointpen = QPen(Qt.black)
        pointpen.setWidth(1)
        pointbrush = QBrush(color)

        pixmap = QPixmap(width + margin.width() * 2, height + margin.height() * 2)
        pixmap.fill(Qt.transparent)

        painter = QPainter(pixmap)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setBrush(pointbrush)
        painter.setPen(pointpen)

        y = init_y_circle
        x = (rect.width() / 2) + margin.width()
        line = QLine(x, y, x, y + rect.height())
        y = middle_y_circle
        x = left
        line2 = QLine(x, y, x + rect.width(), y)

        # Arrow
        p = QPolygonF()
        p.append(QPoint(left, middle_y_circle))
        p.append(QPoint((rect.width() / 2) + margin.width(), top))
        p.append(QPoint(width + margin.width(), middle_y_circle))
        p.append(QPoint((rect.width() / 2) + margin.width(), middle_y_circle))

        painter.save()
        orientation = self.marker.orientation
        if orientation:
            path = QPainterPath()
            path.addPolygon(p)
            painter.drawPath(path)
        painter.restore()
        painter.drawEllipse(rect)

        painter.setPen(pointpen)
        painter.drawLine(line)
        painter.drawLine(line2)
        painter.end()

        return pixmap
