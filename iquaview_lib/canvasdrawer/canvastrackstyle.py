# Copyright (c) 2022 Iqua Robotics SL
#
# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.

"""
 Definitions of the configuration parameters for the track style.
"""

import sys

from copy import copy
from dataclasses import dataclass, asdict
from typing import Optional, Dict

from PyQt5.QtCore import QFileInfo, QSettings
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QWidget, QFileDialog


from iquaview_lib.baseclasses.canvasdrawerstyle import CanvasDrawerStyle, CanvasDrawerStyleConfigWidget
from iquaview_lib.ui.ui_canvastrack_style_config_widget import Ui_CanvasTrackStyleConfigWidget

if sys.version_info >= (3, 8):
    from typing import Literal
else:
    from typing_extensions import Literal

POINT_GEOMETRY = 0
LINE_GEOMETRY = 1

AUTOMATIC_MODE = 'auto'
MANUAL_MODE = 'manual'


@dataclass
class CanvasTrackStyle(CanvasDrawerStyle):
    """Configuration options for the Track Style class."""

    geometry_type: int = LINE_GEOMETRY
    color_hex_str: str = '#000000'
    marker_active: bool = False
    marker_svg: Optional[str] = None
    marker_width: Optional[float] = None
    marker_length: Optional[float] = None
    marker_mode: Optional[Literal['auto', 'manual']] = AUTOMATIC_MODE
    marker_scale: Optional[int] = 400
    has_connection_lost_marker: bool = True

    def check_dict(self, config_dict: Dict) -> bool:
        return asdict(CanvasTrackStyle()).keys() == config_dict.keys()

    def from_dict(self, config_dict: Dict) -> bool:
        """
        Convert from dictionary
        :param config_dict: dictionary with the configuration parameters
        :type config_dict: ConfigTrackDict
        """
        if not self.check_dict(config_dict):
            return False
        self.geometry_type = config_dict['geometry_type']
        self.color_hex_str = config_dict['color_hex_str']
        self.marker_active = config_dict['marker_active']
        self.marker_svg = config_dict['marker_svg']
        self.marker_width = config_dict['marker_width']
        self.marker_length = config_dict['marker_length']
        self.marker_mode = config_dict['marker_mode']
        self.marker_scale = config_dict['marker_scale']
        self.has_connection_lost_marker = config_dict['has_connection_lost_marker']
        return True

    def to_dict(self) -> Dict:
        """
        Convert to dictionary.
        :return: dictionary with the configuration parameters
        :rtype: ConfigTrackDict
        """
        return {
            'geometry_type': copy(self.geometry_type),
            'color_hex_str': copy(self.color_hex_str),
            'marker_active': copy(self.marker_active),
            'marker_svg': copy(self.marker_svg),
            'marker_width': copy(self.marker_width),
            'marker_length': copy(self.marker_length),
            'marker_mode': copy(self.marker_mode),
            'marker_scale': copy(self.marker_scale),
            'has_connection_lost_marker': copy(self.has_connection_lost_marker),
        }

    def config_widget(self, parent: QWidget = None) -> "CanvasTrackStyleConfigWidget":
        return CanvasTrackStyleConfigWidget(self, parent)


class CanvasTrackStyleConfigWidget(CanvasDrawerStyleConfigWidget, Ui_CanvasTrackStyleConfigWidget):

    def __init__(self, style: CanvasTrackStyle, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.set_configuration(style)

    def set_configuration(self, style: CanvasTrackStyle) -> None:
        """
        Set the configuration of the widget
        :param style: configuration
        :type style: CanvasTrackStyle
        """
        if style.color_hex_str is None:
            style.color_hex_str = QColor(self.get_random_color()).name()
        self.color_btn.setColor(QColor(style.color_hex_str))

        if style.geometry_type == LINE_GEOMETRY:
            self.path_radioButton.setChecked(True)
        else:
            self.points_radioButton.setChecked(True)

        if not style.marker_active:
            self.marker_checkBox.setChecked(False)
        else:
            self.marker_checkBox.setChecked(True)
            self.lineEdit.setText(style.marker_svg)
            self.width_doubleSpinBox.setValue(style.marker_width)
            self.length_doubleSpinBox.setValue(style.marker_length)
            if style.marker_mode == AUTOMATIC_MODE:
                self.auto_canvasmarker_radioButton.setChecked(True)
            else:
                self.manual_canvasmarker_radioButton.setChecked(True)
            self.mScaleWidget.setScale(style.marker_scale)

        self.marker_toolButton.clicked.connect(self.load_marker_file)
        # old implementation before mregint with constructor
        # if config_style.geometry_type == LINE_GEOMETRY:
        #    self.path_radioButton.setChecked(True)
        # else:
        #    self.points_radioButton.setChecked(True)
#
        # if config_style.marker_mode == MANUAL_MODE:
        #    self.manual_canvasmarker_radioButton.setChecked(True)
        # else:
        #    self.auto_canvasmarker_radioButton.setChecked(True)
#
        # self.color_btn.setColor(QColor(config_style.color))
        # self.marker_checkBox.setChecked(config_style.marker_active)
        # self.lineEdit.setText(config_style.marker_svg)
        # self.width_doubleSpinBox.setValue(config_style.marker_width)
        # self.length_doubleSpinBox.setValue(config_style.marker_length)
        # self.mScaleWidget.setScale(config_style.marker_scale)

    def get_configuration(self) -> CanvasTrackStyle:
        """
        Get the configuration of the widget
        :return: configuration
        :rtype: CanvasTrackStyle
        """
        return CanvasTrackStyle(LINE_GEOMETRY if self.path_radioButton.isChecked() else POINT_GEOMETRY,
                                QColor(self.color_btn.color()).name(),
                                self.marker_checkBox.isChecked(),
                                self.lineEdit.text(),
                                self.width_doubleSpinBox.value(),
                                self.length_doubleSpinBox.value(),
                                AUTOMATIC_MODE if self.auto_canvasmarker_radioButton.isChecked() else MANUAL_MODE,
                                self.mScaleWidget.scale(),
                                True)

    def is_valid(self) -> bool:
        """
        Check if the configuration is valid
        :return: True if the configuration is valid, False otherwise
        :rtype: bool
        """
        representation_valid = self.path_radioButton.isChecked() or self.points_radioButton.isChecked()
        color_valid = self.color_btn.color().isValid()
        marker_valid = not self.marker_checkBox.isChecked()
        if self.marker_checkBox.isChecked():
            size_valid = self.width_doubleSpinBox.value() > 0 and self.length_doubleSpinBox.value() > 0
            marker_file_valid = self.lineEdit.text() != ""
            if self.manual_canvasmarker_radioButton.isChecked():
                manual_valid = self.mScaleWidget.scale() != ""
            else:
                manual_valid = True
            marker_valid = size_valid and marker_file_valid and manual_valid

        return representation_valid and color_valid and marker_valid

    def load_marker_file(self):
        """ Load marker file """
        default_dir = "Marker path"
        settings = QSettings()
        marker_file, __ = QFileDialog.getOpenFileName(self,
                                                      'AUV configuration',
                                                      settings.value(default_dir),
                                                      "All files (*.*)")
        if marker_file:
            file_info = QFileInfo(marker_file)
            filename = file_info.canonicalFilePath()
            settings.setValue(default_dir, filename)
            self.lineEdit.setText(str(filename))
