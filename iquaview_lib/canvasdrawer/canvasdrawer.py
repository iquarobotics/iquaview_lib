#!/usr/bin/env python
# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


"""
 Widget to configure track appearance (color) and allow to clear track
 history and center track on the map.
"""

import sys
from copy import copy
import logging
from typing import Union, Dict

from PyQt5.QtCore import QTimer, pyqtSignal, QObject, QSize
from PyQt5.QtGui import QPixmap
from qgis.core import QgsRectangle, QgsWkbTypes, QgsPointXY
from qgis.gui import QgsRubberBand, QgsMapCanvas

from iquaview_lib.baseclasses.webserverbase import WebServerRegister
from iquaview_lib.baseclasses.canvasdrawerstyle import CanvasDrawerStyle

logger = logging.getLogger(__name__)


class CanvasDrawer(QObject):
    unlock_signal = pyqtSignal()
    zoom_position_signal = pyqtSignal()
    position_signal = pyqtSignal(QgsPointXY)
    stop_timer_signal = pyqtSignal()

    def __init__(self, canvas: QgsMapCanvas, style: CanvasDrawerStyle, geometry_type: QgsWkbTypes.GeometryType, parent: QObject = None) -> None:
        super().__init__(parent)

        self.canvas = canvas
        self.current_crs = self.canvas.mapSettings().destinationCrs()
        self.band: Union[QgsRubberBand, None] = None
        self.center = False
        self.hidden = True
        self.locked = False
        if not isinstance(style, CanvasDrawerStyle):
            raise RuntimeError("Style not config canvas drawer style")
        self.style: CanvasDrawerStyle = copy(style)
        if not isinstance(geometry_type, QgsWkbTypes.GeometryType):
            raise RuntimeError("Geometry type not right type")
        self.geom_type:  Union[QgsWkbTypes.GeometryType, None] = copy(geometry_type)

        self.timer = QTimer()
        self.timer.timeout.connect(self.send_signal_position)
        self.stop_timer_signal.connect(self.stop_timer)

        self.canvas.destinationCrsChanged.connect(self.canvas_crs_changed)

    def canvas_crs_changed(self):
        raise NotImplementedError()

    def update_z_value(self, z_value):
        """
        Update ZValue.
        :param z_value: new value
        """
        raise NotImplementedError()

    def get_center(self) -> QgsPointXY:
        raise NotImplementedError()

    def get_rectangle(self) -> QgsRectangle:
        raise NotImplementedError()

    def center_to_location(self):
        """
        Center to last received position on the map.
        """
        if not self.locked:
            self.canvas.setExtent(self.get_rectangle())
            self.canvas.zoomScale(400)
            self.canvas.refresh()
        else:
            self.zoom_position_signal.emit()

    def stop_timer(self):
        """ Stop timer to update data"""
        self.timer.stop()

    def lock_location(self):
        """ Lock location center and zoom"""
        self.locked = not self.locked
        if self.locked:
            if not self.timer.isActive():
                self.timer.start(500)
        else:
            self.unlock_signal.emit()
            self.stop_timer_signal.emit()

        return self.locked

    def send_signal_position(self):
        """ Send position via signal (QgsPointXY)"""
        if not self.hidden:
            self.position_signal.emit(self.get_center())

    def show(self, connected: bool):
        raise NotImplementedError()

    def hide(self):
        raise NotImplementedError()

    def hide_band(self):
        """ Hides the rubber band """
        if self.band.isVisible():
            self.band.hide()

    def show_band(self):
        """ Shows the rubber band """
        if not self.hidden and not self.band.isVisible():
            self.band.show()

    def clear_track(self):
        """ Resets rubber band track, removing all its points """
        visible = self.band.isVisible()
        self.band.reset(self.geom_type)
        if visible:
            self.show_band()
        else:
            self.hide_band()

    def close(self):
        """ Overrides close method from QWidget. Hide bands and markers"""
        self.hide_band()

    def get_style(self) -> CanvasDrawerStyle:
        return self.style

    def set_style(self, style: CanvasDrawerStyle):
        self.style = copy(style)

    def webserver_register(self, name: str) -> WebServerRegister:
        return None

    def to_dict(self) -> Dict:
        return {'style': self.style.to_dict()}

    def can_export(self) -> bool:
        raise NotImplementedError()

    def generate_icon(self, size: QSize, margin: QSize = QSize(0, 0)) -> QPixmap:
        """
        Generate an icon for the drawer.
        :param size: Size of the drawing area.
        :type size: QSize
        :param margin: Margin to add to each side of the drawing area defined by size. The resulting pixmap will be size.width()+margin.width()*2, size.height()+margin.height()*2
        :type margin: QSize
        """
        raise NotImplementedError()
