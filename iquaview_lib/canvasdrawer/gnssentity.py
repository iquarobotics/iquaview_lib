#!/usr/bin/env python
# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.

"""
    This file contains the definition of the GNSSEntity class.
"""

import logging
import math
from typing import Dict, List

from PyQt5.QtCore import QTimer, pyqtSignal
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QWidget
from qgis.core import QgsPointXY

from iquaview_lib.baseclasses.entitybase import EntityBase, LabelData
from iquaview_lib.baseclasses.webserverbase import WebServerPoint
from iquaview_lib.cola2api.gps_driver import GPSOrientation, GPSPosition
from iquaview_lib.connection.settings.gpsconnectionwidget import GPSConnectionWidget

logger = logging.getLogger(__name__)

WARNING_SIGNAL = "Connected: NO signal"
WARNING_ORIENTATION = "Connected: NO orientation"


class GNSSEntity(EntityBase):
    """
    This class is used to connect and display the GPS data.
    """

    stop_timer_signal = pyqtSignal()
    LABEL_GPS_FIX_QUALITY = "quality"

    def __init__(self, **kwargs):
        super().__init__(datalog_headers=GNSSEntity.headers(), **kwargs)
        self.timer = QTimer()
        self.timer.timeout.connect(self.update_canvas)
        self.stop_timer_signal.connect(self.stop_timer)
        self.declare_label_data(
            identifier=self.LABEL_GPS_FIX_QUALITY,
            label_data=LabelData(
                label_name="Quality: ",
                text="-",
                color=QColor("black"),
            ),
        )
        self.driver.connect_signal.connect(self.connect)

    def connect(self) -> None:
        if not self.connected:
            if not self.driver.keepgoing:
                self.driver.connect()
            if not self.timer.isActive():
                self.timer.start(1000)
            self.connected = True
            if self.canvasdrawer.connection_lost_marker.isVisible():
                self.canvasdrawer.hide_connection_lost_marker()

        self.entity_updated_signal.emit()

    def stop_timer(self):
        """Stop timer to update data"""
        self.timer.stop()

    def close(self) -> None:
        self.close_dialog()
        self.driver.close()
        self.stop_timer_signal.emit()
        self.connected = False
        if not self.canvasdrawer.connection_lost_marker.isVisible():
            self.canvasdrawer.show_connection_lost_marker()

        self.entity_updated_signal.emit()
        self.set_label_data(identifier=self.LABEL_GPS_FIX_QUALITY, label_data=LabelData(text="-"))
        self.canvasdrawer.position_orientation_signal.emit(
            WebServerPoint(
                name=self.name,
                connected=self.connected,
                latitude=0,
                longitude=0,
            )
        )

    def update_canvas(self) -> None:
        """Updates new values from the driver into the canvas track that draws over the canvas."""
        if self.connected:
            gps_position: GPSPosition = self.driver.get_position()
            gps_orientation: GPSOrientation = self.driver.get_orientation()
            self.on_measurement(gps_position, gps_orientation)
            if gps_position.is_new() and (gps_position.quality >= 1) and (gps_position.quality <= 6):
                self.set_warning(WARNING_SIGNAL, False)
                gps_lat = gps_position.latitude
                gps_lon = gps_position.longitude
                if gps_orientation.is_new():
                    gps_heading = gps_orientation.orientation_deg
                    self.set_warning(WARNING_ORIENTATION, False)
                else:
                    gps_heading = 0
                    self.set_warning(WARNING_ORIENTATION, True)

                pos = QgsPointXY(gps_lon, gps_lat)

                self.canvasdrawer.track_update_canvas(pos, math.radians(gps_heading), epsg_id=4326, update=self.visible)

                self.canvasdrawer.position_orientation_signal.emit(
                    WebServerPoint(
                        name=self.name,
                        connected=self.connected,
                        latitude=gps_lat,
                        longitude=gps_lon,
                        orientation_deg=gps_heading,
                    )
                )
                self.entity_updated_signal.emit()

                gps_fix_quality = gps_position.get_fix_quality_as_string()
                if gps_fix_quality != self.info_labels[self.LABEL_GPS_FIX_QUALITY].text:
                    self.set_label_data(
                        identifier=self.LABEL_GPS_FIX_QUALITY,
                        label_data=LabelData(text=gps_position.get_fix_quality_as_string()),
                    )

            elif not gps_position.is_new():
                self.set_warning(WARNING_SIGNAL, True)

    def is_valid_data(self, driver_widget, style_widget):
        return driver_widget.is_valid() and style_widget.is_valid()

    def remove(self):
        """Remove entity from configuration settings"""
        self.close()
        self.canvasdrawer.close()
        if not self.config.settings["input_tracks"]:
            self.config.settings["input_tracks"] = {}

        self.config.settings["input_tracks"].pop(self.identifier, None)

        self.config.save()

        self.entity_updated_signal.emit()

    def properties_widgets(self) -> Dict[str, QWidget]:
        widgets = {}
        gps_widget = self.driver.get_configuration_widget()

        style_widget = self.canvasdrawer.style.config_widget()
        style_widget.set_configuration(self.canvasdrawer.get_style())

        widgets[self.PROPERTIES_DRIVER] = gps_widget
        widgets[self.PROPERTIES_STYLE] = style_widget
        return widgets

    def save_changes(self, widgets: Dict[str, QWidget]) -> bool:
        saved = False
        try:
            driver_configuration: GPSConnectionWidget = widgets[self.PROPERTIES_DRIVER]
            self.driver.set_configuration(driver_configuration.get_configuration().to_dict())
            saved = super().save_changes(widgets)
        except Exception as e:
            logger.error(f"Entity not saved: {e}")

        return saved

    @staticmethod
    def headers() -> List[str]:
        return [
            # position
            "position.stamp",
            "position.latitude",
            "position.longitude",
            "position.altitude",
            "position.quality",
            "position.quality_string",
            # orientation
            "orientation.stamp",
            "orientation.orientation_deg",
        ]

    def on_measurement(self, pos: GPSPosition, ori: GPSOrientation) -> None:
        self.datalog_record(
            [
                # position
                f"{pos.time:.3f}",
                f"{pos.latitude:.8f}",
                f"{pos.longitude:.8f}",
                f"{pos.altitude:.3f}",
                f"{pos.quality:d}",
                f"{pos.get_fix_quality_as_string():s}",
                # orientation
                f"{ori.time:.3f}",
                f"{ori.orientation_deg:.2f}",
            ]
        )
