#!/usr/bin/env python
# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


"""
 Widget to configure track appearance (color) and allow to clear track
 history and center track on the map.
"""

import logging
import math
from typing import List, Tuple

from PyQt5.QtCore import QObject, QSize, Qt
from PyQt5.QtGui import QColor, QPixmap, QPen, QBrush, QPainter
from qgis.core import QgsWkbTypes, QgsGeometry, QgsCoordinateReferenceSystem, QgsPointXY
from qgis.gui import QgsRubberBand, QgsMapCanvas

from iquaview_lib.canvasdrawer.canvaspolygonstyle import CanvasPolygonStyle
from iquaview_lib.canvasdrawer.canvasdrawer import CanvasDrawer

logger = logging.getLogger(__name__)


class CanvasPolygon(CanvasDrawer):
    """
         Class to configure track appearance (color) and allow to clear track
         history and center track on the map.
    """
    DRAW_CRS = QgsCoordinateReferenceSystem.fromEpsgId(4326)  # wgs84

    def __init__(self, canvas: QgsMapCanvas, style: CanvasPolygonStyle, parent: QObject = None):
        """ Init of the object CanvasPolygon """
        super().__init__(canvas, style, QgsWkbTypes.PolygonGeometry, parent)

        self.band = QgsRubberBand(self.canvas, self.geom_type)
        self.last_range_band = QgsRubberBand(self.canvas, self.geom_type)

        self.last_canvas_crs = self.canvas.mapSettings().destinationCrs()

    def canvas_crs_changed(self):
        """
        Canvas crs changed. update points to new crs
        """
        geometry: QgsGeometry = self.band.asGeometry()
        if not geometry.isNull():
            self.band.reset(self.geom_type)
            self.band.addGeometry(geometry, crs=self.last_canvas_crs)

        geometry: QgsGeometry = self.last_range_band.asGeometry()
        if not geometry.isNull():
            self.last_range_band.reset(self.geom_type)
            self.last_range_band.addGeometry(geometry, crs=self.last_canvas_crs)

        # update crs
        self.last_canvas_crs = self.canvas.mapSettings().destinationCrs()

    def clear(self) -> None:
        self.band.reset(self.geom_type)
        self.last_range_band.reset(self.geom_type)

    def add_polygon(self, points: List[Tuple[float, float]]) -> None:
        if self.hidden:
            self.hidden = False
            self.show(True)

        # Overwrite last range band
        visible: bool = self.last_range_band.isVisible()
        self.last_range_band.reset(self.geom_type)
        self.last_range_band.setFillColor(self.get_color_current())
        g = QgsGeometry.fromPolygonXY([[
            QgsPointXY(lon, lat) for lat, lon in points
        ]])
        self.last_range_band.addGeometry(g, crs=self.DRAW_CRS)

        # Add to general band
        self.band.addGeometry(g, crs=self.DRAW_CRS)

        if not visible:
            self.hide()

    def get_color_old(self):
        color = QColor(self.style.color_old_hex_str)
        color.setAlpha(80)
        return color

    def get_color_current(self):
        color = QColor(self.style.color_current_hex_str)
        color.setAlpha(80)
        return color

    def set_style(self, style: CanvasPolygonStyle):
        super().set_style(style)
        self.band.setFillColor(self.get_color_old())
        self.last_range_band.setFillColor(self.get_color_current())

    def update_z_value(self, z_value):
        self.band.setZValue(5000 - z_value)
        self.last_range_band.setZValue(10000 - (z_value * 2 + 1))

    def show(self, connected: bool):
        super().show_band()
        if not self.hidden and not self.last_range_band.isVisible():
            self.last_range_band.show()

    def hide(self):
        super().hide_band()
        if self.last_range_band.isVisible():
            self.last_range_band.hide()

    def get_center(self):
        return self.last_range_band.asGeometry().centroid().asPoint()

    def get_rectangle(self):
        return self.last_range_band.asGeometry().boundingBox()

    def can_export(self) -> bool:
        return False

    def generate_icon(self, size: QSize, margin: QSize = QSize(0, 0)) -> QPixmap:
        color = QColor(self.style.color_current_hex_str)
        width = size.width()
        height = size.height()

        pointpen = QPen(Qt.black)
        pointpen.setWidth(1)
        pointbrush = QBrush(color)

        pixmap = QPixmap(width + margin.width() * 2, height + margin.height() * 2)
        pixmap.fill(Qt.transparent)

        painter = QPainter(pixmap)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setBrush(pointbrush)
        painter.setPen(pointpen)

        # Range polygon
        span_angle = 30  # deg
        center_angle = -135  # deg
        span_angle_2 = span_angle/2
        min_size = min(width, height)
        radius = min_size/max(abs(math.cos(math.radians(center_angle + span_angle_2))),
                              abs(math.cos(math.radians(center_angle - span_angle_2))))
        pie_height = radius * max(abs(math.sin(math.radians(center_angle + span_angle_2))),
                                  abs(math.sin(math.radians(center_angle - span_angle_2))))
        painter.drawPie(margin.width() + min_size - radius, pixmap.height()/2 - pie_height/2 - radius,
                        2 * radius, 2 * radius, (center_angle-span_angle/2)*16, (span_angle)*16)
        painter.end()

        return pixmap
