# Copyright (c) 2022 Iqua Robotics SL
#
# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.

"""
 Definitions of the configuration parameters for the track style.
"""

import sys

from copy import copy
from dataclasses import dataclass, asdict
from typing import Dict

from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QWidget

from iquaview_lib.baseclasses.canvasdrawerstyle import CanvasDrawerStyle, CanvasDrawerStyleConfigWidget
from iquaview_lib.ui.ui_canvaspolygon_style_config_widget import Ui_CanvasPolygonStyleConfigWidget


@dataclass
class CanvasPolygonStyle(CanvasDrawerStyle):
    """
    Configuration parameters for the track style as typed dictionary.
    """
    color_old_hex_str: str = QColor(0, 0, 0, 20).name()
    color_current_hex_str: str = QColor(200, 0, 0, 60).name()

    def check_dict(self, config_dict: Dict) -> bool:
        return asdict(CanvasPolygonStyle()).keys() == config_dict.keys()

    def from_dict(self, config_dict: Dict) -> bool:
        if not self.check_dict(config_dict):
            return False
        self.color_old_hex_str = config_dict['color_old_hex_str']
        self.color_current_hex_str = config_dict['color_current_hex_str']
        return True

    def to_dict(self) -> Dict:
        return {
            'color_old_hex_str': copy(self.color_old_hex_str),
            'color_current_hex_str': copy(self.color_current_hex_str),
        }

    def config_widget(self, parent: QWidget = None) -> "CanvasPolygonStyleConfigWidget":
        return CanvasPolygonStyleConfigWidget(self, parent)


class CanvasPolygonStyleConfigWidget(CanvasDrawerStyleConfigWidget, Ui_CanvasPolygonStyleConfigWidget):

    def __init__(self, style: CanvasPolygonStyle, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.set_configuration(style)

    def set_configuration(self, style: CanvasPolygonStyle) -> None:
        self.current_range_color_btn.setColor(QColor(style.color_current_hex_str))
        self.old_ranges_color_btn.setColor(QColor(style.color_old_hex_str))

    def get_configuration(self) -> CanvasPolygonStyle:
        """
        Get the configuration of the widget
        :return: configuration
        :rtype: CanvasPolygonStyle
        """
        return CanvasPolygonStyle(color_current_hex_str=QColor(self.current_range_color_btn.color()).name(),
                                  color_old_hex_str=QColor(self.old_ranges_color_btn.color()).name())

    def is_valid(self) -> bool:
        """
        Check if the configuration is valid
        :return: True if the configuration is valid, False otherwise
        :rtype: bool
        """
        return True
