# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_action.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_ActionWidget(object):
    def setupUi(self, ActionWidget):
        ActionWidget.setObjectName("ActionWidget")
        ActionWidget.resize(400, 135)
        self.gridLayout = QtWidgets.QGridLayout(ActionWidget)
        self.gridLayout.setObjectName("gridLayout")
        self.action_name_label = QtWidgets.QLabel(ActionWidget)
        self.action_name_label.setObjectName("action_name_label")
        self.gridLayout.addWidget(self.action_name_label, 1, 0, 1, 1)
        self.action_id_label = QtWidgets.QLabel(ActionWidget)
        self.action_id_label.setObjectName("action_id_label")
        self.gridLayout.addWidget(self.action_id_label, 4, 0, 1, 1)
        self.description_label = QtWidgets.QLabel(ActionWidget)
        self.description_label.setObjectName("description_label")
        self.gridLayout.addWidget(self.description_label, 2, 0, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 0, 1, 1, 1)
        self.remove_toolButton = QtWidgets.QToolButton(ActionWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.remove_toolButton.sizePolicy().hasHeightForWidth())
        self.remove_toolButton.setSizePolicy(sizePolicy)
        self.remove_toolButton.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/resources/mActionDeleteSelected.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.remove_toolButton.setIcon(icon)
        self.remove_toolButton.setAutoRaise(True)
        self.remove_toolButton.setObjectName("remove_toolButton")
        self.gridLayout.addWidget(self.remove_toolButton, 0, 2, 1, 1)
        self.action_name_lineEdit = QtWidgets.QLineEdit(ActionWidget)
        self.action_name_lineEdit.setObjectName("action_name_lineEdit")
        self.gridLayout.addWidget(self.action_name_lineEdit, 1, 1, 1, 2)
        self.description_lineEdit = QtWidgets.QLineEdit(ActionWidget)
        self.description_lineEdit.setObjectName("description_lineEdit")
        self.gridLayout.addWidget(self.description_lineEdit, 2, 1, 1, 2)
        self.action_id_lineEdit = QtWidgets.QLineEdit(ActionWidget)
        self.action_id_lineEdit.setObjectName("action_id_lineEdit")
        self.gridLayout.addWidget(self.action_id_lineEdit, 4, 1, 1, 2)

        self.retranslateUi(ActionWidget)
        QtCore.QMetaObject.connectSlotsByName(ActionWidget)
        ActionWidget.setTabOrder(self.action_name_lineEdit, self.description_lineEdit)
        ActionWidget.setTabOrder(self.description_lineEdit, self.action_id_lineEdit)

    def retranslateUi(self, ActionWidget):
        _translate = QtCore.QCoreApplication.translate
        ActionWidget.setWindowTitle(_translate("ActionWidget", "Action"))
        self.action_name_label.setText(_translate("ActionWidget", "Action name:"))
        self.action_id_label.setText(_translate("ActionWidget", "Action ID:"))
        self.description_label.setText(_translate("ActionWidget", "Action description:"))
from iquaview_lib import resources_rc


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ActionWidget = QtWidgets.QWidget()
    ui = Ui_ActionWidget()
    ui.setupUi(ActionWidget)
    ActionWidget.show()
    sys.exit(app.exec_())
