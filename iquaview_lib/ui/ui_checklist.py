# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_checklist.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_ChecklistWidget(object):
    def setupUi(self, ChecklistWidget):
        ChecklistWidget.setObjectName("ChecklistWidget")
        ChecklistWidget.resize(333, 169)
        self.gridLayout = QtWidgets.QGridLayout(ChecklistWidget)
        self.gridLayout.setObjectName("gridLayout")
        self.items_groupBox = QtWidgets.QGroupBox(ChecklistWidget)
        self.items_groupBox.setStyleSheet("QGroupBox {\n"
"    border: 1px solid silver;\n"
"    border-radius: 6px;\n"
"    margin-top: 6px;\n"
"}\n"
"\n"
"QGroupBox::title {\n"
"    subcontrol-origin: margin;\n"
"    left: 7px;\n"
"    padding: 0px 5px 0px 5px;\n"
"}")
        self.items_groupBox.setObjectName("items_groupBox")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.items_groupBox)
        self.verticalLayout.setObjectName("verticalLayout")
        self.params_vboxlayout = QtWidgets.QVBoxLayout()
        self.params_vboxlayout.setObjectName("params_vboxlayout")
        self.verticalLayout.addLayout(self.params_vboxlayout)
        self.add_item_pushButton = QtWidgets.QPushButton(self.items_groupBox)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.add_item_pushButton.sizePolicy().hasHeightForWidth())
        self.add_item_pushButton.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setItalic(True)
        self.add_item_pushButton.setFont(font)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/resources/mActionAdd.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.add_item_pushButton.setIcon(icon)
        self.add_item_pushButton.setObjectName("add_item_pushButton")
        self.verticalLayout.addWidget(self.add_item_pushButton)
        self.gridLayout.addWidget(self.items_groupBox, 2, 0, 1, 3)
        self.description_label = QtWidgets.QLabel(ChecklistWidget)
        self.description_label.setObjectName("description_label")
        self.gridLayout.addWidget(self.description_label, 1, 0, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 0, 1, 1, 1)
        self.description_lineEdit = QtWidgets.QLineEdit(ChecklistWidget)
        self.description_lineEdit.setObjectName("description_lineEdit")
        self.gridLayout.addWidget(self.description_lineEdit, 1, 1, 1, 2)
        self.remove_checklist_toolButton = QtWidgets.QToolButton(ChecklistWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.remove_checklist_toolButton.sizePolicy().hasHeightForWidth())
        self.remove_checklist_toolButton.setSizePolicy(sizePolicy)
        self.remove_checklist_toolButton.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/resources/mActionDeleteSelected.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.remove_checklist_toolButton.setIcon(icon1)
        self.remove_checklist_toolButton.setAutoRaise(True)
        self.remove_checklist_toolButton.setObjectName("remove_checklist_toolButton")
        self.gridLayout.addWidget(self.remove_checklist_toolButton, 0, 2, 1, 1)

        self.retranslateUi(ChecklistWidget)
        QtCore.QMetaObject.connectSlotsByName(ChecklistWidget)
        ChecklistWidget.setTabOrder(self.description_lineEdit, self.add_item_pushButton)
        ChecklistWidget.setTabOrder(self.add_item_pushButton, self.remove_checklist_toolButton)

    def retranslateUi(self, ChecklistWidget):
        _translate = QtCore.QCoreApplication.translate
        ChecklistWidget.setWindowTitle(_translate("ChecklistWidget", "Checklist"))
        self.items_groupBox.setTitle(_translate("ChecklistWidget", "Items"))
        self.add_item_pushButton.setText(_translate("ChecklistWidget", "New Item"))
        self.description_label.setText(_translate("ChecklistWidget", "Check List ID:"))
from iquaview_lib import resources_rc


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    ChecklistWidget = QtWidgets.QWidget()
    ui = Ui_ChecklistWidget()
    ui.setupUi(ChecklistWidget)
    ChecklistWidget.show()
    sys.exit(app.exec_())
