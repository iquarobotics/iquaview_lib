# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_bag_widget.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_BagWidget(object):
    def setupUi(self, BagWidget):
        BagWidget.setObjectName("BagWidget")
        BagWidget.resize(285, 25)
        self.horizontalLayout = QtWidgets.QHBoxLayout(BagWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.time_horizontalSlider = QtWidgets.QSlider(BagWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.time_horizontalSlider.sizePolicy().hasHeightForWidth())
        self.time_horizontalSlider.setSizePolicy(sizePolicy)
        self.time_horizontalSlider.setOrientation(QtCore.Qt.Horizontal)
        self.time_horizontalSlider.setObjectName("time_horizontalSlider")
        self.horizontalLayout.addWidget(self.time_horizontalSlider)
        self.resume_pause_pushButton = QtWidgets.QPushButton(BagWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.resume_pause_pushButton.sizePolicy().hasHeightForWidth())
        self.resume_pause_pushButton.setSizePolicy(sizePolicy)
        self.resume_pause_pushButton.setObjectName("resume_pause_pushButton")
        self.horizontalLayout.addWidget(self.resume_pause_pushButton)
        self.speed_pushButton = QtWidgets.QPushButton(BagWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.speed_pushButton.sizePolicy().hasHeightForWidth())
        self.speed_pushButton.setSizePolicy(sizePolicy)
        self.speed_pushButton.setMinimumSize(QtCore.QSize(0, 0))
        self.speed_pushButton.setObjectName("speed_pushButton")
        self.horizontalLayout.addWidget(self.speed_pushButton)

        self.retranslateUi(BagWidget)
        QtCore.QMetaObject.connectSlotsByName(BagWidget)

    def retranslateUi(self, BagWidget):
        _translate = QtCore.QCoreApplication.translate
        BagWidget.setWindowTitle(_translate("BagWidget", "Bag Widget"))
        self.resume_pause_pushButton.setText(_translate("BagWidget", "Resume"))
        self.speed_pushButton.setText(_translate("BagWidget", "x5"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    BagWidget = QtWidgets.QWidget()
    ui = Ui_BagWidget()
    ui.setupUi(BagWidget)
    BagWidget.show()
    sys.exit(app.exec_())
