# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_auvconfigparams.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_AUVConfigParamsDlg(object):
    def setupUi(self, AUVConfigParamsDlg):
        AUVConfigParamsDlg.setObjectName("AUVConfigParamsDlg")
        AUVConfigParamsDlg.resize(492, 387)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(AUVConfigParamsDlg.sizePolicy().hasHeightForWidth())
        AUVConfigParamsDlg.setSizePolicy(sizePolicy)
        self.verticalLayout = QtWidgets.QVBoxLayout(AUVConfigParamsDlg)
        self.verticalLayout.setObjectName("verticalLayout")
        self.section_horizontalLayout = QtWidgets.QHBoxLayout()
        self.section_horizontalLayout.setObjectName("section_horizontalLayout")
        self.label = QtWidgets.QLabel(AUVConfigParamsDlg)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setObjectName("label")
        self.section_horizontalLayout.addWidget(self.label)
        self.section_comboBox = QtWidgets.QComboBox(AUVConfigParamsDlg)
        self.section_comboBox.setObjectName("section_comboBox")
        self.section_horizontalLayout.addWidget(self.section_comboBox)
        self.verticalLayout.addLayout(self.section_horizontalLayout)
        self.scrollArea = QtWidgets.QScrollArea(AUVConfigParamsDlg)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.scrollArea.sizePolicy().hasHeightForWidth())
        self.scrollArea.setSizePolicy(sizePolicy)
        self.scrollArea.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.scrollArea.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.scrollArea.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.scrollArea.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustIgnored)
        self.scrollArea.setWidgetResizable(True)
        self.scrollArea.setObjectName("scrollArea")
        self.scrollAreaWidgetContents = QtWidgets.QWidget()
        self.scrollAreaWidgetContents.setGeometry(QtCore.QRect(0, 0, 472, 303))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.scrollAreaWidgetContents.sizePolicy().hasHeightForWidth())
        self.scrollAreaWidgetContents.setSizePolicy(sizePolicy)
        self.scrollAreaWidgetContents.setObjectName("scrollAreaWidgetContents")
        self.formLayout = QtWidgets.QFormLayout(self.scrollAreaWidgetContents)
        self.formLayout.setSizeConstraint(QtWidgets.QLayout.SetNoConstraint)
        self.formLayout.setFieldGrowthPolicy(QtWidgets.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setObjectName("formLayout")
        self.scrollArea.setWidget(self.scrollAreaWidgetContents)
        self.verticalLayout.addWidget(self.scrollArea)
        self.buttonBox = QtWidgets.QDialogButtonBox(AUVConfigParamsDlg)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Apply|QtWidgets.QDialogButtonBox.Close|QtWidgets.QDialogButtonBox.Save)
        self.buttonBox.setCenterButtons(False)
        self.buttonBox.setObjectName("buttonBox")
        self.verticalLayout.addWidget(self.buttonBox)

        self.retranslateUi(AUVConfigParamsDlg)
        QtCore.QMetaObject.connectSlotsByName(AUVConfigParamsDlg)

    def retranslateUi(self, AUVConfigParamsDlg):
        _translate = QtCore.QCoreApplication.translate
        AUVConfigParamsDlg.setWindowTitle(_translate("AUVConfigParamsDlg", "AUV Configuration Parameters"))
        self.label.setText(_translate("AUVConfigParamsDlg", "Section:"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    AUVConfigParamsDlg = QtWidgets.QWidget()
    ui = Ui_AUVConfigParamsDlg()
    ui.setupUi(AUVConfigParamsDlg)
    AUVConfigParamsDlg.show()
    sys.exit(app.exec_())

