# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_ip_port_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_IPPortDialog(object):
    def setupUi(self, IPPortDialog):
        IPPortDialog.setObjectName("IPPortDialog")
        IPPortDialog.resize(250, 74)
        self.gridLayout = QtWidgets.QGridLayout(IPPortDialog)
        self.gridLayout.setObjectName("gridLayout")
        self.ip_lineEdit = QtWidgets.QLineEdit(IPPortDialog)
        self.ip_lineEdit.setObjectName("ip_lineEdit")
        self.gridLayout.addWidget(self.ip_lineEdit, 0, 1, 1, 1)
        self.port_lineEdit = QtWidgets.QLineEdit(IPPortDialog)
        self.port_lineEdit.setObjectName("port_lineEdit")
        self.gridLayout.addWidget(self.port_lineEdit, 0, 3, 1, 1)
        self.ip_label = QtWidgets.QLabel(IPPortDialog)
        self.ip_label.setObjectName("ip_label")
        self.gridLayout.addWidget(self.ip_label, 0, 0, 1, 1)
        self.port_label = QtWidgets.QLabel(IPPortDialog)
        self.port_label.setObjectName("port_label")
        self.gridLayout.addWidget(self.port_label, 0, 2, 1, 1)
        self.buttonBox = QtWidgets.QDialogButtonBox(IPPortDialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout.addWidget(self.buttonBox, 2, 0, 1, 2)

        self.retranslateUi(IPPortDialog)
        self.buttonBox.rejected.connect(IPPortDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(IPPortDialog)

    def retranslateUi(self, IPPortDialog):
        _translate = QtCore.QCoreApplication.translate
        IPPortDialog.setWindowTitle(_translate("IPPortDialog", "Edit IP"))
        self.ip_label.setText(_translate("IPPortDialog", "IP:"))
        self.port_label.setText(_translate("IPPortDialog", ":"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    IPPortDialog = QtWidgets.QDialog()
    ui = Ui_IPPortDialog()
    ui.setupUi(IPPortDialog)
    IPPortDialog.show()
    sys.exit(app.exec_())
