# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_checkfield.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_FieldWidget(object):
    def setupUi(self, FieldWidget):
        FieldWidget.setObjectName("FieldWidget")
        FieldWidget.resize(400, 104)
        self.gridLayout = QtWidgets.QGridLayout(FieldWidget)
        self.gridLayout.setObjectName("gridLayout")
        self.field_name_label = QtWidgets.QLabel(FieldWidget)
        self.field_name_label.setObjectName("field_name_label")
        self.gridLayout.addWidget(self.field_name_label, 1, 0, 1, 1)
        self.remove_field_toolButton = QtWidgets.QToolButton(FieldWidget)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.remove_field_toolButton.sizePolicy().hasHeightForWidth())
        self.remove_field_toolButton.setSizePolicy(sizePolicy)
        self.remove_field_toolButton.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.remove_field_toolButton.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/resources/mActionDeleteSelected.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.remove_field_toolButton.setIcon(icon)
        self.remove_field_toolButton.setAutoRaise(True)
        self.remove_field_toolButton.setObjectName("remove_field_toolButton")
        self.gridLayout.addWidget(self.remove_field_toolButton, 0, 2, 1, 1)
        self.field_description_label = QtWidgets.QLabel(FieldWidget)
        self.field_description_label.setObjectName("field_description_label")
        self.gridLayout.addWidget(self.field_description_label, 2, 0, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 0, 1, 1, 1)
        self.field_name_lineEdit = QtWidgets.QLineEdit(FieldWidget)
        self.field_name_lineEdit.setObjectName("field_name_lineEdit")
        self.gridLayout.addWidget(self.field_name_lineEdit, 1, 1, 1, 2)
        self.field_description_lineEdit = QtWidgets.QLineEdit(FieldWidget)
        self.field_description_lineEdit.setObjectName("field_description_lineEdit")
        self.gridLayout.addWidget(self.field_description_lineEdit, 2, 1, 1, 2)

        self.retranslateUi(FieldWidget)
        QtCore.QMetaObject.connectSlotsByName(FieldWidget)
        FieldWidget.setTabOrder(self.field_name_lineEdit, self.field_description_lineEdit)
        FieldWidget.setTabOrder(self.field_description_lineEdit, self.remove_field_toolButton)

    def retranslateUi(self, FieldWidget):
        _translate = QtCore.QCoreApplication.translate
        FieldWidget.setWindowTitle(_translate("FieldWidget", "CheckField"))
        self.field_name_label.setText(_translate("FieldWidget", "Field name:"))
        self.field_description_label.setText(_translate("FieldWidget", "Field description:"))
from iquaview_lib import resources_rc


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    FieldWidget = QtWidgets.QWidget()
    ui = Ui_FieldWidget()
    ui.setupUi(FieldWidget)
    FieldWidget.show()
    sys.exit(app.exec_())
