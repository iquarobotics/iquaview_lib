# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_ip_dialog.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_IPDialog(object):
    def setupUi(self, IPDialog):
        IPDialog.setObjectName("IPDialog")
        IPDialog.resize(184, 74)
        self.gridLayout = QtWidgets.QGridLayout(IPDialog)
        self.gridLayout.setObjectName("gridLayout")
        self.buttonBox = QtWidgets.QDialogButtonBox(IPDialog)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout.addWidget(self.buttonBox, 2, 0, 1, 2)
        self.ip_label = QtWidgets.QLabel(IPDialog)
        self.ip_label.setObjectName("ip_label")
        self.gridLayout.addWidget(self.ip_label, 0, 0, 1, 1)
        self.ip_lineEdit = QtWidgets.QLineEdit(IPDialog)
        self.ip_lineEdit.setObjectName("ip_lineEdit")
        self.gridLayout.addWidget(self.ip_lineEdit, 0, 1, 1, 1)

        self.retranslateUi(IPDialog)
        self.buttonBox.rejected.connect(IPDialog.reject)
        QtCore.QMetaObject.connectSlotsByName(IPDialog)

    def retranslateUi(self, IPDialog):
        _translate = QtCore.QCoreApplication.translate
        IPDialog.setWindowTitle(_translate("IPDialog", "Edit IP"))
        self.ip_label.setText(_translate("IPDialog", "IP:"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    IPDialog = QtWidgets.QDialog()
    ui = Ui_IPDialog()
    ui.setupUi(IPDialog)
    IPDialog.show()
    sys.exit(app.exec_())
