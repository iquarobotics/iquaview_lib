# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_entity_options_widget.ui'
#
# Created by: PyQt5 UI code generator 5.14.1
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_EntityOptionsWidget(object):
    def setupUi(self, EntityOptionsWidget):
        EntityOptionsWidget.setObjectName("EntityOptionsWidget")
        EntityOptionsWidget.resize(965, 300)
        self.verticalLayout = QtWidgets.QVBoxLayout(EntityOptionsWidget)
        self.verticalLayout.setObjectName("verticalLayout")
        self.mOptionsSplitter = QtWidgets.QSplitter(EntityOptionsWidget)
        self.mOptionsSplitter.setOrientation(QtCore.Qt.Horizontal)
        self.mOptionsSplitter.setChildrenCollapsible(False)
        self.mOptionsSplitter.setObjectName("mOptionsSplitter")
        self.mOptionsListFrame = QtWidgets.QFrame(self.mOptionsSplitter)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.mOptionsListFrame.sizePolicy().hasHeightForWidth())
        self.mOptionsListFrame.setSizePolicy(sizePolicy)
        self.mOptionsListFrame.setMinimumSize(QtCore.QSize(0, 0))
        self.mOptionsListFrame.setMaximumSize(QtCore.QSize(350, 16777215))
        self.mOptionsListFrame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.mOptionsListFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.mOptionsListFrame.setObjectName("mOptionsListFrame")
        self.verticalLayout_10 = QtWidgets.QVBoxLayout(self.mOptionsListFrame)
        self.verticalLayout_10.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_10.setObjectName("verticalLayout_10")
        self.mOptionsListWidget = QtWidgets.QListWidget(self.mOptionsListFrame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.mOptionsListWidget.sizePolicy().hasHeightForWidth())
        self.mOptionsListWidget.setSizePolicy(sizePolicy)
        self.mOptionsListWidget.setMinimumSize(QtCore.QSize(58, 0))
        self.mOptionsListWidget.setMaximumSize(QtCore.QSize(350, 16777215))
        self.mOptionsListWidget.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.mOptionsListWidget.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOff)
        self.mOptionsListWidget.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.mOptionsListWidget.setIconSize(QtCore.QSize(32, 32))
        self.mOptionsListWidget.setTextElideMode(QtCore.Qt.ElideNone)
        self.mOptionsListWidget.setResizeMode(QtWidgets.QListView.Adjust)
        self.mOptionsListWidget.setWordWrap(True)
        self.mOptionsListWidget.setObjectName("mOptionsListWidget")
        self.verticalLayout_10.addWidget(self.mOptionsListWidget)
        self.mOptionsFrame = QtWidgets.QFrame(self.mOptionsSplitter)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(5)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.mOptionsFrame.sizePolicy().hasHeightForWidth())
        self.mOptionsFrame.setSizePolicy(sizePolicy)
        self.mOptionsFrame.setMinimumSize(QtCore.QSize(0, 0))
        self.mOptionsFrame.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.mOptionsFrame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.mOptionsFrame.setObjectName("mOptionsFrame")
        self.verticalLayout_11 = QtWidgets.QVBoxLayout(self.mOptionsFrame)
        self.verticalLayout_11.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_11.setSpacing(6)
        self.verticalLayout_11.setObjectName("verticalLayout_11")
        self.mOptionsStackedWidget = QtWidgets.QStackedWidget(self.mOptionsFrame)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.mOptionsStackedWidget.sizePolicy().hasHeightForWidth())
        self.mOptionsStackedWidget.setSizePolicy(sizePolicy)
        self.mOptionsStackedWidget.setObjectName("mOptionsStackedWidget")
        self.verticalLayout_11.addWidget(self.mOptionsStackedWidget)
        self.verticalLayout.addWidget(self.mOptionsSplitter)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.status_bar = QtWidgets.QStatusBar(EntityOptionsWidget)
        self.status_bar.setSizeGripEnabled(False)
        self.status_bar.setObjectName("status_bar")
        self.horizontalLayout.addWidget(self.status_bar)
        self.buttonBox = QtWidgets.QDialogButtonBox(EntityOptionsWidget)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Close|QtWidgets.QDialogButtonBox.Save)
        self.buttonBox.setObjectName("buttonBox")
        self.horizontalLayout.addWidget(self.buttonBox)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(EntityOptionsWidget)
        self.mOptionsStackedWidget.setCurrentIndex(-1)
        QtCore.QMetaObject.connectSlotsByName(EntityOptionsWidget)

    def retranslateUi(self, EntityOptionsWidget):
        _translate = QtCore.QCoreApplication.translate
        EntityOptionsWidget.setWindowTitle(_translate("EntityOptionsWidget", "Entity Name"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    EntityOptionsWidget = QtWidgets.QWidget()
    ui = Ui_EntityOptionsWidget()
    ui.setupUi(EntityOptionsWidget)
    EntityOptionsWidget.show()
    sys.exit(app.exec_())
