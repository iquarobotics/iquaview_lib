# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


"""
 Class to handle the loading/saving of the different IquaView configuration parameters
"""

import logging
import os.path
from typing import Any, Dict
from shutil import copytree, copyfile

from PyQt5.QtCore import pyqtSignal, QObject

import yaml

logger = logging.getLogger(__name__)

DEFAULTCONFIG: Dict[str, Any] = {
    "configs_path": "/auv_configs",
    "coordinate_format": "degree",
    "data_output_connection": {},
    "default_project_name": "guiproject.qgs",
    "input_tracks": {},
    "last_auv_config_xml": "~/auv_configs/sparus2_configuration.xml",
    "last_open_project": "",
    "map_zoom_scale": 20000,
    "map_latitude": 41.775,
    "map_longitude": 3.034,
    "master_antenna_ip": "192.168.113.73",
    "style_north_arrow_color": "000000",
    "style_scale_bar_color": "000000",
    "style_all_missions_color": "ff0000",
    "style_all_start_end_markers_color": "cd0000",
    "timeout": 3600,
    "vessel_name": "default",
    "vessel_configuration": {
        "default": {
            "gps_offset_heading": 15.0,
            "gps_offset_x": 1.5,
            "gps_offset_y": 0.0,
            "usbl_magnetic_declination": 1.63,
            "usbl_sound_velocity": 1500,
            "usbl_offset_x": 1.5,
            "usbl_offset_y": 0.0,
            "usbl_offset_z": 0.6,
            "usbl_offset_yaw_deg": 0.0,
            "vessel_length": 6.95,
            "vessel_width": 2.6,
            "vrp_offset_x": -3.4,
            "vrp_offset_y": -1.3,
        }
    },
    "visibility_north_arrow": False,
    "visibility_scale_bar": False,
    "web_server_state": False,
    "web_server_port": 5000,
}


class Config(QObject):
    saved_signal = pyqtSignal()
    loaded_signal = pyqtSignal()

    def __init__(self) -> None:
        super().__init__()
        self.settings: Dict[str, Any] = dict()
        self.loaded_path: str = ""

    def load(self):
        """Create a configuration folder and file if not exist.
        Load a configuration file on settings"""
        home = os.path.expanduser("~")
        iquaview_dir = os.path.join(home, ".iquaview")
        if not os.path.isdir(iquaview_dir):
            # create folder
            os.makedirs(iquaview_dir)

        # todo:ask user for custom configs_path
        auv_configs_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "auv_configs")
        destination_folder = os.path.join(home, "auv_configs")
        if not os.path.isdir(destination_folder):
            copytree(auv_configs_path, destination_folder)
        else:
            for source_file in os.listdir(auv_configs_path):
                try:
                    file = os.path.join(auv_configs_path, source_file)
                    dest_file = os.path.join(destination_folder, source_file)
                    if os.path.isfile(file) and not os.path.exists(dest_file):
                        copyfile(file, dest_file)
                except IOError as e:
                    logger.warning("Error copying file: %s" % e)

        # if app.config not exist or is empty
        if not os.path.isfile(iquaview_dir + "/app.config") or os.stat(iquaview_dir + "/app.config").st_size == 0:
            # create app.config
            f = open(iquaview_dir + "/app.config", "w+")

            # set configs_path on app.config
            DEFAULTCONFIG["configs_path"] = home + "/auv_configs"
            for key, value in DEFAULTCONFIG.items():
                f.write(key + ": " + str(value) + "\n")
            f.close()

        path = os.path.join(iquaview_dir, "app.config")

        with open(path, "r") as f:
            self.settings = yaml.safe_load(f)
            if self.settings is None:
                self.settings = {}
            else:
                self.settings = self.fill_default_items(DEFAULTCONFIG.items(), self.settings)
                self.settings["vessel_configuration"] = self.fill_vessel_configuration(
                    DEFAULTCONFIG.get("vessel_configuration").get("default"), self.settings.get("vessel_configuration")
                )
                self.settings["last_auv_config_xml"] = self.fill_last_auv_config_xml(
                    self.settings.get("last_auv_config_xml")
                )

            self.loaded_path = path

        self.fix_color_formatting()
        self.loaded_signal.emit()

    def fill_default_items(self, items, settings):
        """
        Fill default items
        :param items: default dict of items
        :type items: dict
        :param items: settings configuration dict
        :type items: dict
        :return: return dict
        :rtype: dict
        """
        for key, value in items:
            if settings.get(key) is None or key not in settings:
                settings[key] = value
            elif isinstance(value, dict):
                settings[key] = self.fill_default_items(value.items(), settings[key])
        return settings

    def fill_vessel_configuration(self, default, vessels):
        """
        Fill default vessel params on vessel configuration if missing
        :param default: default vessel parameters
        :type default: dict
        :param vessels: all vessel configurations
        :type vessels: dict
        :return: vessel configurations
        :rtype: dict
        """
        for vessel_key, vessel_value in vessels.items():
            for key, value in default.items():
                if vessel_value.get(key) is None or key not in vessel_value:
                    vessels[vessel_key][key] = value
        return vessels

    def fill_last_auv_config_xml(self, last_auv_config_xml):
        """
        add path to last auv config xml if needed
        :param last_auv_config_xml: last auv configuration xml file
        :type last_auv_config_xml: str
        :return: return last auv configuration xml parameter
        :rtype: str
        """
        new_path = last_auv_config_xml
        if last_auv_config_xml[0:2] != "~/" and last_auv_config_xml[0] != "/":
            new_path = os.path.join("~/auv_configs", last_auv_config_xml)
        return new_path

    def fix_color_formatting(self):
        """
        Fixes color format from a n digit number not longer than 6 to be a '#' + 6 char string (#ABC123)
        """
        color_settings = [
            "style_north_arrow_color",
            "style_scale_bar_color",
            "style_all_missions_color",
            "style_all_start_end_markers_color",
        ]

        for entry in color_settings:
            color = self.settings[entry]

            if isinstance(color, int):  # The color value started with num, convert to 6 char str
                zeros = 6 - len(str(color))  # Number of zeros in front of the color
                color = "#" + "0" * zeros + str(color)
            elif isinstance(color, str) and color[0] != "#":
                color = "#" + color

            self.settings[entry] = color

    def save(self, path=None):
        """
        Save the settings to disk. The last path is used if no path is given.
        :param path:
        :return:
        """
        if not path:
            path = self.loaded_path

        with open(path, "w") as f:
            yaml.dump(data=self.settings, stream=f, default_flow_style=False)

        logger.debug(self.settings)
        self.saved_signal.emit()

    @property
    def settings(self):
        """
        :return: return last saved settings
        :rtype: dict
        """
        return self.__settings

    @settings.setter
    def settings(self, settings):
        """
        Update saved settings dictionary
        :param settings: new settings dict
        :return: dict
        """
        self.__settings = settings
