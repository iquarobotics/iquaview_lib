#!/usr/bin/env python3

# Copyright (c) 2022 Iqua Robotics SL
#
# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.

"""
GPS NMEA-183 GGA sequence driver.
"""
import logging
import sys
from dataclasses import dataclass
from typing import Union

if sys.version_info >= (3, 8):
    from typing import Literal
else:
    from typing_extensions import Literal

logger = logging.getLogger(__name__)


@dataclass
class ConfigSerial:
    """Configuration options for the GPSDriver class associated to a serial port."""
    port: Union[str, None] = None
    baud_rate: int = 9600

    def __eq__(self, other) -> bool:
        """Compare two ConfigSerialGPS objects."""
        return other is not None and self.port == other.port and self.baud_rate == other.baud_rate


@dataclass
class ConfigEthernet:
    """Configuration options for the GPSDriver class associated to an ethernet port."""
    ip: Union[str, None] = None
    port: int = 4000
    protocol: Literal['TCP', 'UDP'] = 'TCP'

    def __eq__(self, other) -> bool:
        """Compare two ConfigEthernetGPS objects."""
        return other is not None and self.ip == other.ip and self.port == other.port and self.protocol == other.protocol


@dataclass
class ConfigGPSDriver:
    """Configuration options for the GPSDriver class."""
    # serial
    gga_serial: Union[ConfigSerial, None] = None
    hdt_serial: Union[ConfigSerial, None] = None
    # ethernet
    gga_ethernet: Union[ConfigEthernet, None] = None
    hdt_ethernet: Union[ConfigEthernet, None] = None
    # offsets
    offset_yaw_deg: float = 0.0  # with respect to front of ship (using 2 GPS antennas)
    declination_deg: float = 0.0  # declination (only for magnetic orientation)

    def from_dict(self, config: dict) -> None:
        """Load from a dictionary."""
        # serial
        if 'gga_serial_port' in config:
            self.gga_serial = ConfigSerial(port=config['gga_serial_port'], baud_rate=config['gga_baud_rate'])
        if 'hdt_serial_port' in config:
            self.hdt_serial = ConfigSerial(port=config['hdt_serial_port'], baud_rate=config['hdt_baud_rate'])
        # ethernet
        if 'gga_ip' in config:
            self.gga_ethernet = ConfigEthernet(
                ip=config['gga_ip'],
                port=config['gga_port'],
                protocol=config['gga_protocol']
            )
        if 'hdt_ip' in config:
            self.hdt_ethernet = ConfigEthernet(
                ip=config['hdt_ip'],
                port=config['hdt_port'],
                protocol=config['hdt_protocol']
            )
        # offsets
        if 'offset_yaw_deg' in config:
            self.offset_yaw_deg = config['offset_yaw_deg']
        if 'declination_deg' in config:
            self.declination_deg = config['declination_deg']

    def to_dict(self) -> dict:
        """Convert to a dictionary."""
        config = {}
        # serial
        if self.gga_serial is not None:
            config['gga_serial_port'] = self.gga_serial.port
            config['gga_baud_rate'] = self.gga_serial.baud_rate
        if self.hdt_serial is not None:
            config['hdt_serial_port'] = self.hdt_serial.port
            config['hdt_baud_rate'] = self.hdt_serial.baud_rate
        # ethernet
        if self.gga_ethernet is not None:
            config['gga_ip'] = self.gga_ethernet.ip
            config['gga_port'] = self.gga_ethernet.port
            config['gga_protocol'] = self.gga_ethernet.protocol
        if self.hdt_ethernet is not None:
            config['hdt_ip'] = self.hdt_ethernet.ip
            config['hdt_port'] = self.hdt_ethernet.port
            config['hdt_protocol'] = self.hdt_ethernet.protocol
        # offsets
        config['offset_yaw_deg'] = self.offset_yaw_deg
        config['declination_deg'] = self.declination_deg
        return config
