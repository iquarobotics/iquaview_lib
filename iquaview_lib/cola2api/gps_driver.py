#!/usr/bin/env python3

# Copyright (c) 2022 Iqua Robotics SL
#
# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.

"""
GPS NMEA-183 GGA sequence driver.
"""

import logging
import select
import socket
import threading
import time
from dataclasses import dataclass
from typing import Union

import serial
from PyQt5.QtCore import pyqtSignal

from iquaview_lib.baseclasses.driverbase import DriverBase
from iquaview_lib.cola2api.config_gps_driver import ConfigGPSDriver, ConfigSerial, ConfigEthernet
from iquaview_lib.connection.settings.gpsconnectionwidget import GPSConnectionWidget
from iquaview_lib.utils.calcutils import wrap_angle_degrees

logger = logging.getLogger(__name__)

MAX_INCREMENT_NEW_DATA: float = 2.0  # seconds
TIMEOUT_ETHERNET_READ: float = 1.0  # seconds
TIMEOUT_ETHERNET_CONNECTION_FAILED: float = 5.0  # seconds
ETHERNET_READ_SIZE: int = 1000  # bytes


@dataclass
class GPSPosition:
    """Position read from a GGA NMEA string."""
    # position
    time: float
    latitude: float
    longitude: float
    altitude: float
    quality: int

    def is_new(self) -> bool:
        """Check if the position data is considered new."""
        if time.time() - self.time < MAX_INCREMENT_NEW_DATA:
            return True
        return False

    def get_position_uncertainty(self) -> float:
        """Get position uncertainty depending on fix quality."""
        # fix
        if self.quality == 1:
            return 4.0
        # dgps, pps, rtk, float rtk
        if self.quality in range(2, 6):
            return 1.0
        # other
        return 1e100

    def get_fix_quality_as_string(self) -> str:
        """Get GPS fix quality as a readable string"""
        fix_quality_list = [
            "Invalid",
            "GPS fix",
            "DGPS fix",
            "PPS fix",
            "RTK",
            "Float RTK",
            "Estimated (dead reckoning)",
            "Manual input mode",
            "Simulation mode"
        ]
        if 0 <= self.quality < len(fix_quality_list):
            return fix_quality_list[self.quality]
        return "Invalid"

    def __str__(self) -> str:
        """String representation of this object."""
        line = f"rawgps LATLON ({self.latitude:.8f}, {self.longitude:.8f})"
        line += f"\nrawgps ALTITUDE {self.altitude:.3f}"
        line += f"\nrawgps QUALITY {self.quality:d}"
        return line


@dataclass
class GPSOrientation:
    """Position read from a HDT NMEA string."""
    time: float
    orientation_deg: float

    def is_new(self) -> bool:
        """Check if the data is considered new."""
        if time.time() - self.time < MAX_INCREMENT_NEW_DATA:
            return True
        return False


def degree_minute_to_decimal_degree(degree_minute: float) -> float:
    """
    Transform degree minutes values into decimal degrees (i.e., 3830.0 --> 38.5)
    :param degree_minute: position in degree minutes
    :return: return position in decimal degrees
    """
    degrees = int(degree_minute / 100.0)
    minutes = degree_minute - (degrees * 100.0)
    return degrees + minutes / 60.0


def is_gga(line: str) -> bool:
    """Check if line corresponds to GGA."""
    if line.startswith('GGA', 3):
        return True
    return False


def is_hdt(line: str) -> bool:
    """Check if line corresponds to HDT."""
    if line.startswith('HDT', 3):
        return True
    return False


def is_gst(line: str) -> bool:
    """Check if line corresponds to GST."""
    if line.startswith('GST', 3):
        return True
    return False


class GPSDriver(DriverBase):
    """Class to handle GPS readings."""
    connect_signal = pyqtSignal()

    def __init__(self, config: ConfigGPSDriver) -> None:
        """Constructor."""
        # init
        super().__init__()
        self.config = config
        # init variables
        self.gps_position = GPSPosition(time=0.0, latitude=0.0, longitude=0.0, altitude=0.0, quality=-1)
        self.gps_orientation = GPSOrientation(time=0.0, orientation_deg=0.0)
        self.stream_gga: Union[serial.Serial, socket.socket, None] = None
        self.stream_hdt: Union[serial.Serial, socket.socket, None] = None
        self.keepgoing: bool = False
        self.thread: Union[threading.Thread, None] = None

    @staticmethod
    def __connect_serial_port(config_serial: ConfigSerial) -> serial.Serial:
        """Connect serial port."""
        port = config_serial.port
        baud_rate = config_serial.baud_rate
        # add /dev/ if necessary
        if port.startswith('tty'):
            port = '/dev/' + port
        # connect
        stream = serial.Serial(port, baud_rate, rtscts=True, dsrdtr=True)
        return stream

    def __connect_ethernet(self, config_ethernet: ConfigEthernet) -> socket.socket:
        """Connect to TCP address."""
        addr = config_ethernet.ip
        port = config_ethernet.port
        protocol = config_ethernet.protocol
        if protocol == 'TCP':
            return self.__connect_tcp_ip_port(addr, port)
        if protocol == 'UDP':
            return self.__connect_udp_ip_port(addr, port)
        # error
        line = f"gpsdrv: no protocol specified for ethernet TCP/UDP (provided: '{protocol}')"
        logger.error(line)
        raise Exception(line)

    @staticmethod
    def __connect_tcp_ip_port(addr: str, port: int) -> socket.socket:
        """Connect to TCP address."""
        stream = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        stream.settimeout(2.0)
        try:
            stream.connect((addr, port))
            stream.setblocking(False)
        except Exception as e:
            stream.close()
            raise Exception(e) from e
        return stream

    @staticmethod
    def __connect_udp_ip_port(addr: str, port: int) -> socket.socket:
        """Connect to UDP address."""
        stream = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        stream.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        stream.settimeout(2.0)
        try:
            stream.bind((addr, port))
        except Exception as e:
            stream.close()
            raise Exception(e) from e
        return stream

    def connect(self):
        """ Connect by serial or ip to GPS"""
        # GGA error check
        if (self.config.gga_serial is not None) and (self.config.gga_ethernet is not None):
            # GGA over serial and ethernet
            line = "gpsdrv: cannot connect to both serial and ethernet for NMEA GGA"
            logger.error(line)
            raise Exception(line)
        if (self.config.gga_serial is None) and (self.config.gga_ethernet is None):
            # no GGA
            line = "gpsdrv: no serial or ethernet specified for NMEA GGA"
            logger.error(line)
            raise Exception(line)
        # HDT error check
        if (self.config.hdt_serial is not None) and (self.config.hdt_ethernet is not None):
            # HDT over serial and ethernet
            line = "gpsdrv: cannot connect to both serial and ethernet for NMEA HDT"
            logger.error(line)
            raise Exception(line)

        # check grouped
        if (self.config.gga_serial is not None) and (self.config.hdt_serial is not None):
            # two serial connections, same device?
            if self.config.gga_serial.port == self.config.hdt_serial.port:
                # connect single
                self.stream_gga = self.__connect_serial_port(self.config.gga_serial)
                self.stream_hdt = self.stream_gga
            else:
                # connect both
                self.stream_gga = self.__connect_serial_port(self.config.gga_serial)
                self.stream_hdt = self.__connect_serial_port(self.config.hdt_serial)
        elif (self.config.gga_ethernet is not None) and (self.config.hdt_ethernet is not None):
            # two ethernet connections, same address?
            if self.config.gga_ethernet == self.config.hdt_ethernet:
                # connect single
                self.stream_gga = self.__connect_ethernet(self.config.gga_ethernet)
                self.stream_hdt = self.stream_gga
            else:
                # connect both
                self.stream_gga = self.__connect_ethernet(self.config.gga_ethernet)
                self.stream_hdt = self.__connect_ethernet(self.config.hdt_ethernet)
        else:
            # serial + ethernet or vice-versa
            # serial GGA
            if self.config.gga_serial is not None:
                self.stream_gga = self.__connect_serial_port(self.config.gga_serial)
            # serial HDT
            if self.config.hdt_serial is not None:
                self.stream_hdt = self.__connect_serial_port(self.config.hdt_serial)
            # ethernet GGA
            if self.config.gga_ethernet is not None:
                self.stream_gga = self.__connect_ethernet(self.config.gga_ethernet)
            # ethernet HDT
            if self.config.hdt_ethernet is not None:
                self.stream_hdt = self.__connect_ethernet(self.config.hdt_ethernet)

        # start reading thread
        self.keepgoing = True
        self.thread = threading.Thread(target=self.read_gps)
        self.thread.daemon = True
        self.thread.start()

        # logger
        gga_mode = "SERIAL" if isinstance(self.stream_gga, serial.Serial) else "ETHERNET"
        if gga_mode == "ETHERNET":
            gga_mode += f" {self.config.gga_ethernet.protocol}"
        logger.debug(f"gpsdrv: GGA mode {gga_mode}")
        if self.stream_hdt is not None:
            hdt_mode = "SERIAL" if isinstance(self.stream_hdt, serial.Serial) else "ETHERNET"
            if hdt_mode == "ETHERNET":
                hdt_mode += f" {self.config.hdt_ethernet.protocol}"
            logger.debug(f"gpsdrv: HDT mode {hdt_mode}")
        logger.debug("gpsdrv: started")
        self.connect_signal.emit()

    def read_gps(self) -> None:
        """Parse GPS sequences."""
        while self.keepgoing:
            # read GGA (or both GGA & HDT if single connection)
            try:
                if isinstance(self.stream_gga, serial.Serial):
                    # serial: read directly
                    line = self.stream_gga.readline().decode()
                    logger.debug(f"<<< gpsdrv: serial: {line:s}")
                    if is_gga(line):
                        self.parse_xxGGA(line)
                    elif self.stream_hdt == self.stream_gga and is_hdt(line):
                        self.parse_xxHDT(line)
                    # elif self.is_gst(line):
                    #     self.parse_xxGST(line)
                elif isinstance(self.stream_gga, socket.socket):
                    # ethernet: check availability
                    rlist, _, _ = select.select([self.stream_gga], [], [], TIMEOUT_ETHERNET_READ)
                    if rlist:
                        # something to read
                        data = self.stream_gga.recv(ETHERNET_READ_SIZE).decode()
                        lines = data.split('\r\n')
                        for line in lines:
                            logger.debug(f"<<< gpsdrv: ethernet: {line:s}")
                            if is_gga(line):
                                self.parse_xxGGA(line)
                            elif self.stream_hdt == self.stream_gga and is_hdt(line):
                                self.parse_xxHDT(line)
                            # elif self.is_gst(line):
                            #     self.parse_xxGST(line)
                # check failed connection
                if self.gps_position.time > 0.0 and (
                        time.time() - self.gps_position.time) > TIMEOUT_ETHERNET_CONNECTION_FAILED:
                    line = "gpsdrv: connection dropped"
                    logger.error(line)
                    self.keepgoing = False
                    self.error_signal.emit(line)
            except UnicodeDecodeError:
                pass
            except serial.SerialException as e:
                line = "gpsdrv: error reading serial gps GGA"
                logger.error(e)
                logger.error(line)
                self.keepgoing = False
                self.error_signal.emit(line)

            # second connection for HDT
            if self.stream_hdt is not None and self.stream_hdt != self.stream_gga:
                try:
                    if isinstance(self.stream_hdt, serial.Serial):
                        # serial: read directly
                        line = self.stream_hdt.readline().decode()
                        logger.debug(f"<<< gpsdrv: serial: {line:s}")
                        if is_hdt(line):
                            self.parse_xxHDT(line)
                    elif isinstance(self.stream_hdt, socket.socket):
                        # ethernet: check availability
                        rlist, _, _ = select.select([self.stream_hdt], [], [], TIMEOUT_ETHERNET_READ)
                        if rlist:
                            # something to read
                            data = self.stream_hdt.recv(ETHERNET_READ_SIZE).decode()
                            lines = data.split('\r\n')
                            for line in lines:
                                logger.debug(f"<<< gpsdrv: ethernet: {line:s}")
                                if is_hdt(line):
                                    self.parse_xxHDT(line)
                    # check failed connection
                    if self.gps_orientation.time > 0.0 and (
                            time.time() - self.gps_orientation.time) > TIMEOUT_ETHERNET_CONNECTION_FAILED:
                        line = "gpsdrv: connection dropped"
                        print(line)
                        logger.error(line)
                        self.keepgoing = False
                        self.error_signal.emit(line)
                except UnicodeDecodeError:
                    pass
                except serial.SerialException as e:
                    line = "gpsdrv: error reading serial gps HDT"
                    logger.error(e)
                    logger.error(line)
                    self.keepgoing = False
                    self.error_signal.emit(line)
            # time.sleep(0.05)

    def parse_xxGGA(self, line: str) -> None:
        """
        Read xxGGA sequence.
        $xxGGA,time,lat,N/S,lon,E/W,fix,sat,hdop,alt,M,hgeo,M,,*chk
        $xxGGA, 082051.800, 3840.3358, N, 00908.4963, W, 1, 9, 0.86, 2.2, M, 50.7, M,, *46
        :param line: line to parse
        """
        try:
            fields = line.split(',')
            if len(fields) >= 6:
                # GGA, time, lat, N/S, lon, E/W
                self.gps_position.time = time.time()
                self.gps_position.latitude = degree_minute_to_decimal_degree(float(fields[2]))
                if fields[3] == 'S':
                    self.gps_position.latitude *= -1.0
                self.gps_position.longitude = degree_minute_to_decimal_degree(float(fields[4]))
                if fields[5] == 'W':
                    self.gps_position.longitude *= -1.0
            if len(fields) >= 7:
                self.gps_position.quality = int(fields[6])
            if len(fields) >= 10:
                self.gps_position.altitude = float(fields[9])
        except Exception as e:
            error = f"gpsdrv: problem parsing $xxGGA: {e}"
            logger.error(error)
            self.warning_signal.emit(error)

    def parse_xxHDT(self, line: str) -> None:
        """
        Parse xxHDT sentence that contains heading in degrees
            $xxHDT,XX.X,T
        :param line: line to parse
        """
        try:
            fields = line.split(',')
            if len(fields[1]) > 1:
                # assume we are only reading DGPS to apply offset_yaw_deg
                self.gps_orientation.orientation_deg = wrap_angle_degrees(float(fields[1]) - self.config.offset_yaw_deg)
                self.gps_orientation.time = time.time()
                logger.debug(f'GPS_HDT: {self.gps_orientation.time:.3f} {fields[1]:s} deg')
                logger.debug(
                    'GPS_HDT_with_offset: '
                    f'{self.gps_orientation.time:.3f} '
                    f'{self.gps_orientation.orientation_deg:.3f} deg'
                )
        except Exception as e:
            error = f"gpsdrv: problem parsing $xxHDT: {e}"
            logger.error(error)
            self.warning_signal.emit(error)

    def parse_xxGST(self, line: str) -> None:
        """
        Read xxGST sequence that contains errors.
        $xxGST,time,rms,major,minor,orientation,lat,lon,height,*chk
        :param line: line to parse
        """
        try:
            fields = line.split(',')
            if len(fields) >= 9:
                logger.info(f'rawGST: {line}')
                logger.info(f'rawGST rms {fields[2]}')
                logger.info(f'rawGST error MAJ/MIN/ORI ({fields[3]}, {fields[4]}, {fields[5]})')
                logger.info(f'rawGST error LAT/LON/HEI ({fields[6]}, {fields[7]}, {fields[8]})')
        except Exception as e:
            logger.error(f"gpsdrv: problem parsing $xxGST: {e}")

    def get_position(self) -> GPSPosition:
        """Return last gathered position."""
        return self.gps_position

    def get_orientation(self) -> GPSOrientation:
        """Return last gathered position."""
        return self.gps_orientation

    def get_configuration_widget(self) -> GPSConnectionWidget:
        """
        get driver configuration widget
        :return: return GPS Configuration Connection Widget
        :rtype: GPSConnectionWidget
        """
        gps_connection_widget = GPSConnectionWidget()
        gps_connection_widget.set_configuration(self.config)

        if self.config.hdt_serial is not None:
            gps_connection_widget.heading_serialPortRadioButton.toggled.emit(True)
            gps_connection_widget.heading_serialPortRadioButton.setChecked(True)
            gps_connection_widget.heading_TCPRadioButton.setChecked(False)
        else:
            gps_connection_widget.heading_serialPortRadioButton.toggled.emit(False)
            gps_connection_widget.heading_serialPortRadioButton.setChecked(False)
            gps_connection_widget.heading_TCPRadioButton.setChecked(True)

        if self.config.gga_serial is not None:
            gps_connection_widget.serialPortRadioButton.toggled.emit(True)
            gps_connection_widget.serialPortRadioButton.setChecked(True)
            gps_connection_widget.ethernet_RadioButton.setChecked(False)
            gps_connection_widget.same_radioButton.setChecked(self.config.gga_serial == self.config.hdt_serial)
            gps_connection_widget.configure_radioButton.setChecked(
                self.config.gga_serial != self.config.hdt_serial
            )
            gps_connection_widget.disabled_radioButton.setChecked(
                self.config.hdt_serial is None and self.config.hdt_ethernet is None
            )
        else:
            gps_connection_widget.serialPortRadioButton.toggled.emit(False)
            gps_connection_widget.ethernet_RadioButton.toggled.emit(True)
            gps_connection_widget.serialPortRadioButton.setChecked(False)
            gps_connection_widget.ethernet_RadioButton.setChecked(True)
            gps_connection_widget.same_radioButton.setChecked(self.config.gga_ethernet == self.config.hdt_ethernet)
            gps_connection_widget.configure_radioButton.setChecked(
                self.config.gga_ethernet != self.config.hdt_ethernet
            )
            gps_connection_widget.disabled_radioButton.setChecked(
                self.config.hdt_serial is None and self.config.hdt_ethernet is None
            )
        return gps_connection_widget

    def set_configuration(self, configuration: dict) -> None:
        self.config = ConfigGPSDriver()
        self.config.from_dict(configuration)

    def to_dict(self) -> dict:
        """ Return a dictionary representation of the driver. """
        return self.config.to_dict()

    def close(self) -> None:
        """Close the open connections."""
        # stop loop
        self.keepgoing = False
        # wait for loop thread to join
        try:
            if self.thread is not None:
                self.thread.join(2.0)
        except RuntimeError as e:
            logger.error(str(e.args[0]))
        # close streams
        if self.stream_gga is not None:
            self.stream_gga.close()
        if self.stream_hdt is not None:
            self.stream_hdt.close()
