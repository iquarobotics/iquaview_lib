#!/usr/bin/env python
# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


import argparse
import os

from iquaview_lib.cola2api import mission_types, mission_types_v1


def convert_mission(input_file, output_file):
    # Load old mission
    mission_old = mission_types_v1.Mission()
    mission_old.load_mission(input_file)

    # Create new mission
    mission = mission_types.Mission()

    # Convert all steps from the old mission to the new mission
    for i in range(0, mission_old.size()):
        # Create a maneuver from the old maneuver
        step_old = mission_old.get_step(i)
        maneuver_old = step_old.get_maneuver()
        if maneuver_old.get_maneuver_type() == mission_types_v1.WAYPOINT_MANEUVER:
            # Old waypoints are converted to gotos. Old waypoints are converted to sections if in the middle
            # of the mission, taking as the initial position the final position of the previous mission step. the
            # only goto that is allowed is in the first step of the mission
            if i == 0:
                maneuver = mission_types.MissionGoto()
                maneuver.final_latitude = maneuver_old.position.latitude
                maneuver.final_longitude = maneuver_old.position.longitude
                if maneuver_old.position.altitude_mode:
                    maneuver.final_depth = 0.0
                    maneuver.final_altitude = maneuver_old.position.z
                    maneuver.heave_mode = mission_types.HEAVE_MODE_ALTITUDE
                else:
                    maneuver.final_depth = maneuver_old.position.z
                    maneuver.final_altitude = 0.0
                    maneuver.heave_mode = mission_types.HEAVE_MODE_DEPTH
                maneuver.surge_velocity = maneuver_old.speed
                maneuver.tolerance_xy = min(maneuver_old.tolerance.x, maneuver_old.tolerance.y)
                maneuver.no_altitude_goes_up = True
            else:
                print("Converting step " + str(i + 1) + " from the old waypoint to section")
                maneuver = mission_types.MissionSection()
                previous_step = mission.get_step(i - 1)
                previous_maneuver = previous_step.get_maneuver()
                maneuver.initial_latitude = previous_maneuver.final_latitude
                maneuver.initial_longitude = previous_maneuver.final_longitude
                maneuver.final_latitude = maneuver_old.position.latitude
                maneuver.final_longitude = maneuver_old.position.longitude
                if maneuver_old.position.altitude_mode:
                    maneuver.initial_depth = 0.0
                    maneuver.final_depth = 0.0
                    maneuver.final_altitude = maneuver_old.position.z
                    maneuver.heave_mode = mission_types.HEAVE_MODE_ALTITUDE
                else:
                    maneuver.initial_depth = previous_maneuver.final_depth
                    maneuver.final_depth = maneuver_old.position.z
                    maneuver.final_altitude = 0.0
                    maneuver.heave_mode = mission_types.HEAVE_MODE_DEPTH
                maneuver.surge_velocity = maneuver_old.speed
                maneuver.tolerance_xy = min(maneuver_old.tolerance.x, maneuver_old.tolerance.y)
                maneuver.no_altitude_goes_up = True
        elif maneuver_old.get_maneuver_type() == mission_types_v1.SECTION_MANEUVER:
            # Old sections are still sections in the new version. If a section is found in the first step, it is
            # converted to a goto
            if i == 0:
                print("First step is a section! Converting the step to goto")
                maneuver = mission_types.MissionGoto()
                maneuver.final_latitude = maneuver_old.final_position.latitude
                maneuver.final_longitude = maneuver_old.final_position.longitude
                if maneuver_old.initial_position.altitude_mode or maneuver_old.final_position.altitude_mode:
                    maneuver.final_depth = 0.0
                    maneuver.final_altitude = maneuver_old.final_position.z
                    maneuver.heave_mode = mission_types.HEAVE_MODE_ALTITUDE
                else:
                    maneuver.final_depth = maneuver_old.final_position.z
                    maneuver.final_altitude = 0.0
                    maneuver.heave_mode = mission_types.HEAVE_MODE_DEPTH
                maneuver.surge_velocity = maneuver_old.speed
                maneuver.tolerance_xy = min(maneuver_old.tolerance.x, maneuver_old.tolerance.y)
                maneuver.no_altitude_goes_up = True
            else:
                maneuver = mission_types.MissionSection()
                maneuver.initial_latitude = maneuver_old.initial_position.latitude
                maneuver.initial_longitude = maneuver_old.initial_position.longitude
                maneuver.final_latitude = maneuver_old.final_position.latitude
                maneuver.final_longitude = maneuver_old.final_position.longitude
                if maneuver_old.initial_position.altitude_mode or maneuver_old.final_position.altitude_mode:
                    maneuver.initial_depth = 0.0
                    maneuver.final_depth = 0.0
                    maneuver.final_altitude = maneuver_old.final_position.z
                    maneuver.heave_mode = mission_types.HEAVE_MODE_ALTITUDE
                else:
                    maneuver.initial_depth = maneuver_old.initial_position.z
                    maneuver.final_depth = maneuver_old.final_position.z
                    maneuver.final_altitude = 0.0
                    maneuver.heave_mode = mission_types.HEAVE_MODE_DEPTH
                maneuver.surge_velocity = maneuver_old.speed
                maneuver.tolerance_xy = min(maneuver_old.tolerance.x, maneuver_old.tolerance.y)
                maneuver.no_altitude_goes_up = True
        elif maneuver_old.get_maneuver_type() == mission_types_v1.PARK_MANEUVER:
            # Old parks are still parks in the new version
            maneuver = mission_types.MissionPark()
            maneuver.final_latitude = maneuver_old.position.latitude
            maneuver.final_longitude = maneuver_old.position.longitude
            if maneuver_old.position.altitude_mode:
                maneuver.final_depth = 0.0
                maneuver.final_altitude = maneuver_old.position.z
                maneuver.heave_mode = mission_types.HEAVE_MODE_ALTITUDE
            else:
                maneuver.final_depth = maneuver_old.position.z
                maneuver.final_altitude = 0.0
                maneuver.heave_mode = mission_types.HEAVE_MODE_DEPTH
            maneuver.final_yaw = 0.0
            maneuver.use_yaw = False
            maneuver.surge_velocity = maneuver_old.speed
            maneuver.time = maneuver_old.time
            maneuver.no_altitude_goes_up = True

        # Create step using the generated maneuver, and adding the actions
        step = mission_types.MissionStep()
        step.add_maneuver(maneuver)
        for action_old in step_old.get_actions():
            step.add_action(action_old)  # They are compatible

        # Add the step to the mission
        mission.add_step(step)

    # Write new mission to file
    mission.write_mission(output_file)


if __name__ == "__main__":
    # Parse arguments
    parser = argparse.ArgumentParser(description="Convert COLA2 mission .xml file from version 1.0 to version 2.0.")
    parser.add_argument('-i', '--input', type=str, help="input mission .xml file", required=True)
    parser.add_argument('-o', '--output', type=str, help="output mission .xml file", required=True)
    args = parser.parse_args()

    # Check output file
    if os.path.exists(args.output):
        print("Error: output file already exists")
        exit()

    convert_mission(args.input, args.output)
