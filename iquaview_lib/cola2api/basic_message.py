#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (c) 2022 Iqua Robotics SL - All Rights Reserved
#
# This file is subject to the terms and conditions defined in file
# 'LICENSE.txt', which is part of this source code package.


"""
Message definition with serialization and deserialization.
Message transmitted through modems.
"""

import struct
from dataclasses import dataclass
from enum import IntEnum

try:
    from typing import TypedDict
except ImportError:
    from typing_extensions import TypedDict

import cola2.comms.altitude as alt
import cola2.comms.elapsed_time as elt


class ElapsedTime(IntEnum):
    """
    Define values for elapsed time encoding
    """
    # TODO: Removes when cola2 exports this values
    ELAPSED_TIME_LOOKUP_SIZE = 176  # size of the lookup table
    ELAPSED_TIME_MAXIMUM = 253  # value when elapsed_time is bigger than the biggest value in lookup table
    ELAPSED_TIME_INVALID = 254  # value when not elapsed_time data is available
    ELAPSED_TIME_ENCODING_ERROR = 255  # value for encoding errors, should not appear


class BasicMessageDict(TypedDict):
    """ Basic message dictionary. """
    mtype: str
    time: float
    latitude: float
    longitude: float
    depth: float
    heading_accuracy: float
    command_error: int
    altitude: float
    elapsed_time: int


@dataclass
class BasicMessage:
    """Basic message that is sent and received over acoustic communications."""
    mtype: str = 'I'  # modem type('U': USBL, 'G': Girona500, 'S': Sparus)
    time: float = 0.0  # time when the data is obtained
    latitude: float = 0.0  # robot position latitude
    longitude: float = 0.0  # robot position longitude
    depth: float = 0.0  # robot position depth
    heading_accuracy: float = 0.0  # robot yaw heading or USBL measurement accuracy
    command_error: int = 0  # command from USBL or status_code from robot
    altitude: float = -1.0  # altitude of the robot
    elapsed_time: int = -1  # elapsed time in the robot

    def is_valid(self) -> bool:
        """Check if the message is valid."""
        return self.mtype != 'I' and self.time != 0.0

    def get_navigation_string(self) -> str:
        """Get navigation data as string."""
        return (f"{self.latitude:.6f}, {self.longitude:.6f}, {self.depth:.2f}, "
                f"{self.heading_accuracy:.2f} deg, {self.altitude:.2f}")

    def as_dict(self) -> BasicMessageDict:
        """Obtain as a dictionary, useful for JSON serializing."""
        mtype = self.mtype
        if isinstance(mtype, bytes):
            mtype = mtype.decode()
        return {
            'mtype': mtype,
            'time': self.time,
            'latitude': self.latitude,
            'longitude': self.longitude,
            'depth': self.depth,
            'heading_accuracy': self.heading_accuracy,
            'command_error': self.command_error,
            'altitude': self.altitude,
            'elapsed_time': self.elapsed_time
        }


class BasicMessageSerializer:
    """Class to serialize/deserialize acoustic messages."""

    def __init__(self):
        """Initialize the serializer."""
        self.FMT = b'<1s 1d 2d 2f 1I 2B'  # 39 bytes
        self.SIZE = struct.calcsize(self.FMT)

    def serialize(self, msg: BasicMessage) -> bytes:
        """
        Serialize a basic message.

        :param msg: Message to serialize
        :return: Serialized message
        """
        mtype = msg.mtype.encode()
        enc_altitude = alt.encode(msg.altitude)
        enc_elapsed_time = elt.encode(msg.elapsed_time)
        return struct.pack(self.FMT, mtype, msg.time, msg.latitude, msg.longitude, msg.depth,
                           msg.heading_accuracy, msg.command_error, enc_altitude, enc_elapsed_time)

    def deserialize(self, mser: bytes) -> BasicMessage:
        """
        Deserialize a basic message
        :param mser: Serialized message
        :return: Deserialized message
        """
        # Check length
        try:
            if len(mser) == self.SIZE:
                tmp = struct.unpack(self.FMT, mser)
                msg = BasicMessage()
                msg.mtype = tmp[0]
                msg.time = tmp[1]
                msg.latitude = tmp[2]
                msg.longitude = tmp[3]
                msg.depth = tmp[4]
                msg.heading_accuracy = tmp[5]
                msg.command_error = tmp[6]
                msg.altitude = alt.decode(tmp[7])
                msg.elapsed_time = elt.decode(tmp[8])
                if (tmp[8] + 1) < ElapsedTime.ELAPSED_TIME_LOOKUP_SIZE:
                    # we add 1 to the elapsed time to be pessimistic
                    # prefer to think that there is less time left than there really is.
                    msg.elapsed_time = elt.decode(tmp[8] + 1)
                return msg

            # invalid length
            line = f"basic_message.py: Message length invalid {len(mser):d} (recv) != {self.SIZE:d} (fmt)"
            print(line)
            return BasicMessage()
        except Exception as e:
            print('basic_message.py: Error deserializing basic message!')
            print(e)
            return BasicMessage()
