# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.

"""
This module contains the EntitiesManager class, which is the class that manages the entities.
"""
import logging
import uuid
from typing import Any, Dict, Optional, Tuple

from PyQt5.QtCore import pyqtSignal, QObject, Qt, pyqtSlot
from PyQt5.QtWidgets import QMessageBox, QWidget
from qgis.core import QgsPointXY, QgsGeometry
from qgis.gui import QgsMapCanvas

from iquaview_lib.baseclasses.driverbase import DriverBase
from iquaview_lib.baseclasses.entitybase import EntityBase
from iquaview_lib.canvasdrawer.gnssentity import GNSSEntity
from iquaview_lib.cola2api.gps_driver import ConfigGPSDriver, GPSDriver
from iquaview_lib.config import Config
from iquaview_lib.connection.settings.gpsconnectionwidget import GPSConnectionWidget
from iquaview_lib.baseclasses.canvasdrawerstyle import CanvasDrawerStyle, CanvasDrawerStyleConfigWidget
from iquaview_lib.canvasdrawer.canvastrackstyle import CanvasTrackStyle
from iquaview_lib.canvasdrawer.canvaspolygonstyle import CanvasPolygonStyle

CONNECTION_MENU = 0
STYLE_MENU = 1

logger = logging.getLogger(__name__)

SettingsType = Tuple[int, Dict[str, Any]]


class EntitiesManager(QObject):
    """
    this class is used to manage the entities (Entity classes)
    """

    entity_added_signal = pyqtSignal(EntityBase, bool)
    entity_updated_signal = pyqtSignal([EntityBase], [EntityBase, bool])  # [entity] or [entity, override]
    entity_removed_signal = pyqtSignal(EntityBase)

    def __init__(self, config: Config, canvas: QgsMapCanvas, parent: Optional[QObject] = None) -> None:
        """
        Constructor
        :param config: iquaview configuration
        :type config: Config
        :param canvas: Canvas to draw objects
        :type canvas: QgsMapCanvas
        :param parent: parent
        :type parent: QObject
        """
        super().__init__(parent)
        self.config = config
        self.canvas = canvas

        self.entities_dict = {}
        self.count = 0

        self.center_dict = {}

        self.loading_entities = False
        self.previous_entities_order = {}

        self.entity_updated_signal[EntityBase].connect(self.register_webserver_data_source)
        self.entity_updated_signal[EntityBase, bool].connect(self.register_webserver_data_source)

        self.remove_duplicates()

    def remove_duplicates(self) -> None:
        """
        If an unexpected duplicate appears in the dictionary,
        the second item with the same name in the permanent field will be deleted.
        """
        to_remove = []
        input_tracks = self.config.settings.get("input_tracks")
        if input_tracks and isinstance(input_tracks, dict):
            for key, item in input_tracks.items():
                if key not in to_remove and item.get("permanent", None) is not None:
                    for key_2, item_2 in input_tracks.items():
                        if (
                            key != key_2
                            and item["permanent"] == item_2["permanent"]
                            and key not in to_remove
                            and key_2 not in to_remove
                        ):
                            to_remove.append(key_2)
            for remove_key in to_remove:
                del self.config.settings["input_tracks"][remove_key]
            self.config.save()

    def new_entity(self) -> None:
        """
        Creates a new GNSS Entity and adds it to the entities set
        """
        gps_widget = GPSConnectionWidget()
        gps_widget.heading_serialPortRadioButton.toggled.emit(True)
        gps_widget.heading_serialPortRadioButton.setChecked(True)
        gps_widget.heading_TCPRadioButton.setChecked(False)
        gps_widget.configure_radioButton.setChecked(False)
        gps_widget.same_radioButton.setChecked(True)
        gps_widget.serialPortRadioButton.toggled.emit(True)
        gps_widget.serialPortRadioButton.setChecked(True)
        gps_widget.ethernet_RadioButton.setChecked(False)

        config = gps_widget.get_configuration()
        driver = GPSDriver(config)

        entity = self.add_entity(
            key=0,
            position=0,
            name="",
            visible=True,
            driver=driver,
            style=CanvasTrackStyle(color_hex_str=CanvasDrawerStyleConfigWidget().get_random_color()),
            connect_when_starts=False,
            permanent=None,
        )

        self.edit_entity(entity, True)

    @staticmethod
    def get_new_identifier() -> int:
        """
        Get a new free identifier
        :return: return a new identifier
        """
        return uuid.uuid4().int

    def get_new_position(self) -> int:
        """
        Get a free position
        :return: new position
        :rtype: int
        """
        # if exists, rename key
        count = self.count
        while count in self.entities_dict:
            count += 1

        self.count = count
        return self.count

    def get_new_name(self, position: int) -> str:
        """
        Given a position, generate new name. If name exists, create new one
        :param position: position
        :type position: int
        :return: name
        :rtype: str
        """
        same = True
        while same:
            same = False
            for idx, entity in self.entities_dict.items():
                if entity.name == f"entity_{position}":
                    same = True
                    position += 1

        return f"entity_{position}"

    def add_entity(
        self,
        key: int,
        position: int,
        name: str,
        visible: bool,
        driver: DriverBase,
        style: CanvasDrawerStyle,
        connect_when_starts: bool,
        permanent: Optional[str] = None,
    ) -> GNSSEntity:
        """
        Add entity to the entities set
        :param key: key identifier
        :type key: int
        :param position: entity position
        :type position: int
        :param name: entity name
        :type name: str
        :param visible: track visibility
        :type visible: bool
        :param driver: entity driver
        :type driver: DriverBase
        :param style: track style configuration
        :type style: CanvasDrawerStyle
        :param connect_when_starts: parameter to define if the entity connects at startup
        :type connect_when_starts: bool
        :param permanent:internal identifier to recognize the permanent entity
        :type permanent: Union[str, None]
        :return: returns Entity
        :rtype: GNSSEntity
        """
        key = self.get_new_identifier()
        if position in self.entities_dict:
            position = self.get_new_position()

        if not name:
            name = self.get_new_name(position)

        entity = GNSSEntity(
            id=key,
            position=position,
            name=name,
            visible=visible,
            driver=driver,
            style=style,
            config=self.config,
            canvas=self.canvas,
            permanent=permanent,
            connect_when_starts=connect_when_starts,
        )

        self.add_and_connect_signals(entity, self.loading_entities)

        return entity

    def edit_entity(
        self, entity: EntityBase, new_entity: bool = False, widget_to_view: Optional[QWidget] = None
    ) -> None:
        """
        Opens a dialog to configure the entity
        :param entity: Entity to be edited
        :type entity: EntityBase
        :param new_entity: parameter to define if the entity is new
        :type new_entity: bool
        :param widget_to_view: widget to be viewed
        :type widget_to_view: QWidget
        """
        entity.edit(new_entity, widget_to_view, self.parent())

    def append_entity(self, entity: EntityBase) -> bool:
        """
        Append an entity to the entity list
        :param entity:entity to add
        :return: return True if entity was appended correctly, otherwise false
        """
        try:
            self.loading_entities = True
            entity.identifier = self.get_new_identifier()
            if entity.position in self.entities_dict:
                entity.position = self.get_new_position()
            self.add_and_connect_signals(entity, True)
            self.loading_entities = False
        except Exception as e:
            logger.error(f"error when adding the new entity: {e}")
            self.loading_entities = True
            return False

        return True

    def add_and_connect_signals(self, entity: EntityBase, loading: bool = True) -> None:
        """
        Add an entity to the entity set and connect signals
        :param entity: entity to add and connect signals
        :type entity: EntityBase
        :param loading: True if the entity is being loaded, False otherwise
        :type loading: bool
        :return: None
        """
        entity.entity_updated_signal.connect(lambda: self.entity_updated_signal.emit(entity))
        entity.entity_updated_signal[bool].connect(
            lambda state, x=entity: self.entity_updated_signal[EntityBase, bool].emit(x, state)
        )
        entity.entity_remove_signal.connect(lambda: self.remove_entity(entity))
        entity.entity_save_signal.connect(self.save_entity)
        entity.canvasdrawer.position_signal.connect(self.center_to_position)
        entity.canvasdrawer.zoom_position_signal.connect(self.zoom_to_position)
        entity.canvasdrawer.unlock_signal.connect(self.unlock_position)
        self.entities_dict[entity.position] = entity
        self.entity_added_signal.emit(entity, loading)
        entity.hide()

    def connect_entity(self, entity: EntityBase) -> None:
        """
        Connect or disconnect entity driver
        :param entity: entity to connect
        :type entity: EntityBase
        :return: None
        """
        try:
            if not entity.is_connected():
                entity.connect()
            else:
                entity.close()

        except Exception as e:
            logger.error(f"Connection with driver could not be established. {e}")
            message_box = QMessageBox(
                QMessageBox.Warning,
                "Connection Failed",
                f"Connection with driver could not be established. \n{e}",
                QMessageBox.Close,
                self.parent(),
            )
            message_box.setWindowModality(Qt.WindowModal)
            message_box.exec_()

    def ask_and_remove_entity(self, entity: EntityBase) -> None:
        """
        Ask to remove the entity
        :param entity: entity to remove
        :return: None
        """

        reply = QMessageBox.question(
            self.parent(),
            "Remove Entity",
            f"Are you sure you want to remove {entity.name}?",
            QMessageBox.Yes,
            QMessageBox.No,
        )
        if reply == QMessageBox.Yes:
            self.remove_entity(entity)

    def remove_entity(self, entity: EntityBase) -> None:
        """
        Removes entity from the entities set
        :param entity: entity to be removed
        :type entity: EntityBase
        :return: None
        """

        entity.remove()
        del self.entities_dict[entity.position]
        self.entity_removed_signal.emit(entity)

    def zoom_track(self, entity: EntityBase) -> None:
        """
        Zoom to the track of the entity
        :param entity: Entity
        :type entity: EntityBase
        """
        entity.zoom_track()

    def lock_track(self, entity: EntityBase) -> None:
        """
        Lock the track position of the entity
        :param entity: Entity
        :type entity: EntityBase
        :return:
        """
        entity.lock_track()

    def clear_track(self, entity: EntityBase) -> None:
        """
        Clear the track position of the entity
        :param entity: Entity
        :type entity: EntityBase
        :return:
        """
        entity.clear_track()

    def save_track(self, entity: EntityBase) -> None:
        """
        Save the entire track of the entity
        :param entity: Entity
        :type entity: EntityBase
        :return:
        """
        entity.export_track()

    def save_entity(self, widgets: dict, ui) -> None:
        """
        Saves the entity with the parameters set in the widgets' driver_widget and style_widget
        :param widgets: widgets dict
        :param ui: style widget
        :return:
        """
        try:
            ui.status_bar.showMessage("Saving...")

            entity = self.sender()
            valid = True
            for idx, widget in widgets.items():
                if not widget.is_valid():
                    valid = False
                    break
            if valid:
                saved = entity.save_changes(widgets)
                ui.saved = saved
                if ui.saved:
                    ui.status_bar.showMessage(f"{entity.name} saved", 1500)

            if not valid or not saved:
                QMessageBox.critical(
                    self.parent(),
                    "Failed to save the parameters",
                    "The parameters could not be saved. Review them before closing the dialog.",
                    QMessageBox.Close
                )
        except OSError as e:
            # OSError only if it is a connection error,
            # some entity trying to reconnect after saving the parameters (USBL)
            # we assume it has been saved and the error is after saving
            QMessageBox.critical(
                self.parent(),
                "Error occurred",
                f"Parameters saved correctly, but an error occurred. \n{e}",
                QMessageBox.Close,
            )
        except Exception as e:
            QMessageBox.critical(self.parent(), "Unexpected error occurred", f"{e}", QMessageBox.Close)

    def get_previous_entities_order(self) -> dict:
        """
        Get the previous entities order
        :return: list of entities
        """
        return self.previous_entities_order

    def load_entities(self) -> None:
        """
        Load entities from configuration file
        """
        self.loading_entities = True
        self.previous_entities_order = {}
        if self.config.settings.get("input_tracks") and isinstance(self.config.settings.get("input_tracks"), dict):
            for key, item in self.config.settings.get("input_tracks").items():
                self.previous_entities_order[item["name"]] = key
            for key, item in self.config.settings.get("input_tracks").items():
                if item["permanent"] is None:
                    self.load_entity(key, item)

        self.loading_entities = False

    def load_entity(self, position: int, values: dict) -> None:
        """
        Load the entity with the configuration defined in values
        :param position: entity position
        :type position: int
        :param values: entity's values defined in a dict
        :type values: dict
        :return:
        """

        name = values["name"]
        identifier = values.get("id", 0)
        visible = values.get("visible") if (values.get("visible") is not None) else False
        connect_when_starts = values["connect_when_starts"]
        permanent = values["permanent"]

        driver_values = values["driver"]

        style_dict = {}
        if "canvasdrawer" in values:
            style_dict = values["canvasdrawer"]["style"]
        elif "style" in values:
            style_dict = values["style"]
            style_dict["color_hex_str"] = style_dict["color"]
            style_dict.pop("color")
            values.pop("style")

        config = ConfigGPSDriver()
        config.from_dict(driver_values)

        driver = GPSDriver(config)

        if CanvasTrackStyle().check_dict(style_dict):
            style = CanvasTrackStyle()
            style.from_dict(style_dict)
        elif CanvasPolygonStyle().check_dict(style_dict):
            style = CanvasPolygonStyle()
            style.from_dict(style_dict)
        else:
            style = None
            logger.error(f"Error loading entity with style: {style_dict}")

        if style != None:
            entity = self.add_entity(
                key=identifier,
                position=position,
                name=name,
                visible=visible,
                driver=driver,
                style=style,
                connect_when_starts=connect_when_starts,
                permanent=permanent,
            )
            entity.save_changes(entity.properties_widgets())
        else:
            logger.error(f"Error loading entity: {values}")

    def get_entity(self, permanent: str) -> Optional[EntityBase]:
        """
         Return entity with permanent name
        :param permanent: identifier
        :type permanent: Union[int, None]
        :return:
        """
        for key in self.entities_dict:
            entity: EntityBase = self.entities_dict[key]
            if entity.permanent == permanent:
                return entity
        return None

    def get_entity_settings(self, permanent: str) -> Optional[SettingsType]:
        """
        Return settings if entity with permanent name exists, otherwise None.
        :param permanent: permanent id of entity
        """
        data: Optional[Dict[int, Dict[str, Any]]] = self.config.settings.get("input_tracks")
        if data and isinstance(data, dict):
            for key in data:
                item: Dict[str, Any] = data[key]
                p_id: Optional[str] = item.get("permanent")
                if p_id is not None and permanent == p_id:
                    return key, item
        return None

    def zoom_to_position(self) -> None:
        """
        If several points exist, zooms to all of them.
        If only one point exists, zooms to it
        """
        points = []

        for key, value in self.center_dict.items():
            if value not in points:
                points.append(value)

        multipoint_geom = QgsGeometry.fromMultiPointXY(points)
        self.canvas.setExtent(multipoint_geom.boundingBox())

        if len(points) > 1:
            self.canvas.zoomScale(self.canvas.scale() * 1.2)

        self.canvas.refresh()

    def center_to_position(self, position: QgsPointXY) -> None:
        """
        Add position to center lock dict
        :param position: Position to update to
        :type position: QgsPointXY
        """
        sender = self.sender()
        points = []
        self.center_dict[sender] = position

        scale = self.canvas.scale()
        for key, value in self.center_dict.items():
            if value not in points:
                points.append(value)

        multipoint_geom = QgsGeometry.fromMultiPointXY(points)
        self.canvas.setExtent(multipoint_geom.boundingBox())

        if len(points) > 1:
            if scale < self.canvas.scale() * 1.2:
                scale = self.canvas.scale() * 1.2
            self.canvas.zoomScale(scale)

        self.canvas.refresh()

    def unlock_position(self) -> None:
        """
        Remove track from center lock dict
        """
        sender = self.sender()
        self.center_dict.pop(sender)

    @pyqtSlot(EntityBase)
    @pyqtSlot(EntityBase, bool)
    def register_webserver_data_source(self, entity: EntityBase, override: bool = False) -> None:
        """
        Register the data source entity on the web server
        :param entity: entity to register
        :type entity: EntityBase
        :param override: override existing data source
        :type override: bool
        """
        self.parent().web_server.register_data_source(entity.web_server_register(), override=override)
