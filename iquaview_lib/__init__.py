# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.

version_info = (24, 1, 4)

PLUGIN_NAME = "iquaview_lib"

__version__ = ".".join(map(str, version_info))
