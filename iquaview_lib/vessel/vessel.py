#!/usr/bin/env python
"""
Copyright (c) 2020 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.
"""

import logging
from importlib import util

from PyQt5.QtCore import QObject, Qt, pyqtSignal
from PyQt5.QtGui import QColor, QIcon
from PyQt5.QtWidgets import QMenuBar, QAction, QMenu, QToolBar
from qgis.gui import QgsMapCanvas

from iquaview_lib.canvasdrawer.canvastrackstyle import CanvasTrackStyle
from iquaview_lib.cola2api.gps_driver import ConfigGPSDriver, GPSDriver, GPSPosition, GPSOrientation
from iquaview_lib.config import Config
from iquaview_lib.connection.settings.gpsconnectionwidget import GPSConnectionWidget
from iquaview_lib.entity.entitiesmanager import EntitiesManager
from iquaview_lib.vessel.vesselentity import VesselEntity

logger = logging.getLogger(__name__)


class Vessel(QObject):
    """
    Class to handle the vessel and show its track on the map.
    """

    vessel_position_signal = pyqtSignal(GPSPosition)
    vessel_orientation_signal = pyqtSignal(GPSOrientation)

    def __init__(
        self,
        entities_manager: EntitiesManager,
        canvas: QgsMapCanvas,
        config: Config,
        menubar: QMenuBar,
        before_action: QAction,
        before_toolbar: QToolBar,
        parent=None,
    ):
        """
        Init of the object Vessel
        :param entities_manager: class is used to manage the entities
        :type entities_manager: EntitiesManager
        :param canvas: canvas is a class for displaying all GIS data types on a canvas
        :type canvas: QgsMapCanvas
        :param config: iquaview configuration
        :type config: Config
        :param menubar: A menu bar consists of a list of pull-down menu items.
        :type menubar: QMenuBar
        :param before_action: insert before action before_action
        :rtype: QAction
        :param before_toolbar:insert before toolbar before_toolbar
        :type before_toolbar: QToolBar
        :param parent: parent widget
        :type parent: QWidget
        """
        super().__init__(parent)

        self.entities_manager = entities_manager
        self.canvas = canvas
        self.config = config
        self.driver = None

        self.boat_pose_action = QAction(QIcon(":/resources/mActionBoatPose.svg"), "Show Vessel Pose", self)
        self.boat_pose_action.setCheckable(True)

        self.vessel_pos_system_action = QAction(
            QIcon(":resources/mActionVesselPosSystem.svg"), "Vessel Position System...", self
        )

        self.vessel_menu = QMenu("Vessel")
        self.vessel_menu.addAction(self.boat_pose_action)
        self.vessel_menu.addAction(self.vessel_pos_system_action)
        menubar.insertMenu(before_action, self.vessel_menu)

        # Toolbar for Tools
        self.vessel_toolbar = QToolBar("Vessel")
        self.vessel_toolbar.setObjectName("Vessel")
        self.vessel_toolbar.addAction(self.boat_pose_action)
        self.vessel_toolbar.addAction(self.vessel_pos_system_action)
        parent.insertToolBar(before_toolbar, self.vessel_toolbar)

        vessel_settings = self.entities_manager.get_entity_settings("vessel")
        # Vessel Pose
        if vessel_settings is None:
            identifier = 0
            position = 0
            gps_widget = GPSConnectionWidget()

            config = gps_widget.get_configuration()
            style = self.default_track_style()
        else:
            position, item = vessel_settings
            identifier = item.get("id")
            driver_values = item["driver"]
            config = ConfigGPSDriver()
            config.from_dict(driver_values)

            # offset
            vessel_name = self.config.settings["vessel_name"]
            config.offset_yaw_deg = self.config.settings["vessel_configuration"][vessel_name]["gps_offset_heading"]
            if util.find_spec("iquaview_evologics_usbl") is not None:
                config.declination_deg = self.config.settings["vessel_configuration"][vessel_name].get(
                    "usbl_magnetic_declination", 0
                )

            style = self.default_track_style()
            if "canvasdrawer" in item:
                style.from_dict(item["canvasdrawer"]["style"])
            elif "style" in item:
                style_dict = item["style"]
                style_dict["color_hex_str"] = style_dict["color"]
                style_dict.pop("color")
                item.pop("style")
                style.from_dict(style_dict)
        self.driver = GPSDriver(config)

        self.vesselentity = VesselEntity(
            id=identifier,
            position=position,
            name="Vessel",
            visible=True,
            driver=self.driver,
            style=style,
            config=self.config,
            canvas=self.canvas,
            permanent="vessel",
            connect_when_starts=False,
        )

        self.entities_manager.append_entity(self.vesselentity)
        self.vesselentity.save_changes(self.vesselentity.properties_widgets())

        # republish position and orientation from vessel entity
        self.vesselentity.vessel_position_signal.connect(self.vessel_position_signal.emit)
        self.vesselentity.vessel_orientation_signal.connect(self.vessel_orientation_signal.emit)

        self.boat_pose_action.triggered.connect(lambda: self.entities_manager.connect_entity(self.vesselentity))
        self.vessel_pos_system_action.triggered.connect(
            lambda: self.entities_manager.edit_entity(
                self.vesselentity, False, self.vesselentity.vessel_pos_system_widget
            )
        )
        self.vesselentity.entity_updated_signal.connect(self.update_icon_state)

    def update_icon_state(self):
        """
        If vessel is connected set checked boat pose action , otherwise unchecked
        """
        self.boat_pose_action.setChecked(self.vesselentity.is_connected())

    def default_track_style(self):
        return CanvasTrackStyle(
            color_hex_str=QColor(Qt.darkGreen).name(),
            marker_active=True,
            marker_svg=":/resources/vessel.svg",
            marker_width=2.6,
            marker_length=6.95,
        )
