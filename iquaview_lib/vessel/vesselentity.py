#!/usr/bin/env python
# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


import copy
import logging
import math
from importlib import util
from typing import Dict

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QWidget
from qgis.core import QgsPointXY

from iquaview_lib.baseclasses.webserverbase import WebServerRegister, WebServerPoint
from iquaview_lib.baseclasses.entitybase import LabelData
from iquaview_lib.canvasdrawer.gnssentity import GNSSEntity, WARNING_ORIENTATION, WARNING_SIGNAL
from iquaview_lib.cola2api.gps_driver import GPSOrientation, GPSPosition
from iquaview_lib.connection.settings.gpsconnectionwidget import GPSConnectionWidget
from iquaview_lib.utils import calcutils as calc
from iquaview_lib.vessel.vesselpossystem import VesselPositionSystem

logger = logging.getLogger(__name__)


class VesselEntity(GNSSEntity):
    PROPERTIES_VESSEL = "vessel"
    vessel_position_signal = pyqtSignal(GPSPosition)
    vessel_orientation_signal = pyqtSignal(GPSOrientation)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.vessel_pos_system_widget = VesselPositionSystem(self.config, parent=None)
        self.last_position = GPSPosition(time=0.0, latitude=0.0, longitude=0.0, altitude=0.0, quality=-1)
        self.last_orientation = GPSOrientation(time=0.0, orientation_deg=0.0)

    def properties_widgets(self) -> Dict[str, QWidget]:
        widgets: dict = super().properties_widgets()

        self.vessel_pos_system_widget.reload()

        self.vessel_pos_system_widget.vessel_width_doubleSpinBox.valueChanged.connect(
            widgets[self.PROPERTIES_STYLE].width_doubleSpinBox.setValue
        )
        self.vessel_pos_system_widget.vessel_length_doubleSpinBox.valueChanged.connect(
            widgets[self.PROPERTIES_STYLE].length_doubleSpinBox.setValue
        )

        widgets[self.PROPERTIES_STYLE].width_doubleSpinBox.valueChanged.connect(
            self.vessel_pos_system_widget.vessel_width_doubleSpinBox.setValue
        )
        widgets[self.PROPERTIES_STYLE].length_doubleSpinBox.valueChanged.connect(
            self.vessel_pos_system_widget.vessel_length_doubleSpinBox.setValue
        )

        widgets[self.PROPERTIES_VESSEL] = self.vessel_pos_system_widget
        return widgets

    def update_canvas(self):
        """Updates new values from the driver into the canvas track that draws over the canvas."""
        if self.connected:
            gps_position: GPSPosition = self.driver.get_position()
            gps_orientation: GPSOrientation = self.driver.get_orientation()
            self.on_measurement(gps_position, gps_orientation)
            if gps_position.is_new() and (gps_position.quality >= 1) and (gps_position.quality <= 6):
                # emit position and orientation if changed
                if self.last_position.time != gps_position.time:
                    self.vessel_position_signal.emit(gps_position)
                    self.last_position = copy.deepcopy(gps_position)
                if self.last_orientation.time != gps_orientation.time:
                    self.vessel_orientation_signal.emit(gps_orientation)
                    self.last_orientation = copy.deepcopy(gps_orientation)

                self.set_warning(WARNING_SIGNAL, False)
                gps_lat = gps_position.latitude
                gps_lon = gps_position.longitude
                if gps_orientation.is_new():
                    gps_heading = gps_orientation.orientation_deg
                    self.set_warning(WARNING_ORIENTATION, False)
                else:
                    gps_heading = 0
                    self.set_warning(WARNING_ORIENTATION, True)

                vessel_name = self.config.settings["vessel_name"]
                gps_offset_y = self.config.settings["vessel_configuration"][vessel_name]["gps_offset_y"]
                gps_offset_x = self.config.settings["vessel_configuration"][vessel_name]["gps_offset_x"]
                vrp_offset_y = self.config.settings["vessel_configuration"][vessel_name]["vrp_offset_y"]
                vrp_offset_x = self.config.settings["vessel_configuration"][vessel_name]["vrp_offset_x"]
                brng = math.radians(gps_heading) + math.atan2(gps_offset_y + vrp_offset_y, gps_offset_x + vrp_offset_x)
                distance = -math.sqrt(
                    math.pow(gps_offset_x + vrp_offset_x, 2) + math.pow(gps_offset_y + vrp_offset_y, 2)
                )  # Distance in m

                gps_lon, gps_lat = calc.endpoint_sphere(gps_lon, gps_lat, distance, math.degrees(brng))

                pos = QgsPointXY(gps_lon, gps_lat)

                self.canvasdrawer.track_update_canvas(pos, math.radians(gps_heading), epsg_id=4326, update=self.visible)

                self.canvasdrawer.position_orientation_signal.emit(
                    WebServerPoint(
                        name=self.name,
                        connected=self.connected,
                        latitude=gps_lat,
                        longitude=gps_lon,
                        orientation_deg=gps_heading,
                    )
                )
                self.entity_updated_signal.emit()

                gps_fix_quality = gps_position.get_fix_quality_as_string()
                if gps_fix_quality != self.info_labels[self.LABEL_GPS_FIX_QUALITY].text:
                    self.set_label_data(
                        identifier=self.LABEL_GPS_FIX_QUALITY,
                        label_data=LabelData(text=gps_position.get_fix_quality_as_string()),
                    )

            elif not gps_position.is_new():
                self.set_warning(WARNING_SIGNAL, True)

    def save_changes(self, widgets: Dict[str, QWidget]) -> bool:
        try:
            vessel_pos_system: VesselPositionSystem = widgets[self.PROPERTIES_VESSEL]
            driver_configuration: GPSConnectionWidget = widgets[self.PROPERTIES_DRIVER]

            vessel_pos_system.on_accept()

            vessel_name = self.config.settings["vessel_name"]
            width = self.config.settings["vessel_configuration"][vessel_name]["vessel_width"]
            length = self.config.settings["vessel_configuration"][vessel_name]["vessel_length"]
            self.canvasdrawer.set_marker_size(width, length)
            self.update_canvas()

            driver_configuration.offset_yaw_deg = self.config.settings["vessel_configuration"][vessel_name][
                "gps_offset_heading"
            ]
            if util.find_spec("iquaview_evologics_usbl") is not None:
                driver_configuration.declination_deg = self.config.settings["vessel_configuration"][vessel_name][
                    "usbl_magnetic_declination"
                ]
            saved = super().save_changes(widgets)
        except Exception as e:
            logger.error("Entity not saved: {}".format(e))

        return saved

    def web_server_register(self) -> WebServerRegister:
        vessel_name = self.config.settings["vessel_name"]

        web_server_register = super().web_server_register()
        web_server_register.icon_detail_name = "vessel"
        web_server_register.icon_detail_size = (
            self.config.settings["vessel_configuration"][vessel_name]["vessel_width"],
            self.config.settings["vessel_configuration"][vessel_name]["vessel_length"],
        )
        return web_server_register
