# Copyright (c) 2022 Iqua Robotics SL

# This program is free software: you can redistribute it and/or modify it under the terms of the
# GNU General Public License as published by the Free Software Foundation, either version 2 of
# the License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with this program.
# If not, see <http://www.gnu.org/licenses/>.

"""
 Provides functions to check the mission status by decoding the error byte that comes
 either from WiFi (checking the /cola2_safety/status_code) or from the USBL messages.
"""

import logging
from typing import Dict, Optional, Tuple, Union
from .vehicledata import VehicleData
from PyQt5.QtCore import pyqtSignal, QObject, QTimer
from qgis.core import Qgis, QgsMessageLog

logger = logging.getLogger(__name__)


class StatusCode:
    """
    Class to decodify the status code represented in 32bits (4 bytes)
    into different bits representing vehicle status, errors and warnings.
    """

    """
    Definitions of each bit number
    """

    # 31-24 TBD

    # 23-21 TBD (6 bits left)

    BCB_STATUS_INDICATOR_2 = 20
    BCB_STATUS_INDICATOR_1 = 19
    RA_DROP_WEIGHT = 18
    RA_EMERGENCY_SURFACE = 17
    RA_ABORT_SURFACE = 16
    RA_ABORT_MISSION = 15
    LOW_BATTERY_WARNING = 14
    BATTERY_ERROR = 13
    WATCHDOG_TIMER = 12
    NAVIGATION_ERROR = 11
    NO_ALTITUDE_ERROR = 10
    WATER_INSIDE = 9
    HIGH_TEMPERATURE = 8
    CURRENT_MISSION_STEPS_BASE = 7  # to 0

    def __init__(self) -> None:
        pass

    def unpack(self, status_code: int) -> Dict[str, Union[bool, int]]:
        """
        Unpacks an status code integer to a dictionary of status, errors and warnings
        """
        bcb_status_indicator_2 = status_code & (1 << StatusCode.BCB_STATUS_INDICATOR_2) != 0
        bcb_status_indicator_1 = status_code & (1 << StatusCode.BCB_STATUS_INDICATOR_1) != 0
        ra_drop_weight = status_code & (1 << StatusCode.RA_DROP_WEIGHT) != 0
        ra_emergency_surface = status_code & (1 << StatusCode.RA_EMERGENCY_SURFACE) != 0
        ra_abort_surface = status_code & (1 << StatusCode.RA_ABORT_SURFACE) != 0
        ra_abort_mission = status_code & (1 << StatusCode.RA_ABORT_MISSION) != 0
        low_battery_warning = status_code & (1 << StatusCode.LOW_BATTERY_WARNING) != 0
        battery_error = status_code & (1 << StatusCode.BATTERY_ERROR) != 0
        watchdog_timer = status_code & (1 << StatusCode.WATCHDOG_TIMER) != 0
        navigation_error = status_code & (1 << StatusCode.NAVIGATION_ERROR) != 0
        no_altitude_error = status_code & (1 << StatusCode.NO_ALTITUDE_ERROR) != 0
        water_inside = status_code & (1 << StatusCode.WATER_INSIDE) != 0
        high_temperature = status_code & (1 << StatusCode.HIGH_TEMPERATURE) != 0
        current_mission_steps_base = status_code & 255  # last 8 bits (1111 1111)

        return {
            "bcb_status_indicator_2": bcb_status_indicator_2,
            "bcb_status_indicator_1": bcb_status_indicator_1,
            "ra_drop_weight": ra_drop_weight,
            "ra_emergency_surface": ra_emergency_surface,
            "ra_abort_surface": ra_abort_surface,
            "ra_abort_mission": ra_abort_mission,
            "low_battery_warning": low_battery_warning,
            "battery_error": battery_error,
            "watchdog_timer": watchdog_timer,
            "navigation_error": navigation_error,
            "no_altitude_error": no_altitude_error,
            "water_inside": water_inside,
            "high_temperature": high_temperature,
            "current_mission_steps_base": current_mission_steps_base,
        }


class VehicleStatus(QObject):
    mission_started = pyqtSignal()
    mission_stopped = pyqtSignal()
    stop_timer_signal = pyqtSignal()
    status_code_signal = pyqtSignal(dict)

    def __init__(self, vehicledata: VehicleData, msglog: QgsMessageLog) -> None:
        super().__init__()
        self.vehicle_data = vehicledata
        self.msglog = msglog
        self.status: str = ""
        self.status_color: str = "green"
        self.recovery_action_status: str = ""
        self.status_decoder = StatusCode()
        self.mission_is_executing: bool = False
        self.connected: bool = False
        self.timer = QTimer()
        self.timer.timeout.connect(self.check_status)

        self.vehicle_data.state_signal.connect(self.set_vehicle_data_status)
        self.stop_timer_signal.connect(self.stop_timer)

    def init_wifi(self) -> None:
        self.connected = True
        if not self.timer.isActive():
            self.timer.start(1000)

    def set_vehicle_data_status(self) -> None:
        self.status = "No connection with COLA2"
        self.status_color = "red"

    def check_status(self) -> None:
        if self.connected:
            status_code = self.vehicle_data.get_status_code()
            recovery_action = self.vehicle_data.get_recovery_action()
            if status_code is not None:
                self.assign_status(status_code)

            if recovery_action is not None:
                self.assign_recovery_status(recovery_action)

    def assign_status(self, status_byte: int):
        status_code = self.status_decoder.unpack(status_byte)
        self.status_code_signal.emit(status_code)
        status: str = ""
        status_color: str = "green"

        if status_code["current_mission_steps_base"] == 0:
            self.mission_is_executing = False
            self.mission_stopped.emit()

        # Set status
        if status_code["low_battery_warning"]:
            status = "Low battery charge/voltage."
            status_color = "orange"

        # Abort or Abort & Surface or Emergency surface or drop weight
        if status_code["ra_abort_mission"]:
            status = "Abort mission!"
            status_color = "red"

        if status_code["ra_abort_surface"]:
            status = "Abort and Surface!"
            status_color = "red"

        if status_code["ra_emergency_surface"]:
            status = "Emergency Surface!"
            status_color = "red"

        if status_code["ra_drop_weight"]:
            status = "Drop weight!"
            status_color = "red"

        # Append to status
        if status_code["navigation_error"]:
            status += " Navigation error."
            status_color = "red"

        if status_code["water_inside"]:
            status += " Water inside."
            status_color = "red"

        if status_code["high_temperature"]:
            status += " High Temperature."
            status_color = "red"

        if status_code["no_altitude_error"]:
            status += " No altitude."
            status_color = "red"

        if status_code["watchdog_timer"]:
            status += " Watchdog timeout reached."
            status_color = "red"

        if status_code["battery_error"]:
            status += " Battery charge/voltage below safety threshold."
            status_color = "red"

        if status_code["bcb_status_indicator_1"] and status_code["bcb_status_indicator_2"]:
            status += " Switching off due to magnetic switch."
            if status_color != "red":
                status_color = "orange"

        elif status_code["bcb_status_indicator_1"]:
            status += " Switching off due to BMS error."
            status_color = "red"

        elif status_code["bcb_status_indicator_2"]:
            status += " Switching off due to low battery."
            if status_color != "red":
                status_color = "orange"

        if status != "" and status_color != "green":
            log_level = 4 if status_color == "red" else 3
            self.msglog.logMessage("", "Status code:", log_level)
            self.msglog.logMessage(status, "Status code:", log_level)
            logger.info(f"Status code: {status_code} {status_byte}")
        elif status_code["current_mission_steps_base"] != 0:
            status = f"Executing mission, waypoint {status_code['current_mission_steps_base']}"
            status_color = "green"
            self.mission_is_executing = True
            self.mission_started.emit()

            feedback = self.vehicle_data.get_captain_state_feedback()
            if feedback is not None and feedback["valid_data"] == "new_data":
                for keyvalue in feedback["keyvalues"]:
                    if keyvalue["key"] == "total_steps":
                        status += f"/{keyvalue['value']}"

        self.status = status
        self.status_color = status_color

    def assign_recovery_status(self, recovery_status: Tuple[int, str]) -> None:
        error_level = recovery_status[0]
        error_string = recovery_status[1]
        if error_level == 2:
            self.recovery_action_status = "Abort mission: " + error_string
        elif error_level == 3:
            self.recovery_action_status = "Abort and Surface: " + error_string
        elif error_level == 4:
            self.recovery_action_status = "Emergency Surface: " + error_string
        elif error_level == 5:
            self.recovery_action_status = "Drop weight: " + error_string
        else:
            self.recovery_action_status = ""

        if self.recovery_action_status != "":
            self.msglog.logMessage("", "Recovery Action:", 6)
            self.msglog.logMessage(self.recovery_action_status, "Recovery Action:", 6)

    def get_status(self) -> Tuple[str, str]:
        return self.status, self.status_color

    def get_recovery_action_status(self) -> str:
        return self.recovery_action_status

    def is_mission_in_execution(self) -> bool:
        return self.mission_is_executing

    def stop_timer(self) -> None:
        """Stop timer to update data"""
        self.timer.stop()

    def disconnect_object(self) -> None:
        self.status = ""
        self.recovery_action_status = ""
        self.connected = False
        self.stop_timer_signal.emit()
