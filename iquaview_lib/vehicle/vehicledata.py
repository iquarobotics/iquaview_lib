# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


"""
 Class to subscribe to topics
"""
import logging
from typing import Any, Dict, List, Optional, Tuple, Union
from lxml import etree
from enum import Enum

from PyQt5.QtCore import pyqtSignal, QObject, QTimer
from PyQt5.QtWidgets import QMessageBox

from iquaview_lib.cola2api.cola2_interface import SubscribeToTopic
from iquaview_lib.vehicle.diagnosticsaggparser import DiagnosticsAggParser
from iquaview_lib.xmlconfighandler.vehicledatahandler import VehicleDataHandler
from iquaview_lib.definitions import ROSBRIDGE_SERVER_PORT
from iquaview_lib.vehicle.vehicleinfo import VehicleInfo

logger = logging.getLogger(__name__)


def is_navigation_valid(data: Optional[Dict[str, Any]]) -> bool:
    if data is not None and data["valid_data"] != "disconnected":
        gpos = data["global_position"]
        if gpos["latitude"] != 0.0 and gpos["longitude"] != 0.0:
            return True
    return False


def is_navigation_new(data: Optional[Dict[str, Any]]) -> bool:
    return is_navigation_valid(data) and data["valid_data"] == "new_data"


def is_gps_valid(data: Optional[Dict[str, Any]]) -> bool:
    if data is not None and data["valid_data"] != "disconnected":
        # greater or equal than STATUS_FIX=0
        if data["status"]["status"] >= 0 and data["position_covariance"][0] < 10.0:
            return True
    return False


def is_gps_new(data: Optional[Dict[str, Any]]) -> bool:
    return is_gps_valid(data) and data["valid_data"] == "new_data"


class VehicleTopicKey(str, Enum):
    ChronyTracking = "chrony tracking"


class VehicleData(QObject):
    state_signal = pyqtSignal()
    start_timer_signal = pyqtSignal()
    stop_timer_signal = pyqtSignal()

    def __init__(self, xml_filename: str, vehicle_info: VehicleInfo) -> None:
        """
        Constructor
        :param xml_filename: xml filename configuration
        :param vehicle_info: information about the AUV
        """
        super().__init__()

        self.auv_config_xml = xml_filename

        self.vehicle_info = vehicle_info

        self.subscribed: bool = False

        self.timer = QTimer()
        self.timer.timeout.connect(self.refresh_data)
        self.start_timer_signal.connect(self.start_timer)
        self.stop_timer_signal.connect(self.stop_timer)
        self.topics: Dict[str, SubscribeToTopic] = {}
        self.current_data: Dict[str, Any] = {}

        self.topic_names = self.read_xml_topics()
        # read services from xml
        self.services = self.read_xml_services()
        # read lauch list
        self.controlstation_launchlist = self.read_xml_controlstation_launch_list()
        self.vehicle_launchlist = self.read_xml_vehicle_launch_list()

    def read_xml_topics(self) -> Dict[str, str]:
        """
        Read a vehicle data topics from a XML
        :return: return vehicle data topics in a dictionary
        :rtype: dict
        """
        vd_handler = VehicleDataHandler(self.auv_config_xml)
        # get vehicle data topics
        xml_vehicle_data_topics = vd_handler.read_topics()

        topic_names: Dict[str, str] = {}

        for topic in xml_vehicle_data_topics:
            topic_names[topic.get("id")] = topic.text

        topic_names["rosout"] = "/rosout_agg"

        return topic_names

    def read_xml_services(self) -> Dict[str, str]:
        """
        Read a vehicle data services from a XML
        :return: return vehicle data services in a dictionary
        :rtype: dict
        """
        vd_handler = VehicleDataHandler(self.auv_config_xml)
        # get vehicle data topics
        xml_vehicle_data_services = vd_handler.read_services()

        services_names: Dict[str, str] = {}

        for service in xml_vehicle_data_services:
            services_names[service.get("id")] = service.text

        logger.debug(f"Services loaded: {services_names}")

        return services_names

    def read_xml_controlstation_launch_list(self) -> Optional[Dict[str, str]]:
        """
        Read a launch list from a XML
        :return: return launchs in a dictionary
        :rtype: dict
        """
        vd_handler = VehicleDataHandler(self.auv_config_xml)
        # get launch list
        xml_launch_list = vd_handler.read_controlstation_launch_list()
        if xml_launch_list is None:
            return None

        launch_list: Dict[str, str] = {}
        for launch in xml_launch_list:
            launch_list[launch.get("id")] = launch.text

        logger.debug(f"Launch commands loaded: {launch_list}")
        return launch_list

    def read_xml_vehicle_launch_list(self):
        """
        Read a launch list from a XML
        :return: return launchs in a dictionary
        :rtype: dict
        """
        vd_handler = VehicleDataHandler(self.auv_config_xml)
        # get launch list
        xml_launch_list = vd_handler.read_vehicle_launch_list()
        if xml_launch_list is None:
            launch_list = None
        else:
            launch_list = {}

            for launch in xml_launch_list:
                launch_list[launch.get("id")] = launch.text

            logger.debug(f"Launch commands loaded: {launch_list}")

        return launch_list

    def subscribe_topics(self) -> None:
        """
        Subscribe topics
        """

        ip = self.vehicle_info.get_vehicle_ip()
        port = ROSBRIDGE_SERVER_PORT
        vehicle_namespace = self.vehicle_info.get_vehicle_namespace()
        try:
            for key, value in self.topic_names.items():
                if key == "watchdog timer":
                    self.topics[key] = SubscribeToTopic(ip, port, vehicle_namespace + value)
                    self.topics[key].subscribe()
                    self.topics[key].recv()
                elif key == "rosout":
                    self.topics[key] = SubscribeToTopic(ip, port, value, 30, True)
                    self.topics[key].subscribe()
                elif "usage" in key:
                    self.topics[key] = SubscribeToTopic(ip, port, vehicle_namespace + value, 10)
                    self.topics[key].subscribe()
                else:
                    self.topics[key] = SubscribeToTopic(ip, port, vehicle_namespace + value)
                    self.topics[key].subscribe()

            self.subscribed = True

            self.start_timer_signal.emit()

        except Exception as e:
            logger.error("Connection with COLA2 could not be established")
            QMessageBox.critical(
                self.parent(),
                "Connection with AUV Failed",
                f"Connection with COLA2 could not be established. \nException: {e}",
                QMessageBox.Close,
            )
            self.state_signal.emit()
            self.subscribed = False

    def unsubscribe_topic(self, key: str) -> None:
        """
        Unsubscribe topic with key key
        :param key: the key of the topic in the xml
        :type key: str
        """
        if self.topics.get(key) is not None:
            self.topics[key].close()

    def subscribe_topic(self, key: str) -> None:
        """
        Subscribe topic with key 'key'
        :param key: the key of the topic in the xml
        :type key: str
        """
        ip = self.vehicle_info.get_vehicle_ip()
        port = ROSBRIDGE_SERVER_PORT
        vehicle_namespace = self.vehicle_info.get_vehicle_namespace()
        value = self.topic_names[key]
        if self.topics.get(key) is not None:
            self.topics[key].close()
        self.topics[key] = SubscribeToTopic(ip, port, vehicle_namespace + value)
        self.topics[key].subscribe()

    def is_topic_in_config(self, key: Union[VehicleTopicKey, str]) -> bool:
        if isinstance(key, VehicleTopicKey):
            return key.value in self.topic_names
        return key in self.topic_names

    def is_subscribed_to_topic(self, key: str) -> bool:
        """
        Get True if is subscribed to topic with key 'key', otherwise False
        :param key: key of the topic
        :type key: str
        :return: Return True if is subscribed to topic with key 'key', otherwise False
        :rtype: bool
        """
        if self.topics.get(key) is not None and self.topics[key].get_keepgoing():
            return True
        else:
            return False

    def refresh_data(self) -> None:
        """
        Start timer and refresh data
        """
        if self.subscribed:
            for key, value in self.topics.items():
                if key == "rosout":
                    buffer = value.get_buffer()
                    self.set_buffer(key, buffer)
                    value.clear_buffer()

                else:
                    if value is not None:
                        data = value.get_data()
                        self.set_data(key, data)

    def _get_data(self, key: str, subkey: Optional[str] = None) -> Optional[Dict[str, Any]]:
        if self.current_data is None:
            return None
        data: Optional[Dict[str, Dict[str, Any]]] = self.current_data.get(key)
        if subkey is None:
            return data
        else:
            if data is not None:
                return data.get(subkey)
        return None

    def get_diagnostics_agg(self):
        """
        Get diagnostics agg
        :return: return diagnostics agg in a dict
        :rtype: dict
        """
        return self._get_data("diagnostics agg")

    def get_safety_supervisor_status(self) -> Optional[Dict[str, Any]]:
        """
        Get safety supervisor status.
        :return: return safety supervisor status in a dict
        :rtype: dict
        """
        return self._get_data("safety supervisor status")

    def get_nav_sts(self) -> Optional[Dict[str, Any]]:
        """
        Get navigation status.
        :return: return navigation status in a dict
        :rtype: dict
        """
        return self._get_data("navigation status")

    def get_gps(self) -> Optional[Dict[str, Any]]:
        """
        Get gps data.
        :return: return gps data in a dict
        :rtype: dict
        """
        return self._get_data("gps")

    def get_desired_pose(self) -> Optional[Dict[str, Any]]:
        """
        Get merged world waypoint req. (Desires pose)
        :return: return merged world waypoint req in a dict
        :rtype: dict
        """
        return self._get_data("merged world waypoint req")

    def get_desired_twist(self) -> Optional[Dict[str, Any]]:
        """
        Get merged body velocity req. (Desired twist)
        :return: return merged body velocity req
        :rtype: dict
        """
        return self._get_data("merged body velocity req")

    def get_watchdog(self) -> Optional[Dict[str, Any]]:
        """
        Get watchdog.
        :return: return watchdog
        :rtype: dict
        """
        return self._get_data("watchdog timer")

    def get_thrusters_status(self) -> bool:
        """
         Get thrusters status
        :return: return thrusters status
        :rtype: bool
        """
        data = self._get_data("diagnostics agg")
        if data is None:
            return None
        diagnostics_agg_parser = DiagnosticsAggParser(data)
        value_str = diagnostics_agg_parser.get_value("/control/controller", "thruster_allocator_enabled")
        if value_str is None:
            return None
        return value_str.lower() == "true"

    def get_battery_charge(self) -> Optional[float]:
        """
         Get battery charge
        :return: return battery charge
        :rtype: float
        """
        data = self._get_data("diagnostics agg")
        if data is None:
            return None
        diagnostics_agg_parser = DiagnosticsAggParser(self.current_data.get("diagnostics agg"))
        value_str = diagnostics_agg_parser.get_value("/safety/batteries", "charge")
        if value_str is None:
            return None
        return float(value_str)

    def get_captain_state(self) -> Optional[int]:
        """
         Get captain state
        :return:  return captain state
        :rtype: int
        """
        return self._get_data("captain status", "state")

    def get_captain_status(self) -> Optional[Dict[str, Any]]:
        """
        Get captain status
        IDLE=0
        GOTO=1
        MISSION=2
        KEEPPOSITION=3
        SAFETYKEEPPOSITION=4
        EXTERNALMISSION=5
        SECTION=6
        :return: return captain status
        :rtype: dict
        """
        return self._get_data("captain status")

    def get_cpu_usage(self) -> Optional[float]:
        """
        Get cpu usage
        :return: return vehicle cpu usage
        :rtype: float
        """
        return self._get_data("cpu usage", "data")

    def get_ram_usage(self) -> Optional[float]:
        """
        Get ram usage
        :return: return vehicle ram usage
        :rtype: float
        """
        return self._get_data("ram usage", "data")

    def get_thruster_setpoints(self) -> Optional[Dict[str, Any]]:
        """
         Get thruster setpoints topic
        :return: return thruster setpoints
        :rtype: dict
        """
        return self._get_data("thruster setpoints")

    def get_captain_state_feedback(self) -> Optional[Dict[str, Any]]:
        """
         Get captain state feedback
        :return: return state feedback
        :rtype: dict
        """
        return self._get_data("state feedback")

    def get_status_code(self) -> Optional[int]:
        """
        Get the status code
        :return: return status code codified in a byte
        :rtype: int
        """
        return self._get_data("safety supervisor status", "status_code")

    def get_recovery_action(self) -> Optional[Tuple[int, str]]:
        """Get recovery action
        :return: return recovery action codified in a byte
        :rtype: int
        """
        data = self._get_data("safety supervisor status", "recovery_action")
        if data is None:
            return None
        error_level: int = data["error_level"]
        error_string: str = data["error_string"]
        return error_level, error_string

    def get_rosout(self) -> Optional[List[str]]:
        """
         Get rosout
        :return: returns rosout
        :rtype: list
        """
        data = self._get_data("rosout")
        if (data is not None) and (not data):  # avoid empty list
            return None
        return data

    def get_chrony_tracking(self):
        return self._get_data(VehicleTopicKey.ChronyTracking)

    def _get_service(self, key: str) -> Optional[str]:
        if self.services is None:
            return None
        return self.services.get(key)

    def get_calibrate_magnetometer_service(self) -> Optional[str]:
        """
        Get calibrate magnetometer service
        :return: returns calibrate magnetometer service
        :rtype: str
        """
        return self._get_service("calibrate magnetometer")

    def get_stop_magnetometer_calibration_service(self) -> Optional[str]:
        """
        Get stop magnetometer calibration service
        :return: return stop magnetometer calibration service
        :rtype: str
        """
        return self._get_service("stop magnetometer calibration")

    def get_keep_position_service(self) -> Optional[str]:
        """
        Get keep position service
        :return: return keep position service
        :rtype: str
        """
        return self._get_service("keep position")

    def get_disable_keep_position_service(self) -> Optional[str]:
        """
        Get disable keep position service
        :return: return disable keep position service
        :rtype: str
        """
        return self._get_service("disable keep position")

    def get_disable_all_keep_positions_service(self) -> Optional[str]:
        """
        Get disable all keep position service
        :return: return disable all keep position service
        :rtype: str
        """
        return self._get_service("disable all keep positions")

    def get_reset_timeout_service(self) -> Optional[str]:
        """Get reset timeout service
        :return: return reset timeout service
        :rtype: str
        """
        return self._get_service("reset timeout")

    def get_goto_service(self) -> Optional[str]:
        """
        Get goto service
        :return: return enable goto service
        :rtype: str
        """
        return self._get_service("enable goto")

    def get_disable_goto_service(self) -> Optional[str]:
        """
        Get disable goto service
        :return: return disable goto service
        :rtype: str
        """
        return self._get_service("disable goto")

    def get_enable_thrusters_service(self) -> Optional[str]:
        """
        Get enable thrusters service
        :return: return enable thrusters service
        :rtype: str
        """
        return self._get_service("enable thrusters")

    def get_disable_thrusters_service(self) -> Optional[str]:
        """
         Get disable thrusters service
        :return: return disable thrusters service
        :rtype: str
        """
        return self._get_service("disable thrusters")

    def get_enable_mission_service(self) -> Optional[str]:
        """
        Get enable mission service
        :return: return enable mission service
        :rtype: str
        """
        return self._get_service("enable mission")

    def get_disable_mission_service(self) -> Optional[str]:
        """
        Get disable mission service
        :return: return disable mission service
        :rtype: str
        """
        return self._get_service("disable mission")

    def get_pause_mission_service(self) -> Optional[str]:
        """
        Get pause mission service
        :return: return pause mission service
        :rtype: str
        """
        return self._get_service("pause mission")

    def get_resume_mission_service(self) -> Optional[str]:
        """
        Get pause mission service
        :return: return pause mission service
        :rtype: str
        """
        return self._get_service("resume mission")

    def get_reset_recovery_action_service(self) -> Optional[str]:
        """
        Get reset recovery action service
        :return: return recovery action service
        :rtype: str
        """
        return self._get_service("reset recovery action")

    def get_teleoperation_launch(self) -> Optional[str]:
        """
        Get teleoperation launch
        :return: return teleoperation launch
        :rtype: str
        """
        if self.controlstation_launchlist is None:
            return None
        if self.controlstation_launchlist.get("teleoperation") is None:
            return None

        return self.controlstation_launchlist["teleoperation"]

    def get_vehicle_launch_dict(self):
        """
        Get vehicle launch list
        :return: return vehicle launch list
        :rtype: dict
        """
        if self.vehicle_launchlist is None:
            return None

        return self.vehicle_launchlist

    def set_data(self, identifier: str, data) -> None:
        """
        set data 'data' in  self.current_data list with key 'identifier'

        :param identifier: key to identify the data
        :param data: new data to update
        """

        if data:
            self.current_data[identifier] = data
        else:
            self.state_signal.emit()

    def set_buffer(self, identifier: str, buffer: Dict[str, Any]) -> None:
        """
        set data 'buffer' in  self.current_data list with key 'identifier'

        :param identifier: key to identify the data
        :param buffer: new data to update
        """

        self.current_data[identifier] = buffer

    def is_subscribed(self) -> bool:
        """get subscribed state
        :return: return subscribe state
        :rtype: bool
        """
        return self.subscribed

    def start_timer(self) -> None:
        """start timer"""
        if not self.timer.isActive():
            self.timer.start(1000)

    def stop_timer(self) -> None:
        """Stop timer to update data"""
        self.timer.stop()

    def save(self) -> None:
        """
        save the vehicle data inside the last auv configuration xml
        """
        # last auv config xml
        vd_handler = VehicleDataHandler(self.auv_config_xml)
        # TODO: save topics, services and controlstation launchs
        xml_vehicle_launch_list = vd_handler.read_vehicle_launch_list()
        xml_vehicle_launch_list.clear()
        for title, launch in self.get_vehicle_launch_dict().items():
            xml_launch = etree.SubElement(xml_vehicle_launch_list, "launch", id=title)
            xml_launch.text = launch
        vd_handler.write()

    def disconnect_object(self) -> None:
        """Disconnect subscribers and stop timer"""
        self.subscribed = False

        for key, subscriber in self.topics.items():
            if subscriber is not None:
                subscriber.close()

        self.stop_timer_signal.emit()
