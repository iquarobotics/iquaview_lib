# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


"""
Class to read and store the xml structure associated to the vehicle_info tag in the AUV config file
"""

from typing import Optional
from iquaview_lib.xmlconfighandler.vehicleinfohandler import VehicleInfoHandler


class VehicleInfo:
    def __init__(self, xml_filename: str) -> None:
        """
        Constructor
        :param xml_filename: xml filename
        :type xml_filename: str
        """
        self.xml_filename = xml_filename

        self.vehicle_ip: Optional[str] = None
        self.vehicle_port: Optional[str] = None
        self.vehicle_type: Optional[str] = None
        self.vehicle_name: Optional[str] = None
        self.vehicle_width: Optional[str] = None
        self.vehicle_length: Optional[str] = None
        self.vehicle_namespace: str = ""
        self.user: Optional[str] = None
        self.remote_vehicle_package: Optional[str] = None

        # read XML
        self.read_xml()

    def read_xml(self) -> None:
        """

        read the last auv configuration xml loaded, and save the vehicle information
        """
        vi_reader = VehicleInfoHandler(self.xml_filename)
        xml_vehicle_info = vi_reader.read_configuration()

        for item in xml_vehicle_info:
            if item.tag == "vehicle_ip":
                self.vehicle_ip = item.text
            elif item.tag == "vehicle_port":
                self.vehicle_port = item.text
            elif item.tag == "vehicle_type":
                self.vehicle_type = item.text
            elif item.tag == "vehicle_name":
                self.vehicle_name = item.text
            elif item.tag == "vehicle_width":
                self.vehicle_width = item.text
            elif item.tag == "vehicle_length":
                self.vehicle_length = item.text
            elif item.tag == "vehicle_namespace":
                self.vehicle_namespace = item.text
            elif item.tag == "user":
                self.user = item.text
            elif item.tag == "remote_vehicle_package":
                self.remote_vehicle_package = item.text

    def get_vehicle_ip(self) -> Optional[str]:
        """
        :return: vehicle ip
        :rtype: str
        """
        return self.vehicle_ip

    def get_vehicle_port(self) -> Optional[str]:
        """
        :return: vehicle port
        :rtype: str
        """
        return self.vehicle_port

    def get_vehicle_type(self) -> Optional[str]:
        """
        :return: vehicle type
        :rtype: str
        """
        return self.vehicle_type

    def get_vehicle_name(self) -> Optional[str]:
        """
        :return: vehicle name
        :rtype: str
        """
        return self.vehicle_name

    def get_vehicle_width(self) -> Optional[str]:
        """
        :return: vehicle width
        :rtype: str
        """
        return self.vehicle_width

    def get_vehicle_length(self) -> Optional[str]:
        """
        :return: vehicle length
        :rtype: str
        """
        return self.vehicle_length

    def get_vehicle_namespace(self) -> Optional[str]:
        """
        :return: vehicle namespace
        :rtype: str
        """
        return self.vehicle_namespace

    def get_vehicle_user(self) -> Optional[str]:
        """
        :return: user
        :rtype: str
        """
        return self.user

    def get_remote_vehicle_package(self) -> Optional[str]:
        """
        :return: remote vehicle package
        :rtype: str
        """
        return self.remote_vehicle_package

    def set_vehicle_ip(self, vehicle_ip: str) -> None:
        """
        Set vehicle_ip
        :param vehicle_ip: vehicle ip address
        :type vehicle_ip: str
        """
        self.vehicle_ip = vehicle_ip

    def set_vehicle_port(self, vehicle_port: str) -> None:
        """
        Set vehicle port
        :param vehicle_port: vehicle port
        :type vehicle_port: str
        """
        self.vehicle_port = vehicle_port

    def set_vehicle_type(self, vehicle_type: str) -> None:
        """
        Set vehicle type
        :param vehicle_type: vehicle type
        :type vehicle_type: str
        """
        self.vehicle_type = vehicle_type

    def set_vehicle_name(self, vehicle_name: str) -> None:
        """
        Set vehicle name
        :param vehicle_name:  vehicle name
        :type vehicle_name: str
        """
        self.vehicle_name = vehicle_name

    def set_vehicle_width(self, vehicle_width: str) -> None:
        """
        Set vehicle width
        :param vehicle_width:  vehicle width
        :type vehicle_width: str
        """
        self.vehicle_width = vehicle_width

    def set_vehicle_length(self, vehicle_length: str) -> None:
        """
        Set vehicle length
        :param vehicle_length:  vehicle length
        :type vehicle_length: str
        """
        self.vehicle_length = vehicle_length

    def set_vehicle_namespace(self, vehicle_namespace: str) -> None:
        """
        Set vehicle namespace
        :param vehicle_namespace: namespace
        :type vehicle_namespace: str
        """
        self.vehicle_namespace = vehicle_namespace

    def set_user(self, user: str) -> None:
        """
        Set vehicle user
        :param user: user
        :type user: str
        """
        self.user = user

    def set_remote_vehicle_package(self, remote_vehicle_package: str) -> None:
        """
        Set remote vehicle package
        :param remote_vehicle_package: remote missions path
        :type remote_vehicle_package: str
        """
        self.remote_vehicle_package = remote_vehicle_package

    def save(self) -> None:
        """

        save the vehicle info inside the last auv configuration xml
        """
        # last auv config xml
        vi_handler = VehicleInfoHandler(self.xml_filename)
        xml_vehicle_info = vi_handler.read_configuration()

        for item in xml_vehicle_info:
            if item.tag == "vehicle_ip":
                item.text = str(self.vehicle_ip)
            elif item.tag == "vehicle_port":
                item.text = str(self.vehicle_port)
            elif item.tag == "vehicle_type":
                item.text = str(self.vehicle_type)
            elif item.tag == "vehicle_name":
                item.text = str(self.vehicle_name)
            elif item.tag == "vehicle_width":
                item.text = str(self.vehicle_width)
            elif item.tag == "vehicle_length":
                item.text = str(self.vehicle_length)
            elif item.tag == "vehicle_namespace":
                item.text = str(self.vehicle_namespace)
            elif item.tag == "user":
                item.text = str(self.user)
            elif item.tag == "remote_vehicle_package":
                item.text = str(self.remote_vehicle_package)

        vi_handler.write()
