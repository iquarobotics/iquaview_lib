# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.

""" QDialog to enter a valid ip"""

import logging

from PyQt5.QtCore import pyqtSignal
from PyQt5.QtGui import QValidator
from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QMessageBox

from iquaview_lib.ui.ui_ip_port_dialog import Ui_IPPortDialog
from iquaview_lib.utils.textvalidator import validate_ip, validate_port, get_ip_validator, get_port_validator, get_color

logger = logging.getLogger(__name__)


class IPPortDialog(QDialog, Ui_IPPortDialog):
    """ Dialog to configure IP and port address"""
    apply_signal = pyqtSignal()

    def __init__(self, parent=None):
        """ Init of the object IPPortDialog """
        super().__init__(parent)
        self.setupUi(self)
        self.ip_lineEdit.setValidator(get_ip_validator())
        self.port_lineEdit.setValidator(get_port_validator())
        self.ip_state = False
        self.port_state = False

        # set signals
        self.ip_lineEdit.textChanged.connect(self.on_ip_changed)
        self.port_lineEdit.textChanged.connect(self.on_port_changed)
        self.buttonBox.button(QDialogButtonBox.Ok).clicked.connect(self.accept_address)

    def is_valid(self):
        """ Check if the IP and Port are valid """
        valid = validate_ip(self.ip_lineEdit.text()) == QValidator.Acceptable
        valid = valid and validate_port(self.port_lineEdit.text()) == QValidator.Acceptable
        return valid

    def on_ip_changed(self):
        """ Check if the IP is valid """
        sender = self.sender()
        self.ip_state = validate_ip(sender.text())
        color = get_color(self.ip_state)
        sender.setStyleSheet(f'QLineEdit {{ background-color: {color} }}')

    def on_port_changed(self):
        """ Check if the Port is valid """
        sender = self.sender()
        self.port_state = validate_port(sender.text())
        color = get_color(self.port_state)
        sender.setStyleSheet(f'QLineEdit {{ background-color: {color} }}')

    def accept_address(self) -> None:
        """ Emit the signal when the dialog is accepted """
        if self.is_valid():
            self.accept()
        elif self.ip_state is False:
            QMessageBox.warning(self, "Invalid ip", f"The IP {self.ip_lineEdit.text()} is not valid")
        elif self.port_state is False:
            QMessageBox.warning(self, "Invalid port", f"The port {self.port_lineEdit.text()} is not valid")
