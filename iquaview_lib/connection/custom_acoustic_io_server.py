# -*- coding: utf-8 -*-

# Copyright (c) 2022 Iqua Robotics SL - All Rights Reserved
#
# This file is subject to the terms and conditions defined in file
# 'LICENSE.txt', which is part of this source code package.

"""
Format:
    request:
        {'action': send/recv, 'data': str}
    response:
        {'action': error, 'data': description}
        {'action': resp, 'data': ok}
        {'action': resp, 'data': {
            'auv_position':
        }}
"""
import os
import socket
import select
import threading
import json
import logging
import time
import base64
from typing import List, Tuple, Optional, cast
from iquaview_lib.cola2api.basic_message import BasicMessageDict, BasicMessage
from PyQt5.QtCore import QObject, pyqtSignal

try:
    from typing import TypedDict, Literal
except ImportError:
    from typing_extensions import TypedDict, Literal

TIMEOUT = 5.0  # timeout connection
TIMEOUT_SEND_CUSTOM = 5.0  # timeout send custom message
logger = logging.getLogger("custom_acoustic_io_server")


class DataCustom(TypedDict):
    time: float  # timestamp of the custom data reception
    raw: bytes  # raw string received through acoustics


class DataCustomOutput(TypedDict):
    time: float  # timestamp of the custom data reception
    raw: str  # raw string received through acoustics


class DataOutput(TypedDict):
    auv_position: BasicMessageDict  # position reported by the auv
    usbl_measurement: BasicMessageDict  # position reported by the usbl
    custom: DataCustomOutput  # custom data transmitted


# accepted encodings
Encodings = Literal['plain', 'base64']
ENCODINGS = ('plain', 'base64')  # change to typing.get_args(Encodings) when python >= 3.8


class Request(TypedDict):
    action: Literal['send', 'recv']
    encoding: Encodings
    data: str


class Response(TypedDict):
    action: Literal['resp']
    encoding: Encodings
    data: DataOutput


class CustomAcousticIOServer(QObject):
    """Class to handle custom user inputs and outputs over acoustic communications."""
    close_signal = pyqtSignal()

    def __init__(self, port: int, max_custom_size_down: int) -> None:
        """Constructor."""
        super().__init__()
        # copy config
        self.port: int = port  # port of the custom_io
        self.max_custom_size_down: int = max_custom_size_down  # max size of custom message ship2auv
        # init variables
        self.connected: bool = False  # flag to know if connected
        self.socket: Optional[socket.socket] = None  # socket that acts as server
        self.thread: Optional[threading.Thread] = None  # thread that runs the server
        self.keepgoing: bool = True  # flag to stop threads when server stops
        self.client_connection_list: List[socket.socket] = list()  # list of sockets to clients
        self.r_channel, self.w_channel = os.pipe()
        # data to send
        self.custom_to_send: DataCustom = {'time': 0.0, 'raw': b''}
        # data received
        self.auv_data_from_vehicle: BasicMessage = BasicMessage()
        self.usbl_data_from_vehicle: BasicMessage = BasicMessage()
        self.custom_data_from_vehicle: DataCustom = {'time': 0.0, 'raw': b''}
        # signal
        self.close_signal.connect(self.close)

    def set_port(self, port: int) -> None:
        """Set port"""
        if port != self.port:
            self.port = port

    def connect(self) -> None:
        """Connect socket server to network."""
        try:
            # pipe
            self.r_channel, self.w_channel = os.pipe()
            # create tcp socket
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            # start server
            server_address = ('', self.port)  # all interfaces
            self.socket.bind(server_address)
            logger.info('starting up on %s port %s' % self.socket.getsockname())
            self.socket.listen(5)  # denotes the number of clients can queue
            # start server thread
            self.keepgoing = True
            self.thread = threading.Thread(target=self.server_thread)
            self.thread.daemon = True
            self.thread.start()
            self.connected = True

        except OSError as e:
            # socket.bind fails
            msg = f"Custom acoustic IO server error {e}"
            logger.error(msg)
            raise OSError(msg) from e
        except Exception as e:
            msg = f"Custom acoustic IO server unexpected error {e}"
            logger.error(msg)
            raise Exception(msg) from e

    def server_thread(self) -> None:
        """Manage client connection (launched in a different thread)"""
        try:
            # reset clients at server start
            self.client_connection_list = list()
            while self.keepgoing:
                self.socket = cast(socket.socket, self.socket)
                # wait for a client forever
                logger.debug('waiting for a connection')
                read_ready, _, _ = select.select([self.socket.fileno(), self.r_channel], [], [])
                if self.socket is not None:
                    connection, client_address = self.socket.accept()
                    connection.settimeout(TIMEOUT)
                    # start client thread
                    c_thread = threading.Thread(target=self.client_thread, args=[connection, client_address])
                    c_thread.daemon = True
                    c_thread.start()
                    # add client socket to list
                    self.client_connection_list.append(connection)
                elif self.r_channel in read_ready:
                    # we are closing the socket using pipes to avoid lock in select
                    logger.info("break after reading from r_channel")
                    break
        except OSError as e:
            logger.error("OSError: {}".format(e))
            self.close_signal.emit()

    def client_thread(self, connection: socket.socket, client_address: Tuple[str, int]) -> None:
        """
        Send or receive data from client
        :param connection: is a new socket object usable to send and receive data on the connection
        :param client_address: is the address bound to the socket on the other end of the connection
        """
        try:
            logger.info(f"client connected: {client_address}")
            while self.keepgoing:
                # load request as json
                received = connection.recv(2048)
                if not received:
                    continue
                data: Request = json.loads(received.decode())
                logger.debug(f"received '{repr(data)}'")

                # action exists in message
                if 'action' not in data:
                    noaction_error = json.dumps({
                        "action": "error",
                        "data": (
                            "message should follow format:\n"
                            "{'action': 'send/recv', 'data': 'somedata'}")
                    })
                    connection.sendall(noaction_error.encode())
                    continue

                # client wants to send custom information
                if data['action'] == 'send':
                    action = "resp"
                    # common errors
                    if 'data' not in data:
                        action = "error"
                        response = "no data to send"
                    elif 'encoding' not in data:
                        action = "error"
                        response = "no encoding specified"
                    elif data['encoding'] not in ENCODINGS:
                        action = "error"
                        response = f"unsupported encoding {data['encoding']} specified"
                    else:
                        # get data with specified encoding
                        rawdata = b""
                        if data['encoding'] == 'plain':
                            rawdata = data['data'].encode()
                        elif data['encoding'] == 'base64':
                            rawdata = base64.decodebytes(data['data'].encode())

                        # check binary size
                        if len(rawdata) == 0:
                            action = "error"
                            response = 'data of length 0'
                        elif len(rawdata) > self.max_custom_size_down:
                            action = "error"
                            response = f'too long [{len(rawdata)}], max size is {self.max_custom_size_down}'
                        else:
                            response = 'ok'
                            self.custom_to_send["time"] = time.time()
                            self.custom_to_send["raw"] = rawdata
                    message_dict = {"action": action, "data": response}
                    message = json.dumps(message_dict)
                    connection.sendall(message.encode())

                # client wants to read custom information
                elif data['action'] == 'recv':
                    if 'encoding' not in data:
                        action = "error"
                        response = "no encoding specified"
                        message_dict = {"action": action, "data": response}
                        message = json.dumps(message_dict)
                        logger.info(f"send response: {repr(message)}")
                        connection.sendall(message.encode())
                    else:
                        # get data to send
                        datacustom = self.custom_data_from_vehicle
                        tim: float = datacustom['time']
                        # encode as necessary
                        raw: bytes = datacustom['raw']
                        if data['encoding'] == 'base64':
                            raw = base64.encodebytes(raw)[:-1]  # avoid added \n at the end
                        # send response
                        response_dict: Response = {
                            "action": "resp",
                            "encoding": data['encoding'],
                            "data": {
                                "auv_position": self.auv_data_from_vehicle.as_dict(),
                                "usbl_measurement": self.usbl_data_from_vehicle.as_dict(),
                                "custom": {
                                    "time": tim,
                                    "raw": raw.decode(),
                                },
                            }
                        }
                        message = json.dumps(response_dict)
                        connection.sendall(message.encode())

                else:
                    error = json.dumps({
                        "action": "error",
                        "data": f"unrecognized action '{data['action']}'",
                    })
                    connection.sendall(error.encode())

        except socket.timeout:
            logger.error('Timeout error')
            connection.close()
        except socket.error as ex:
            logger.error(f'Error {ex}')
            connection.close()
        except Exception as ex:
            logger.error(f'Exception {ex}')
            error = json.dumps({
                "action": "error",
                "data": f"exception '{ex}'",
            })
            connection.sendall(error.encode())
        finally:
            logger.info(f'Closing the connection with client: {client_address}')
            connection.close()

    def get_data_to_send(self) -> Tuple[bytes, float]:
        """Return data to send"""
        if (time.time() - self.custom_to_send["time"]) < TIMEOUT_SEND_CUSTOM:
            return self.custom_to_send["raw"], self.custom_to_send["time"]
        return b"", 0.0

    def set_auv_position(self, auv_position_data: BasicMessage):
        self.auv_data_from_vehicle = auv_position_data

    def set_usbl_measurement(self, usbl_position_data: BasicMessage):
        self.usbl_data_from_vehicle = usbl_position_data

    def set_custom_data(self, custom_data: DataCustom):
        self.custom_data_from_vehicle = custom_data

    def close(self) -> None:
        """ Close server"""
        self.connected = False

        # stop signal
        self.keepgoing = False

        # close clients
        for conn in self.client_connection_list:
            conn.close()

        # close server thread
        os.write(self.w_channel, '!'.encode())  # write something, just to wake up the select call
        try:
            if self.thread is not None:
                self.thread.join(1.0)
        except RuntimeError as e:
            logger.error(str(e.args[0]))

        # close server socket
        if self.socket is not None:
            self.socket.shutdown(socket.SHUT_RDWR)
            self.socket.close()
            self.socket = None

        # reset data
        self.custom_to_send = {'time': 0.0, 'raw': b''}
        self.auv_data_from_vehicle = BasicMessage()
        self.usbl_data_from_vehicle = BasicMessage()
        self.custom_data_from_vehicle = {'time': 0.0, 'raw': b''}


if __name__ == "__main__":
    cds = CustomAcousticIOServer(port=8585, max_custom_size_down=64 - 39)
    while True:
        if cds.get_data_to_send() == "exit":
            break
