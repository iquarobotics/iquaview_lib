# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


"""
 Class to manage client connections to iquaview
"""

import os
import socket
import threading
import select
import logging
import serial
from math import floor

from PyQt5.QtCore import pyqtSignal, QThreadPool, QThread, QObject

from iquaview_lib.utils.workerthread import Worker

logger = logging.getLogger(__name__)


class SocketServer(QObject):
    reconnect_signal = pyqtSignal()
    disconnect_signal = pyqtSignal(bool)

    def __init__(self) -> None:
        super().__init__()

        self.connected = False
        self.keepgoing = False
        self.socket_udp = None
        self.socket_tcp = None
        self.stop_reconnect = False
        self.output_is_serial = None
        self.output_protocol = None
        self.output_ip = None
        self.output_port = None
        self.output_protocol = None
        self.output_serial_device = None
        self.output_serial_baudrate = None
        self.thread_clients = None
        self.r_channel, self.w_channel = os.pipe()
        self.client_connection_list = []

        self.threadpool = QThreadPool()

        self.disconnect_signal.connect(self.close)
        self.reconnect_signal.connect(self.start_connection_thread)

    def start_connection_thread(self):
        """ Start cola2 thread """
        if not self.is_connected():
            worker = Worker(self.connect)  # Any other args, kwargs are passed to the run function
            self.threadpool.start(worker)

    def connect(self):
        """ Starts connexion. Serial or Ethernet (TCP or UDP)"""
        try:
            self.connected = True
            if not self.output_is_serial:
                if self.output_protocol == "UDP":
                    self.socket_udp = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
                    self.socket_udp.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
                else:
                    if self.socket_tcp is not None:
                        self.socket_tcp.close()
                    self.socket_tcp = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    self.socket_tcp.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
                    # Bind the socket to the address given on the command line
                    server_address = (self.output_ip, int(self.output_port))
                    self.socket_tcp.bind(server_address)
                    self.socket_tcp.listen(5)  # 5 denotes the number of clients can queue

                    self.keepgoing = True
                    self.thread_clients = threading.Thread(target=self.manage_clients)
                    self.thread_clients.daemon = True
                    self.thread_clients.start()

        except Exception as e:
            logger.error("Data Output error trying to establish connection: {}".format(e))
            self.disconnect_signal.emit(True)
            QThread.sleep(5)
            if not self.stop_reconnect:
                self.reconnect_signal.emit()

    def close(self, reconnect: bool = True):
        """
        Sets the widget as disconnected
        :param reconnect: if reconnect is False, disconnect reconnect signal
        :type reconnect: bool
        """
        if not reconnect:
            self.stop_reconnect = True
            try:
                self.reconnect_signal.disconnect(self.start_connection_thread)
            except:
                logger.error("No connected to signal")
        self.connected = False
        self.keepgoing = False
        for conn in self.client_connection_list:
            conn.close()

        if self.socket_tcp is not None:
            self.socket_tcp.shutdown(socket.SHUT_RDWR)
            self.socket_tcp.close()
            self.socket_tcp = None

        if self.socket_udp is not None:
            self.socket_udp.close()
            self.socket_udp = None

        os.write(self.w_channel, '!'.encode())  # write something, just to wake up the select call
        self.threadpool.clear()

    def manage_clients(self):
        """ Manage client connection"""
        try:
            self.client_connection_list = []
            while self.keepgoing:
                rfds, _, _ = select.select([self.socket_tcp.fileno(), self.r_channel], [], [])
                if self.socket_tcp is not None:
                    logger.debug('Waiting for a connection')
                    connection, client_address = self.socket_tcp.accept()
                    connection.settimeout(5.0)

                    self.client_connection_list.append(connection)
        except OSError as e:
            logger.error("OSError: {}".format(e))
            self.disconnect_signal.emit(True)
            QThread.sleep(5)
            if not self.stop_reconnect:
                self.reconnect_signal.emit()

        except Exception as e:
            logger.error(e)
            self.disconnect_signal.emit(True)
            QThread.sleep(5)
            if not self.stop_reconnect:
                self.reconnect_signal.emit()

    def send_data(self, message):
        """
        Send position via serial or ethernet
        :param message: message to send
        :type message: str
        """
        if self.output_is_serial:
            device = self.output_serial_device
            baudrate = self.output_serial_baudrate
            with serial.Serial(device, baudrate, timeout=2) as ser:
                ser.write((message + "\n").encode())
                ser.close()
        else:
            if self.output_protocol == "UDP":
                self.socket_udp.sendto((message + "\r\n").encode(), (self.output_ip, int(self.output_port)))
            else:
                for connection in self.client_connection_list:
                    try:
                        connection.sendall((message + "\r\n").encode())
                    except socket.timeout:
                        logger.error('Timeout error')
                        connection.close()
                        self.client_connection_list.remove(connection)
                    except socket.error as ex:
                        logger.error('Error {}'.format(ex))
                        connection.close()
                        self.client_connection_list.remove(connection)
                    except Exception as ex:
                        logger.error("Exception {}".format(ex))
                        connection.close()
                        self.client_connection_list.remove(connection)

    def is_connected(self):
        """
        Returns whether the widget is set as connected
        :return: True if connected
        :rtype: bool
        """
        return self.connected

    def gen_hdt(self, heading):
        """
        Generate HDT message
        :param heading: heading
        :type heading: float
        :return: HDT message
        :rtype: str
        """
        string = 'GPHDT,%s,T' % (heading)
        crc = 0
        for c in string:
            crc = crc ^ ord(c)
        crc = crc & 0xFF

        return '$%s*%0.2X' % (string, crc)

    def gen_gga(self, time_t, lat, lat_pole, lng, lng_pole, fix_quality, num_sats, hdop, alt_m, geoidal_sep_m,
                dgps_age_sec,
                dgps_ref_id):
        """
        Generate GGA message
        :param time_t: time
        :type time_t: float
        :param lat: latitude
        :type lat: float
        :param lat_pole: latitude pole
        :type lat_pole: str
        :param lng: longitude
        :type lng: float
        :param lng_pole: longitude pole
        :type lng_pole: str
        :param fix_quality: fix quality
        :type fix_quality: int
        :param num_sats: number of satellites
        :type num_sats: int
        :param hdop: horizontal dilution of precision
        :type hdop: float
        :param alt_m: altitude
        :type alt_m: float
        :param geoidal_sep_m: geoidal separation
        :type geoidal_sep_m: float
        :param dgps_age_sec: DGPS age
        :type dgps_age_sec: float
        :param dgps_ref_id: DGPS reference ID
        :type dgps_ref_id: int
        :return: GGA message
        :rtype: str
        """
        hhmmssss = '%02d%02d%02d%s' % (time_t.tm_hour, time_t.tm_min, time_t.tm_sec, '.%02d' if 0 != 0 else '')

        lat_abs = abs(lat)
        lat_deg = lat_abs
        lat_min = (lat_abs - floor(lat_deg)) * 60
        lat_sec = round((lat_min - floor(lat_min)) * 10000000)
        lat_pole_prime = ('S' if lat_pole == 'N' else 'N') if lat < 0 else lat_pole
        lat_format = '%02d%02d.%07d' % (lat_deg, lat_min, lat_sec)

        lng_abs = abs(lng)
        lng_deg = lng_abs
        lng_min = (lng_abs - floor(lng_deg)) * 60
        lng_sec = round((lng_min - floor(lng_min)) * 10000000)
        lng_pole_prime = ('W' if lng_pole == 'E' else 'E') if lng < 0 else lng_pole
        lng_format = '%03d%02d.%07d' % (lng_deg, lng_min, lng_sec)

        dgps_format = '%s,%s' % ('%.1f' % dgps_age_sec if dgps_age_sec is not None else '',
                                 '%04d' % dgps_ref_id if dgps_ref_id is not None else '')

        string = 'GPGGA,%s,%s,%s,%s,%s,%d,%02d,%.1f,%.1f,M,%.1f,M,%s' % (
            hhmmssss, lat_format, lat_pole_prime, lng_format, lng_pole_prime, fix_quality, num_sats, hdop, alt_m,
            geoidal_sep_m, dgps_format)
        crc = 0
        for c in string:
            crc = crc ^ ord(c)
        crc = crc & 0xFF

        return '$%s*%0.2X' % (string, crc)
