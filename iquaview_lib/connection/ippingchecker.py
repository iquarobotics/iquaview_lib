# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


"""
This module contains the IPPingChecker class, which is used to check the connection with the IP
"""

import logging
import subprocess

from PyQt5.QtCore import pyqtSignal, QThreadPool, QObject, QTimer

from iquaview_lib.utils.workerthread import Worker

logger = logging.getLogger(__name__)


class IPPingChecker(QObject):
    """
    This class is used to check the connection with the IP
    """
    is_connected_signal = pyqtSignal(bool)

    def __init__(self, ip_address: str):
        """
        Initializes the IPPingChecker class.
        """
        super().__init__()
        self.ip_address = ip_address
        self.connected = False
        self.threadpool = QThreadPool()

        self.connect()

    @property
    def ip_address(self):
        """
        Returns the IP address.
        """
        return self._ip_address

    @ip_address.setter
    def ip_address(self, ip_address: str):
        """
        Sets the IP address.
        """
        self._ip_address = ip_address

    def connect(self):
        """
        Connects to the IP.
        """
        if not self.connected:
            self.connected = True
            self.start_ping_thread()

    def reconnect_ping_thread(self):
        """
        Reconnects the ping thread.
        """
        if self.connected:
            QTimer.singleShot(5000, self.start_ping_thread)

    def start_ping_thread(self):
        """ Start ping thread """
        if self.connected:
            worker = Worker(self.send_ping)  # Any other args, kwargs are passed to the run function
            worker.signals.finished.connect(self.reconnect_ping_thread)
            self.threadpool.start(worker)

    def send_ping(self):
        """
        Sends a ping to the IP.
        :return: True if the ping was successful, False otherwise
        """

        try:
            if self.connected:
                response = subprocess.Popen(f"ping -c 1 -w2 {self.ip_address} > /dev/null 2>&1", shell=True).wait()
                if self.connected:
                    state = response == 0  # 0 means success
                    self.is_connected_signal.emit(state)

        except OSError as os_error:
            logger.error(f"Problem connecting to {self.ip_address} {os_error}")
            self.threadpool.clear()
        except Exception as exception:
            logger.error(f"Problem connecting to {self.ip_address} {exception}")
            self.threadpool.clear()

    def stop_ping(self):
        """
        Disconnects the updates.
        """
        self.connected = False
        self.threadpool.clear()
