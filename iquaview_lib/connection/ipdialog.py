# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.

""" QDialog to enter a valid ip"""

import logging

from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QMessageBox
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtGui import QValidator

from iquaview_lib.ui.ui_ip_dialog import Ui_IPDialog
from iquaview_lib.utils.textvalidator import validate_ip, get_ip_validator, get_color

logger = logging.getLogger(__name__)


class IPDialog(QDialog, Ui_IPDialog):
    """ Dialog to configure IP address"""
    apply_signal = pyqtSignal()

    def __init__(self, parent=None):
        """ Init of the object IPDialog """
        super().__init__(parent)
        self.setupUi(self)
        self.ip_lineEdit.setValidator(get_ip_validator())

        # set signals
        self.ip_lineEdit.textChanged.connect(self.on_ip_changed)
        self.buttonBox.button(QDialogButtonBox.Ok).clicked.connect(self.accept_ip)

    def is_valid(self):
        """ Check if the IP is valid """
        return validate_ip(self.ip_lineEdit.text()) == QValidator.Acceptable

    def on_ip_changed(self):
        """ Check if the IP is valid """
        sender = self.sender()
        state = validate_ip(sender.text())
        color = get_color(state)
        sender.setStyleSheet(f'QLineEdit {{ background-color: {color} }}')

    def accept_ip(self) -> None:
        """ Emit the signal when the dialog is accepted """
        if self.is_valid():
            self.accept()
        else:
            QMessageBox.warning(self, "Invalid ip", f"The IP {self.ip_lineEdit.text()} is not valid")
