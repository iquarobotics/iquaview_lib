# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.

"""
 Widget to set up the connection of a GPS device,
 either from a serial port or a TCP/IP connection.
"""
import sys

if sys.version_info >= (3, 8):
    from typing import Literal
else:
    from typing_extensions import Literal

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QWidget
from qgis.core import QgsGpsDetector

from iquaview_lib.cola2api.config_gps_driver import ConfigGPSDriver, ConfigSerial, ConfigEthernet
from iquaview_lib.ui.ui_gps_connection import Ui_GPSConnectionWidget
from iquaview_lib.utils.textvalidator import (line_edit_validator,
                                              validate_line_edits,
                                              get_ip_validator,
                                              get_int_validator)


class GPSConnectionWidget(QWidget, Ui_GPSConnectionWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.configure_heading = False
        self.serial_port = None
        self.heading_serial_port = None
        self.serial_baudrate = 9600
        self.heading_serial_baudrate = 9600
        self.ip = None
        self.heading_ip = None
        self.hdt_port = 4000
        self.gga_port = 4000
        self.protocol = 'TCP'
        self.heading_protocol = 'TCP'
        self.declination_deg = 0.0
        self.offset_yaw_deg = 0.0

        # set validators
        self.ip_text.setValidator(get_ip_validator())
        self.heading_ip_text.setValidator(get_ip_validator())
        int_port_validator = get_int_validator(0, 65535)
        self.hdt_port_text.setValidator(int_port_validator)
        self.gga_port_text.setValidator(int_port_validator)

        # set signals
        self.updateDevicesButton.clicked.connect(self.update_devices)
        self.ip_text.textChanged.connect(lambda: line_edit_validator(self.ip_text))
        self.gga_port_text.textChanged.connect(lambda: line_edit_validator(self.gga_port_text))
        self.update_devices()

        self.heading_updateDevicesButton.clicked.connect(self.heading_update_devices)
        self.heading_ip_text.textChanged.connect(lambda: line_edit_validator(self.heading_ip_text))
        self.hdt_port_text.textChanged.connect(lambda: line_edit_validator(self.hdt_port_text))
        self.heading_update_devices()

        self.configure_radioButton.toggled.emit(self.configure_radioButton.isChecked())
        if not self.configure_radioButton.isChecked():
            self.same_radioButton.setChecked(True)
        else:
            self.configure_radioButton.setChecked(True)

        self.heading_serialPortRadioButton.toggled.emit(True)
        self.heading_serialPortRadioButton.setChecked(True)
        self.heading_TCPRadioButton.setChecked(False)
        self.configure_radioButton.setChecked(False)
        self.same_radioButton.setChecked(True)
        self.serialPortRadioButton.toggled.emit(True)
        self.serialPortRadioButton.setChecked(True)
        self.ethernet_RadioButton.setChecked(False)

    @staticmethod
    def get_icon() -> QIcon:
        """
        Get icon
        :return: widget icon
        :rtype: QIcon
        """
        return QIcon(":/resources/mActionConnectionSettings.svg")

    def update_devices(self):
        """ Load available ports and update device ComboBox """
        detector = QgsGpsDetector('scan')
        self.deviceComboBox.clear()
        devices_list = []
        for port in detector.availablePorts():
            devices_list.append(port[0])
        self.deviceComboBox.addItems(devices_list)

    def heading_update_devices(self):
        """ Load available ports and update device ComboBox """
        detector = QgsGpsDetector('scan')
        self.heading_deviceComboBox.clear()
        devices_list = []
        for port in detector.availablePorts():
            devices_list.append(port[0])
        self.heading_deviceComboBox.addItems(devices_list)

    def is_serial(self):
        """
        return True if serial RadioButton is checked, otherwise False
        :return: True if serial RadioButton is checked, otherwise False
        :rtype: bool
        """
        if self.serialPortRadioButton.isChecked():
            return True
        elif self.ethernet_RadioButton.isChecked():
            return False

    def heading_is_serial(self):
        """
        return True if serial RadioButton of heading is checked, otherwise False
        :return: True if serial RadioButton of heading is checked, otherwise False
        :rtype: bool
        """
        if self.heading_serialPortRadioButton.isChecked():
            return True
        elif self.heading_TCPRadioButton.isChecked():
            return False

    def set_configuration(self, config: ConfigGPSDriver):
        """
        Set configuration
        :param config: configuration
        :type config: ConfigGPSDriver
        """
        if config.gga_serial is not None:
            self.serial_port = config.gga_serial.port
            self.serial_baudrate = config.gga_serial.baud_rate
        if config.hdt_serial is not None:
            self.heading_serial_port = config.hdt_serial.port
            self.heading_serial_baudrate = config.hdt_serial.baud_rate
        if config.gga_ethernet is not None:
            self.ip = config.gga_ethernet.ip
            self.gga_port = config.gga_ethernet.port
            self.protocol = config.gga_ethernet.protocol
        if config.hdt_ethernet is not None:
            self.heading_ip = config.hdt_ethernet.ip
            self.hdt_port = config.hdt_ethernet.port
            self.heading_protocol = config.hdt_ethernet.protocol

    @property
    def configure_heading(self):
        """
        Return configure heading
        :return: configure heading
        :rtype: str
        """
        self.__configure_heading = self.configure_radioButton.isChecked()
        return self.__configure_heading

    @configure_heading.setter
    def configure_heading(self, configure_heading):
        """
        Set configure heading state
        :param configure_heading: new configure heading state
        :type configure_heading: str
        """
        self.__configure_heading = configure_heading
        self.configure_radioButton.setChecked(configure_heading)

    @property
    def serial_port(self):
        """
        Return serial port
        :return: serial port
        :rtype: str
        """
        self.__serial_port = self.deviceComboBox.currentText()
        return self.__serial_port

    @serial_port.setter
    def serial_port(self, serial_port):
        """
        Set serial port
        :param serial_port: new serial port data
        :type serial_port: str
        """
        self.__serial_port = serial_port
        self.deviceComboBox.setCurrentText(serial_port)

    @property
    def heading_serial_port(self):
        """
        Return heading serial port
        :return: heading serial port
        :rtype: str
        """
        self.__heading_serial_port = self.heading_deviceComboBox.currentText()
        return self.__heading_serial_port

    @heading_serial_port.setter
    def heading_serial_port(self, heading_serial_port):
        """
        Set serial port
        :param heading_serial_port: new serial port data
        :type heading_serial_port: str
        """
        self.__heading_serial_port = heading_serial_port
        self.heading_deviceComboBox.setCurrentText(heading_serial_port)

    @property
    def serial_baudrate(self):
        """
        Return serial baudrate
        :return: serial baudrate
        :rtype: int
        """
        self.__serial_baudrate = int(self.baudRateComboBox.currentText())
        return self.__serial_baudrate

    @serial_baudrate.setter
    def serial_baudrate(self, serial_baudrate):
        """
        Set serial baudrate
        :param serial_baudrate: new serial baudrate data
        :type serial_baudrate: int
        """
        self.__serial_baudrate = serial_baudrate
        self.baudRateComboBox.setCurrentText(str(serial_baudrate))

    @property
    def heading_serial_baudrate(self):
        """
        Return heading serial baudrate
        :return: heading serial baudrate
        :rtype: int
        """
        self.__heading_serial_baudrate = int(self.heading_baudRateComboBox.currentText())
        return self.__heading_serial_baudrate

    @heading_serial_baudrate.setter
    def heading_serial_baudrate(self, heading_serial_baudrate):
        """
        Set heading serial baudrate
        :param heading_serial_baudrate: new serial baudrate data
        :type heading_serial_baudrate: int
        """
        self.__heading_serial_baudrate = heading_serial_baudrate
        self.heading_baudRateComboBox.setCurrentText(str(heading_serial_baudrate))

    @property
    def ip(self):
        """
        :return: return ip address
        :rtype: str
        """
        self.__ip = self.ip_text.text()
        return self.__ip

    @ip.setter
    def ip(self, ip):
        """
        Set ip address
        :param ip: new ip address
        :type ip: str
        """
        self.__ip = ip
        self.ip_text.setText(ip)

    @property
    def heading_ip(self):
        """
        :return: return heading ip address
        :rtype: str
        """
        self.__heading_ip = self.heading_ip_text.text()
        return self.__heading_ip

    @heading_ip.setter
    def heading_ip(self, heading_ip):
        """
        Set heading ip address
        :param heading_ip: new heading ip address
        :type heading_ip: str
        """
        self.__heading_ip = heading_ip
        self.heading_ip_text.setText(heading_ip)

    @property
    def hdt_port(self):
        """
        :return: return hdt port
        :rtype: int
        """
        self.__hdt_port = int(self.hdt_port_text.text())
        return self.__hdt_port

    @hdt_port.setter
    def hdt_port(self, port):
        """
        set hdt port
        :param port: new hdt port
        :type port: int
        """
        self.__hdt_port = port
        self.hdt_port_text.setText(str(port))

    @property
    def gga_port(self):
        """
        :return: return gga port
        :rtype: int
        """
        self.__gga_port = int(self.gga_port_text.text())
        return self.__gga_port

    @gga_port.setter
    def gga_port(self, port):
        """
        set gga port
        :param port: new gga port
        :type port: int
        """
        self.__gga_port = port
        self.gga_port_text.setText(str(port))

    @property
    def protocol(self) -> Literal['TCP', 'UDP']:
        """
        :return: return protocol
        :rtype: str
        """
        self.__protocol = self.protocol_comboBox.currentText()
        return self.__protocol

    @protocol.setter
    def protocol(self, protocol):
        """
        set protocol
        :param protocol:
        :type protocol: str
        """
        self.__protocol = protocol
        if protocol is not None:
            index = self.protocol_comboBox.findText(protocol)
            index = index if index != -1 else 0
            self.protocol_comboBox.setCurrentIndex(index)

    @property
    def heading_protocol(self) -> Literal['TCP', 'UDP']:
        """
        :return: return protocol
        :rtype: str
        """
        self.__heading_protocol = self.heading_protocol_comboBox.currentText()
        return self.__heading_protocol

    @heading_protocol.setter
    def heading_protocol(self, heading_protocol):
        """
        set heading protocol
        :param heading_protocol:
        :type heading_protocol: str
        """
        self.__heading_protocol = heading_protocol
        if heading_protocol is not None:
            index = self.heading_protocol_comboBox.findText(heading_protocol)
            index = index if index != -1 else 0
            self.heading_protocol_comboBox.setCurrentIndex(index)

    def is_valid(self):
        """ Check if gps data is valid"""
        if self.configure_radioButton.isChecked():
            # different sources for GGA and HDT
            return (
                    (
                            self.serialPortRadioButton.isChecked()
                            or (self.ethernet_RadioButton.isChecked()
                                and validate_line_edits([self.ip_text, self.gga_port_text]))
                    )
                    and (
                            self.heading_serialPortRadioButton.isChecked()
                            or (self.heading_TCPRadioButton.isChecked()
                                and validate_line_edits([self.heading_ip_text, self.hdt_port_text]))
                    )
            )

        # one source
        return (
                self.serialPortRadioButton.isChecked()
                or (self.ethernet_RadioButton.isChecked()
                    and validate_line_edits([self.ip_text, self.gga_port_text, self.hdt_port_text]))
        )

    def get_configuration(self) -> ConfigGPSDriver:
        """
        Get configuration
        :return: configuration
        :rtype: ConfigGPSDriver
        """
        if self.configure_radioButton.isChecked():
            if self.heading_is_serial():
                if self.is_serial():
                    config = ConfigGPSDriver()
                    config.gga_serial = ConfigSerial(self.serial_port, self.serial_baudrate)
                    config.hdt_serial = ConfigSerial(self.heading_serial_port, self.heading_serial_baudrate)
                else:
                    config = ConfigGPSDriver()
                    config.gga_ethernet = ConfigEthernet(self.ip, self.gga_port, self.protocol)
                    config.hdt_serial = ConfigSerial(self.heading_serial_port, self.heading_serial_baudrate)
            else:
                if self.is_serial():
                    config = ConfigGPSDriver()
                    config.gga_serial = ConfigSerial(self.serial_port, self.serial_baudrate)
                    config.hdt_ethernet = ConfigEthernet(self.heading_ip, self.hdt_port, self.heading_protocol)
                else:
                    config = ConfigGPSDriver()
                    config.gga_ethernet = ConfigEthernet(self.ip, self.gga_port, self.protocol)
                    config.hdt_ethernet = ConfigEthernet(self.heading_ip, self.hdt_port, self.heading_protocol)

        elif self.disabled_radioButton.isChecked():
            if self.is_serial():
                config = ConfigGPSDriver()
                config.gga_serial = ConfigSerial(self.serial_port, self.serial_baudrate)
            else:
                config = ConfigGPSDriver()
                config.gga_ethernet = ConfigEthernet(self.ip, self.gga_port, self.protocol)
        elif self.is_serial():
            config = ConfigGPSDriver()
            config.gga_serial = ConfigSerial(self.serial_port, self.serial_baudrate)
            config.hdt_serial = ConfigSerial(self.serial_port, self.serial_baudrate)

        else:
            config = ConfigGPSDriver()
            config.gga_ethernet = ConfigEthernet(self.ip, self.gga_port, self.protocol)
            config.hdt_ethernet = ConfigEthernet(self.ip, self.gga_port, self.protocol)

        config.declination_deg = self.declination_deg
        config.offset_yaw_deg = self.offset_yaw_deg
        return config
