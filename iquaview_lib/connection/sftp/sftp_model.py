# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.

"""
 This module contains the SftpModel class, which is used to store the
 information of a SFTP connection.
"""

from PyQt5.QtGui import QStandardItemModel


class SFTPModel(QStandardItemModel):
    """ This class is used to store the information of a SFTP connection. """

    def __init__(self, file_icon=None, folder_icon=None):
        """
        Init of the object SFTPModel
        :param file_icon: icon for file rows
        :type file_icon: QIcon
        :param folder_icon: icon for folder rows
        :type folder_icon: QIcon
        """
        super().__init__()
        self.row_list = []
        self.file_icon = file_icon
        self.folder_icon = folder_icon

    def add_row(self, row):
        """
        Add row to the model
        :param row: row to be added
        :type row: list
        """
        self.row_list.append(row)
        self.appendRow(row)

    def remove_all_rows(self):
        """
        Removes all rows from the model
        """
        self.row_list = []
        self.removeRows(0, self.rowCount())

    def get_folder(self, name):
        """
        Returns the folder specified by name
        :param name: name of the folder
        :type name: str
        :return: folder item
        :rtype: SFTPFolder
        """
        for row in self.row_list:
            if name == row.text():
                return row
        return None

    def get_file_rows(self):
        """
        Returns the rows that contain files
        :return: list files
        :rtype: list
        """
        files = []
        for row in self.row_list:
            if row[1].icon().name() == self.file_icon.name():
                files.append(row)
        return files

    def take_file_rows(self):
        """
        Returns the rows that contain files and removes them from the model
        :return: list of files
        :rtype: list
        """
        files = []
        i = 0
        while i < self.rowCount():
            row = self.row_list[i]
            if isinstance(row, list):
                if row[1].icon().name() == self.file_icon.name():
                    self.row_list.remove(row)
                    files.append(self.takeRow(i))
                    i = 0
                else:
                    i += 1
            else:
                if row.icon().name() == self.folder_icon.name():
                    self.row_list.remove(row)
                    files.append(self.takeRow(i))
                    i = 0
                else:
                    i += 1
        return files

    def take_folder_rows(self):
        """
        Returns the rows that contain folders and removes them from the model
        :return: list of folders
        :rtype: list
        """
        folders = []
        i = 0
        while i < self.rowCount():
            row = self.row_list[i]
            if isinstance(row, list):
                if row[1].icon().name() == self.folder_icon.name():
                    self.row_list.remove(row)
                    folders.append(self.takeRow(i))
                    i = 0
                else:
                    i += 1
            else:
                if row.icon().name() == self.folder_icon.name():
                    self.row_list.remove(row)
                    folders.append(self.takeRow(i))
                    i = 0
                else:
                    i += 1
        return folders
