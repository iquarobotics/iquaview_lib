# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.

"""
This module contains the SFTPFolder class, which is used to represent a folder.
"""

from PyQt5.QtGui import QStandardItem


class SFTPFolder(QStandardItem):
    """ SFTPFolder class."""

    def __init__(self, name):
        """
        Init of the object SFTPFolder
        :param name: name of the folder
        :type name: str
        """
        super().__init__(name)
        self.row_list = []
        self.dummy_row = QStandardItem("")
        self.dummy_row.setEditable(False)
        self.appendRow(self.dummy_row)

    def add_row(self, row):
        """
        Adds a row to the item
        :param row: row to be added
        :type row: QStandardItem
        """
        if self.dummy_row is not None:
            self.removeRow(0)
            self.dummy_row = None
        self.row_list.append(row)
        self.appendRow(row)

    def add_msg(self, msg):
        """
        Adds a message to the item
        :param msg: message to be added
        :type msg = str
        """
        if self.rowCount() > 0:
            self.removeRow(0)
            self.dummy_row = None
        msg_item = QStandardItem(str(msg))
        msg_item.setEditable(False)
        self.appendRow(msg_item)

    def get_folder(self, name):
        """
        Returns the folder specified by name
        :param name: name of the folder
        :type name: str
        :return: folder item
        :rtype: SFTPFolder
        """
        for row in self.row_list:
            if name == row.text():
                return row
        return None

    def get_path(self, base_path=""):
        """
        Returns the path of the folder item
        :param base_path: base path to add to the folder path
        :type base_path: str
        :return: path of the folder
        :rtype: str
        """
        path_names = [f'/{self.text()}']

        parent = self.parent()
        while parent is not None:
            path_names.append('/' + parent.text())
            parent = parent.parent()

        path_names.append(base_path)

        path_list = list(reversed(path_names))
        item_path = "".join(path_list)

        return item_path

    def is_empty(self):
        """
        :return: Returns if the folder is empty and has no folders inside
        :rtype: bool
        """
        return self.dummy_row is not None

    def remove_dummy(self):
        """
        Removes the dummy row
        """
        if self.dummy_row is not None:
            self.takeRow(0)
            self.dummy_row = None
