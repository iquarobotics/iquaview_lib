# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.

""" Dialog to show the progress of a file transfer """

from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtWidgets import QDialog

from iquaview_lib.ui.ui_sftp_transfer_progress import Ui_transfer_progress


class SFTPTransferProgressDialog(QDialog, Ui_transfer_progress):
    """ Dialog to show the progress of a file transfer """
    cancel_transfer_signal = pyqtSignal()

    def __init__(self, n_files, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.progress_bar.setValue(0)
        self.curr_file_label.setText("1")
        self.n_files_label.setText(str(n_files))
        self.n_files = n_files
        self.cancel_button.clicked.connect(self.close)

    def update_values(self, transferred_bytes, total_bytes):
        """
        Update the progress bar and the current file label
        :param transferred_bytes: bytes transferred
        :type transferred_bytes: int
        :param total_bytes: total bytes to transfer
        :type total_bytes: int
        """
        total = total_bytes / 1000
        transferred = transferred_bytes / 1000
        progress = 100 * transferred / total
        self.curr_kbytes_label.setText(f"{transferred:.0F}")
        self.total_kbytes_label.setText(f"{total:.0F}")
        self.progress_bar.setValue(progress)
        if progress == 100:
            curr_file = int(self.curr_file_label.text())
            if curr_file < self.n_files:
                curr_file += 1
                self.curr_file_label.setText(str(curr_file))

    def keyPressEvent(self, e) -> None:
        """ Override event handler keyPressEvent"""
        if e.key() == Qt.Key_Escape:
            self.cancel_transfer_signal.emit()
            super().keyPressEvent(e)

    def closeEvent(self, event):
        """ Overrides closeEvent"""
        super().closeEvent(event)
        self.cancel_transfer_signal.emit()

    def close(self) -> bool:
        """ Overrides close"""
        self.hide()
        super().close()
