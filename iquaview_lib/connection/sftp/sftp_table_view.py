# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.

""" This class implements a table view for SFTP connections """

from datetime import datetime
from os import path

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QStandardItem
from PyQt5.QtWidgets import QTableView, QStyle, QHeaderView

from iquaview_lib.connection.sftp.sftp_folder import SFTPFolder
from iquaview_lib.connection.sftp.sftp_model import SFTPModel


class SFTPTableView(QTableView):
    """ This class implements a table view for SFTP connections """

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setMinimumSize(QtCore.QSize(630, 100))
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self.setAlternatingRowColors(True)
        self.setSelectionMode(QtWidgets.QAbstractItemView.ContiguousSelection)
        self.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.setTextElideMode(QtCore.Qt.ElideRight)
        self.setHorizontalScrollMode(QtWidgets.QAbstractItemView.ScrollPerPixel)
        self.setShowGrid(False)
        self.setGridStyle(QtCore.Qt.SolidLine)
        self.setSortingEnabled(False)
        self.setWordWrap(False)
        self.horizontalHeader().setVisible(True)
        self.horizontalHeader().setCascadingSectionResizes(False)
        self.horizontalHeader().setDefaultSectionSize(15)
        self.horizontalHeader().setMinimumSectionSize(15)
        self.horizontalHeader().setSortIndicatorShown(True)
        self.horizontalHeader().setStretchLastSection(True)
        self.verticalHeader().setVisible(False)
        self.verticalHeader().setCascadingSectionResizes(False)
        self.verticalHeader().setDefaultSectionSize(21)
        self.verticalHeader().setSortIndicatorShown(False)
        self.verticalHeader().setStretchLastSection(False)

        self.folder_icon = self.style().standardIcon(getattr(QStyle, "SP_DirClosedIcon"))
        self.file_icon = self.style().standardIcon(getattr(QStyle, "SP_FileIcon"))

        model = SFTPModel(self.file_icon, self.folder_icon)
        model.setHorizontalHeaderItem(0, QStandardItem(""))
        model.setHorizontalHeaderItem(1, QStandardItem("NAME"))
        model.setHorizontalHeaderItem(2, QStandardItem("SIZE"))
        model.setHorizontalHeaderItem(3, QStandardItem("MODIFIED"))
        self.setModel(model)

        headers = self.horizontalHeader()
        headers.setSectionResizeMode(0, QHeaderView.Fixed)
        headers.setSectionResizeMode(1, QHeaderView.ResizeToContents)
        headers.setSectionResizeMode(2, QHeaderView.ResizeToContents)
        headers.setSectionResizeMode(3, QHeaderView.ResizeToContents)
        headers.setDefaultAlignment(Qt.AlignLeft)

        self.date_format = "%d/%m/%y \t %H:%M:%S"

        self.horizontalHeader().sectionClicked.connect(self.sort_table)

    def add_folder(self, pathname):
        """
        Adds a folder to the table model
        :param pathname: path name of the folder
        :type pathname: str
        """
        basename = path.basename(pathname)
        if basename[0] != '.':  # ignore folders that start with dot ('.')
            folder = SFTPFolder(basename)
            folder.setIcon(self.folder_icon)
            folder.setEditable(False)
            marker = QStandardItem("")
            marker.setEditable(False)
            folder_size = QStandardItem("")
            folder_date = QStandardItem("")
            folder_size.setEditable(False)
            folder_date.setEditable(False)
            row = [marker, folder, folder_size, folder_date]
            self.model().add_row(row)

    def add_file(self, pathname, size, date):
        """
        Adds a new row to the table model with 4 items: marker, name, size and last modified date
        :param pathname:  path name of the file
        :type pathname: str
        :param size: size of the file in kB
        :type size: float
        :param date: date of the file
        :type date: datetime
        """
        basename = path.basename(pathname)
        # ignore folders that start with dot ('.') and the link to default mission
        if basename[0] != '.' and basename != "default_mission.xml":
            marker = QStandardItem("")
            file_name = QStandardItem(basename)
            file_name.setIcon(self.file_icon)
            file_size = QStandardItem(f"{size / 1000:.2f} kB")
            file_date = QStandardItem(date.strftime(self.date_format))
            marker.setEditable(False)
            file_name.setEditable(False)
            file_size.setEditable(False)
            file_date.setEditable(False)
            row = [marker, file_name, file_size, file_date]
            self.model().add_row(row)

    def sort_table(self, index):
        """
        Sorts the table by name, size or date depending on the value specified by index
        :param index: index of the column
        :type index: int
        """
        if index == 1:
            self.sort_table_by_name()
        elif index == 2:
            self.sort_table_by_size()
        elif index == 3:
            self.sort_table_by_date()

    @staticmethod
    def sort_list_by_name(list_of_files):
        """
        Sorts the specified list by name
        :param list_of_files: list of QStandardItem where the name value is in the index 1 of the list
        :type list_of_files: list
        """
        if len(list_of_files) > 0:
            # Auxiliary value is appended to the elements of the list
            for i, item in enumerate(list_of_files):
                str_name = item[1].text()
                item.append(str_name)

            ncolumns = len(list_of_files[0])

            # List is sorted using the auxiliar column values
            list_of_files.sort(key=lambda row: row[ncolumns - 1].upper())

            # Auxiliary value is removed from all the elements of the list
            for i, item in enumerate(list_of_files):
                list_of_files[i] = item[:-1]

    def sort_table_by_name(self):
        """
        Sorts the table by name. This will sort in ascending or descending order
        depending on the sort indicator of the header of the table.
        """
        model = self.model()
        folder_rows = model.take_folder_rows()
        file_rows = model.take_file_rows()

        self.sort_list_by_name(folder_rows)
        self.sort_list_by_name(file_rows)

        # add ordered rows to the model
        if self.horizontalHeader().sortIndicatorOrder() == Qt.DescendingOrder:
            for i, row in enumerate(folder_rows):
                model.add_row(row)
            for i, row in enumerate(file_rows):
                model.add_row(row)
            self.horizontalHeader().setSortIndicator(1, Qt.DescendingOrder)
        elif self.horizontalHeader().sortIndicatorOrder() == Qt.AscendingOrder:
            for i in reversed(range(0, len(folder_rows))):
                model.add_row(folder_rows[i])
            for i in reversed(range(0, len(file_rows))):
                model.add_row(file_rows[i])
            self.horizontalHeader().setSortIndicator(1, Qt.AscendingOrder)

    def sort_table_by_size(self):
        """
        Sorts the table by size. This will sort in ascending or descending order
        depending on the sort indicator of the header of the table.
        """
        model = self.model()
        folder_rows = model.take_folder_rows()
        file_rows = model.take_file_rows()

        self.sort_list_by_name(folder_rows)

        # create auxiliary column in the rows with the size of type float instead of str
        for i, row in enumerate(file_rows):
            str_size = row[2].text()
            float_size = float(str_size[:-2])  # cut 'kB' from the str
            file_rows[i].append(float_size)

        # sort by the float size value created
        file_rows.sort(key=lambda f_row: f_row[4])

        # delete the auxiliary column
        for i, row in enumerate(file_rows):
            file_rows[i] = row[:-1]

        # add ordered rows to the model
        if self.horizontalHeader().sortIndicatorOrder() == Qt.DescendingOrder:
            for i, row in enumerate(folder_rows):
                model.add_row(row)
            for i, row in enumerate(file_rows):
                model.add_row(row)
            self.horizontalHeader().setSortIndicator(2, Qt.DescendingOrder)
        elif self.horizontalHeader().sortIndicatorOrder() == Qt.AscendingOrder:
            for i, row in enumerate(folder_rows):
                model.add_row(row)
            for i in reversed(range(0, len(file_rows))):
                model.add_row(file_rows[i])
            self.horizontalHeader().setSortIndicator(2, Qt.AscendingOrder)

    def sort_table_by_date(self):
        """
        Sorts the table by date. This will sort in ascending or descending order
        depending on the sort indicator of the header of the table.
        """
        model = self.model()
        folder_rows = model.take_folder_rows()
        file_rows = model.take_file_rows()

        self.sort_list_by_name(folder_rows)

        # create auxiliary column in the rows with the date of type datetime
        for i, row in enumerate(file_rows):
            str_date = row[3].text()
            date = datetime.strptime(str_date, self.date_format)
            file_rows[i].append(date)

        # sort by the datetime created
        file_rows.sort(key=lambda f_row: f_row[4])

        # delete the auxiliary column
        for i, row in enumerate(file_rows):
            file_rows[i] = row[:-1]

        # add ordered rows to the model
        if self.horizontalHeader().sortIndicatorOrder() == Qt.DescendingOrder:
            for i, row in enumerate(folder_rows):
                model.add_row(row)
            for i, row in enumerate(file_rows):
                model.add_row(row)
            self.horizontalHeader().setSortIndicator(3, Qt.DescendingOrder)
        elif self.horizontalHeader().sortIndicatorOrder() == Qt.AscendingOrder:
            for i, row in enumerate(folder_rows):
                model.add_row(row)
            for i in reversed(range(0, len(file_rows))):
                model.add_row(file_rows[i])
            self.horizontalHeader().setSortIndicator(3, Qt.AscendingOrder)
