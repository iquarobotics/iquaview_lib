# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.

""" Dialog to show help about SFTP connection """

from PyQt5.QtGui import QPalette
from PyQt5.QtWidgets import QDialog

from iquaview_lib.ui.ui_sftp_help_dialog import Ui_SFTP_Help_Dialog


class SFTPHelpDialog(QDialog, Ui_SFTP_Help_Dialog):
    """ Dialog to show help about SFTP connection """

    def __init__(self, same_file_i, not_found_i, older_file_i, newer_file_i, current_mission_c=None, parent=None):
        super().__init__(parent)
        self.setupUi(self)

        self.same_file_icon = same_file_i
        self.not_found_icon = not_found_i
        self.newer_file_icon = newer_file_i
        self.older_file_icon = older_file_i
        self.current_mission_color = current_mission_c

        pixmap_size = 20
        self.same_file_label.setPixmap(self.same_file_icon.pixmap(pixmap_size))
        self.not_found_label.setPixmap(self.not_found_icon.pixmap(pixmap_size))
        self.older_file_label.setPixmap(self.older_file_icon.pixmap(pixmap_size))
        self.newer_file_label.setPixmap(self.newer_file_icon.pixmap(pixmap_size))

        if current_mission_c is not None:
            palette = self.current_mission_label.palette()
            palette.setColor(QPalette.Background, self.current_mission_color)
            self.current_mission_label.setAutoFillBackground(True)
            self.current_mission_label.setPalette(palette)
        else:
            # Remove current mission help
            while self.curr_mission_layout.count() > 1:
                item = self.curr_mission_layout.takeAt(0)
                item.widget().setParent(None)

        self.pushButton.pressed.connect(self.close)
