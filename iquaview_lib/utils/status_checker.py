# Copyright (c) 2024 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.

""" Functions to check status """

import time

from PyQt5.QtWidgets import QMessageBox

from iquaview_lib.utils.busywidget import BusyWidget, TaskThread


def exec_func_timeout(func, timeout_s, expected_result=True):
    """
    Execute the input function iteratively until the timeout is reached or the function returns true
    :param func: function to evaluate iteratively. Should return true/false
    :param timeout_s: timeout in seconds
    :type timeout_s: int
    :param expected_result: expected result of the function.
    If true, the function will be executed until it returns true
    :type expected_result: boolean
    :return: True if the function returns the expected result, False if the timeout is reached
    """
    start_time = time.time()
    while True:
        res = func()
        time.sleep(0.1)
        if expected_result == res:
            return True
        if time.time() - start_time > timeout_s:
            return False


def show_confirmation_dialog(title, msg, action, action_msg):
    """
    Show a confirmation dialog with a msg, action to call and action msg
    :param title: title to show in the dialog
    :type title: str
    :param msg: Message to display in the dialog.
    :type msg: str
    :param action: Action to call
    :type action: QAction
    :param action_msg: Action message to display in the button
    :type action_msg: str
    :return: return the QMessageBox button role.
    :rtype: QMessageBox.ButtonRule
    """
    dialog = QMessageBox(QMessageBox.Warning, title, msg, QMessageBox.Yes | QMessageBox.No)
    dialog.setDefaultButton(QMessageBox.Yes)
    dialog.button(QMessageBox.No).setText("Close")

    if action:
        dialog.button(QMessageBox.Yes).setText(action_msg)

    return dialog.exec()


def execute_action_and_check_status(title, action, enabling_msg, status_func, res, func, *args):
    """
    Execute action and call BusyWidget to execute status_func
    :param title: title to show in the dialog
    :type title: str
    :param action: Action to call
    :type action: QAction
    :param enabling_msg: Message to display in the BusyWidget
    :type enabling_msg: str
    :param status_func: function to call to get the status
    :type status_func: function
    :param res: expected result
    :type res: bool
    :param func: Function to execute after check status
    :param func: function
    :param args: List of args to pass to fn.
    :type args: list
    :return: True if the mission is executed, False otherwise
    :rtype: bool
    """
    action.trigger()

    if enabling_msg:
        return execute_with_busy_widget(title, enabling_msg, status_func, res, func, *args)
    else:
        return func(*args)


def execute_with_busy_widget(title, enabling_msg, status_func, res, func, *args):
    """
    Execute a function with a busy widget, checking a status function for a specified result.

    :param title: title to show in the dialog
    :type title: str
    :param enabling_msg: Message to be displayed on the busy widget.
    :type enabling_msg: str
    :param status_func: Function to check the status.
    :type status_func: function
    :param res: Expected result from the status function.
    :param func: Function to be executed if the status matches the expected result.
    :type func: function
    :param args: Additional arguments to be passed to the function.
    :return: True if the function is executed successfully, False otherwise.
    :rtype: bool
    """
    busy_widget = BusyWidget(enabling_msg, TaskThread(exec_func_timeout, status_func, 5, res))
    busy_widget.on_start()
    busy_widget.exec()

    if status_func() == res:
        return func(*args)
    else:
        QMessageBox.warning(None, title, f"Error {enabling_msg.lower()}", QMessageBox.Close)
        return False


def check_status(title, msg, status_func, res, func, *args, action=None, action_msg=None, enabling_msg=None) -> bool:
    """
    Checks the status of an action, until the result is the same or the timeout has expired.
    :param title: title to show in the dialog
    :type title: str
    :param msg: message to show in the dialog
    :type msg: str
    :param status_func: function to call to get the status
    :type status_func: function
    :param res: expected result
    :type res: bool
    :param action: (optional) action to trigger
    :type action: QAction
    :param func: Function to execute after check status
    :param func: function
    :param args: List of args to pass to fn.
    :type args: list
    :param action_msg: (optional) message to show near the Yes button
    :type action_msg: str
    :param enabling_msg: (optional) message to show on the busy widget
    :type enabling_msg: str
    :return: True if the mission is executed, False otherwise
    :rtype: bool
    """
    if status_func() != res:
        user_response = show_confirmation_dialog(title, msg, action, action_msg)
        if user_response == QMessageBox.Yes and action:
            return execute_action_and_check_status(title, action, enabling_msg, status_func, res, func, *args)

        elif user_response == QMessageBox.Yes:
            return func(*args)

    return False
