# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.

"""
 Utils to perform QGIS operations
"""

from qgis.core import QgsPointXY, QgsProject, QgsCoordinateTransform


def transform_point(point, crs_source, crs_destination) -> QgsPointXY:
    """
    Transforms a point from one crs to another
    :param point: point to transform
    :type point: QgsPointXY
    :param crs_source: source crs
    :type crs_source: QgsCoordinateReferenceSystem
    :param crs_destination: destination crs
    :type crs_destination: QgsCoordinateReferenceSystem
    :return: transformed point
    :rtype: QgsPointXY
    """
    if crs_source.authid() != crs_destination.authid():
        trans = QgsCoordinateTransform(crs_source,
                                       crs_destination,
                                       QgsProject.instance().transformContext())
        transformed_point = trans.transform(point.x(), point.y())
    else:
        transformed_point = QgsPointXY(point.x(), point.y())

    return transformed_point


def calc_tolerance(width: int, height: int, percentage: float = 0.02) -> float:
    """
    Compute the tolerance on canvas
    :param width: canvas width
    :type width: int
    :param height: canvas height
    :type height: int
    :param percentage: percentage of tolerance
    :type percentage: float
    :return: tolerance
    :rtype: float
    """
    # 2% of tolerance
    width_tolerance = percentage * width
    height_tolerance = percentage * height
    if width_tolerance < height_tolerance:
        tolerance = width_tolerance
    else:
        tolerance = height_tolerance
    return tolerance
