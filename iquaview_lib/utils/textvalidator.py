# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


"""
 Class for validating user input fields
"""

from typing import List

from PyQt5.QtCore import QRegExp
from PyQt5.QtGui import QValidator, QIntValidator, QRegExpValidator
from PyQt5.QtWidgets import QDoubleSpinBox, QLineEdit


def line_edit_validator(line_edit: QLineEdit) -> QValidator.State:
    """
    Validate QLineEdit using its declared QValidator.

    Use in signal QLineEdit.textChanged connection as:
        foo_lineEdit.textChanged.connect(
            lambda: line_edit_validator(foo_lineEdit)
        )

    :param line_edit: LineEdit to validate.
    :type line_edit: QLineEdit
    """
    validator = line_edit.validator()
    state: QValidator.State = validator.validate(line_edit.text(), 0)[0]
    color = get_color(state)
    line_edit.setStyleSheet(f'QLineEdit {{ background-color: {color} }}')
    return state


def validate_line_edits(line_edits: List[QLineEdit]) -> bool:
    """
    Helper function to evaluate multiple LineEdits with their defined validators.

    Useful to validate whole configuration widgets on their self.is_valid() method.

    :param line_edits: List of LineEdits to validate together.
    :type line_edits: List[QLineEdit]
    :return: All succesful at validation or at least one failed.
    :rtype: bool
    """
    for line_edit in line_edits:
        if line_edit.validator().validate(line_edit.text(), 0)[0] != QValidator.Acceptable:
            return False
    return True


def validate_ip(text) -> QValidator.State:
    validator = get_ip_validator()
    state = validator.validate(text, 0)[0]
    return state


def validate_int(text, minimum=None, maximum=None) -> QValidator.State:
    validator = get_int_validator(minimum, maximum)
    state = validator.validate(text, 0)[0]
    return state


def validate_port(text) -> QValidator.State:
    validator = get_port_validator()
    state = validator.validate(text, 0)[0]
    return state


def validate_param_range(text, int_values=False) -> QValidator.State:
    validator = get_param_range_validator(int_values=int_values)
    state = validator.validate(text, 0)[0]
    return state


def validate_custom_double(text) -> QValidator.State:
    validator = get_custom_double_validator()
    state = validator.validate(text, 0)[0]
    return state


def validate_custom_int(text) -> QValidator.State:
    validator = get_custom_int_validator()
    state = validator.validate(text, 0)[0]
    return state


def validate_latitude(text) -> QValidator.State:
    validator = get_latitude_validator()
    state = validator.validate(text, 0)[0]
    return state


def validate_longitude(text) -> QValidator.State:
    validator = get_longitude_validator()
    state = validator.validate(text, 0)[0]
    return state


def validate_extended_latitude(text) -> QValidator.State:
    validator = get_extended_latitude_validator()
    state = validator.validate(text, 0)[0]
    return state


def validate_extended_longitude(text) -> QValidator.State:
    validator = get_extended_longitude_validator()
    state = validator.validate(text, 0)[0]
    return state


def get_color(state: QValidator.State) -> str:
    """
    Get a hexadecimal color according to validator state.

    :param state: State of the validator.
    :type state: QValidator.State
    :return: Color to represent the state.
    :rtype: str
    """
    if state == QValidator.Acceptable:
        return ''  # default theme color (usually white)
    if state == QValidator.Intermediate:
        return '#fff79a'  # yellow
    return '#f6989d'  # red


def get_ip_validator():
    string = r"(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])"
    regexp = QRegExp(string)
    validator = QRegExpValidator(regexp)
    return validator


def get_port_validator():
    string = r"(6553[0-5]|655[0-2][0-9]\d|65[0-4](\d){2}|6[0-4](\d){3}|[1-5](\d){4}|[1-9](\d){0,3})"
    regexp = QRegExp(string)
    validator = QRegExpValidator(regexp)
    return validator


def get_param_range_validator(int_values=False):
    """
    example: 1, 2, 3, 20
    :return:
    """
    if int_values:
        string = r"^[+-]?\d+(\s*,\s*[+-]?\d+)*$"
    else:
        string = r"(^[+-]?\d+)|(^[+-]?\d+[\.]?\d+)(\s*,\s*(([+-]?\d+)|([+-]?\d+[\.]?\d+)))*$"
    regexp = QRegExp(string)
    validator = QRegExpValidator(regexp)
    return validator


def get_int_validator(minimum=None, maximum=None):
    validator = QIntValidator()
    if minimum is not None:
        validator.setBottom(minimum)
    if maximum is not None:
        validator.setTop(maximum)
    return validator


def get_latitude_validator():
    string = r"-?(?:90|((?:0|[1-8][0-9]|[0-9])(?:\.\d+)?(?:[eE][+\-]?\d+)?))"
    regexp = QRegExp(string)
    validator = QRegExpValidator(regexp)
    return validator


def get_longitude_validator():
    string = r"-?(?:180|((?:0|1[1-7][0-9]|[1-9][0-9]|[0-9])(?:\.\d+)?(?:[eE][+\-]?\d+)?))"
    regexp = QRegExp(string)
    validator = QRegExpValidator(regexp)
    return validator


def get_extended_latitude_validator():
    """
    validates whether the latitude is in
    'decimal degrees', 'degrees and minutes' or 'degrees minutes and seconds'
    """
    string = r"^\s*(" \
             r"[+-]?\d{1,3}\s+\d{1,2}(?:\s+|'\s*)\d{1,2}(\'{0,2})?|" \
             r"[+-]?\d{1,3}\s+\d{1,2}(?:\s+|'\s*)\d{1,2}(?:\.\d+)?(\'{0,2})?|" \
             r"[+-]?\d{1,3}\s+\d{1,2}(?:\.\d+)?'?|" \
             r"[+-]?0*(?:90|((?:0|[1-8][0-9]|[0-9])(?:\.\d+)?(?:[eE][+\-]?\d+)?))" \
             r"){1,2}\s*[NSEW]?\s*$"
    regexp = QRegExp(string)
    validator = QRegExpValidator(regexp)
    return validator


def get_extended_longitude_validator():
    """
    validates whether the longitude is in
    'decimal degrees', 'degrees and minutes' or 'degrees minutes and seconds'
    """
    string = r"^\s*(" \
             r"[+-]?\d{1,3}\s+\d{1,2}(?:\s+|'\s*)\d{1,2}(\'{0,2})|" \
             r"[+-]?\d{1,3}\s+\d{1,2}(?:\s+|'\s*)\d{1,2}(?:\.\d+)?(\'{0,2})|" \
             r"[+-]?\d{1,3}\s+\d{1,2}(?:\.\d+)?'?|" \
             r"[+-]?0*(?:180|((?:0|1[1-7][0-9]|[1-9][0-9]|[0-9])(?:\.\d+)?(?:[eE][+\-]?\d+)?))" \
             r"){1,2}\s*[NSEW]?\s*$"
    regexp = QRegExp(string)
    validator = QRegExpValidator(regexp)
    return validator


def get_custom_int_validator(only_positive_numbers=False):
    if only_positive_numbers:
        string = "[+]?\\d*"
    else:
        string = "[+-]?\\d*"
    regexp = QRegExp(string)
    validator = QRegExpValidator(regexp)
    return validator


def get_custom_double_validator():
    string = "[+-]?\\d*[\\.]?\\d+"
    regexp = QRegExp(string)
    validator = QRegExpValidator(regexp)
    return validator


def evaluate_doublespinbox(spinbox: QDoubleSpinBox, text):
    """
    Set spinbox stylesheet depending if value_text is valid
    :param spinbox: spinbox
    :type spinbox: QDoubleSpinBox
    :param value_text: value to evaluate
    :type value_text: str
    :return: qvalidator state
    :rtype: int
    """
    state = validate_custom_double(str(text))
    color = get_color(state)
    spinbox.setStyleSheet(f'QDoubleSpinBox {{ background-color: {color} }}')
    return state


def check_line_edit_starts_by_slash(line_edit: QLineEdit):
    """
    Check if the input text of a line edit starts by /. if it does not it changes it. This assumes that
    :param line_edit: line edit to check
    :type line_edit: QLineEdit
    """
    if not line_edit.text().startswith("/"):
        position = line_edit.cursorPosition()
        line_edit.setText(f"/{line_edit.text()}")
        line_edit.setCursorPosition(position + 1)
