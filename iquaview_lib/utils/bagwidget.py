# -*- coding: utf-8 -*-

# Copyright (c) 2022 Iqua Robotics SL - All Rights Reserved
#
# This file is subject to the terms and conditions defined in file
# 'LICENSE.txt', which is part of this source code package.
"""
Bag widget
"""

import base64
import json

import rosbag
from PyQt5.QtWidgets import QWidget
from PyQt5.QtCore import QEvent

from iquaview_lib.utils import bag_analysis_helper, ordered_set, message_converter
from iquaview_lib.utils.busywidget import BusyWidget, TaskThread
from iquaview_lib.ui.ui_bag_widget import Ui_BagWidget


class BagWidget(QWidget, Ui_BagWidget):

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)
        self.speed_pushButton.clicked.connect(self.change_speed)
        self.speed = 1
        self.slider_clicked = False
        self.field_topic_dict = {}
        self.bag_filename = None
        self.time_horizontalSlider.installEventFilter(self)

    def change_speed(self):
        """
        change the play speed.
        """
        text = self.speed_pushButton.text()
        if text == "x1":
            self.speed = 1
            self.speed_pushButton.setText("x5")
        elif text == "x5":
            self.speed = 5
            self.speed_pushButton.setText("x10")
        elif text == "x10":
            self.speed = 10
            self.speed_pushButton.setText("x100")
        elif text == "x100":
            self.speed = 100
            self.speed_pushButton.setText("x1")

    def open_bag(self):
        """
        Open bag file
        """
        self.bag = rosbag.Bag(self.bag_filename, 'r')

    def close_bag(self):
        """
        Close the bag file. Closing an already closed bag does nothing.
        """
        self.bag.close()

    def load_data(self, bag_filename):
        """
        Given a bag filename. Open the bag and load the topics in a dictionary.
        :param bag_filename: bag name
        :return: Field-topics dict.
        """
        self.bag_filename = bag_filename
        taskthread = TaskThread(self.open_bag)
        bw = BusyWidget(title="Opening bag...", task=taskthread)
        bw.on_start()
        bw.exec_()

        taskthread = TaskThread(self.create_field_topic_dict)
        bw = BusyWidget(title="Reading bag...", task=taskthread)
        bw.on_start()
        bw.exec_()

        topics = ordered_set.OrderedSet()
        for key, value in self.field_topic_dict.items():
            topics.add(value)

        return topics

    def create_field_topic_dict(self):
        """
        Create a dictionary where key is a field of a topic and content is the name of the topic
        :param bag: bag name
        :type bag: str
        """
        self.field_topic_dict.clear()

        topics = self.bag.get_type_and_topic_info()[1].keys()
        # for every topic in the bag read first message to get every field in message
        for topic in topics:
            try:
                # use next to only read first message
                topic, msg, t = next(self.bag.read_messages(topics=[topic]), None)
                msg_dict = message_converter.convert_ros_message_to_dictionary(msg)
                # bytes
                if msg_dict.get('data') is not None and type(msg_dict['data']) is bytes:
                    b64_decoded = base64.b64decode(msg_dict['data'])
                    values = []
                    for v in b64_decoded:
                        values.append(v)
                    msg_dict['data'] = values
                json_dumps = json.dumps(msg_dict, indent=4)
                json_loaded = json.loads(json_dumps)
                bag_analysis_helper.iterate_dictionary(self.field_topic_dict, json_loaded, topic, topic)
                self.field_topic_dict[str(topic + "/t")] = topic
            except Exception as e:
                print("Error reading {}. Exception: {}".format(topic, e))

    def eventFilter(self, obj, event):
        """ Event filter"""
        if (event.type() == QEvent.MouseButtonPress
                and obj == self.time_horizontalSlider):
            self.slider_clicked = True
            minimum = self.time_horizontalSlider.minimum()
            maximum = self.time_horizontalSlider.maximum()
            width = self.time_horizontalSlider.width()
            self.time_horizontalSlider.setValue(minimum + ((maximum - minimum) * event.x()) / width)

        return QWidget.eventFilter(self, obj, event)
