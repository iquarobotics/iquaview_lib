# -*- coding: utf-8 -*-
# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


"""
 Helpers for coordinate conversions
"""
import logging
import re
from geopy import point
from PyQt5.QtGui import QValidator

from enum import IntEnum
from iquaview_lib.utils.textvalidator import validate_extended_latitude, validate_extended_longitude
from iquaview_lib.utils.calcutils import from_scientific_notation

logger = logging.getLogger(__name__)


class CoordinateFormat(IntEnum):
    DEGREE = 0
    DEGREE_MINUTE = 1
    DEGREE_MINUTE_SECOND = 2
    MIN_DECIMAL_DEGREE_PRECISION = 6
    MAX_DECIMAL_DEGREE_PRECISION = 9
    MIN_DECIMAL_DEGREE_MINUTE_PRECISION = 4
    MAX_DECIMAL_DEGREE_MINUTE_PRECISION = 7
    MIN_DECIMAL_DEGREE_MINUTE_SECOND_PRECISION = 3
    MAX_DECIMAL_DEGREE_MINUTE_SECOND_PRECISION = 5

    @staticmethod
    def get_coordinate_format(coordinate_format):
        """
        Given a string, returns coordinate display format in IntEnum. In case it cannot be converted, returns the input.
        :param coordinate_format: coordinate display format to convert.
        :return: IntEnum
        """
        # back compatible
        if coordinate_format == "degree":
            coordinate_format = CoordinateFormat.DEGREE
        elif coordinate_format == "degree_minute":
            coordinate_format = CoordinateFormat.DEGREE_MINUTE
        elif coordinate_format == "degree_minute_second":
            coordinate_format = CoordinateFormat.DEGREE_MINUTE_SECOND

        return coordinate_format


def degree_to_degree_minute(lat, lon):
    """
    Transforms coordinates from DDD.DDDDD (float) to a string of DDD, MM.MM """
    lat_degree = __degree_to_degree_minute_aux__(lat)
    lon_degree = __degree_to_degree_minute_aux__(lon)
    # lat_degree = __splitDegreeMinutes__(lat_degree)
    # lon_degree = __splitDegreeMinutes__(lon_degree)

    return lat_degree, lon_degree


def degree_minute_to_degree(lat, lon):
    """
    Transforms latitude and longitude in the format
            DDD MM.MM to the format DDD.DD
    :param lat: lat is a list, first element is degrees, second element is decimal minutes
    :param lon: lon is a list, first element is degrees, second element is decimal minutes
    :return: return lat and lon in degrees (DDD.DD)
    """
    return float(lat[0]) + float(lat[1]) / 60.0, float(lon[0]) + float(lon[1]) / 60.0


def degree_to_degree_minute_second(lat, lon):
    """
    Transforms coordinates from DDD.DDDDD° (float) to a string of
    DDD°MM'SS.SSS''
    @param lat: latitude
    @type lat: float
    @param lon: longitude
    @type lon: float
    """
    lat_str = __degree_to_degree_minute_second_aux__(lat)
    lon_str = __degree_to_degree_minute_second_aux__(lon)
    return [lat_str, lon_str]


def degree_minute_second_to_degree(lat, lon):
    """
    Transforms coordinates from DDD°MM'SS.SSS'' string to a  of
    DDD.DDDDD° (float)
    @param lat: value in DDD°MM'SS.SSS''
    @type lat: string
    @param lon: value in DDD°MM'SS.SSS''
    @type lon: string
    """
    lat_float = __degree_minute_second_to_degree_aux__(lat)
    lon_float = __degree_minute_second_to_degree_aux__(lon)
    return lat_float, lon_float


def __degree_minute_second_to_degree_aux__(value):
    """
    Transforms coordinates from DDD°MM'SS.SSS'' string to a  of
    DDD.DDDDD° (float)
    @param value: value in DDD°MM'SS.SSS''
    @type value: string
    """
    deg, min_sec = str(value).split('°')
    min_sec, rest = min_sec.split('\'\'')
    minute, sec = min_sec.split('\'')

    dd = float(deg) + float(minute) / 60 + float(sec) / (60 * 60)
    return dd


def __degree_to_degree_minute_second_aux__(value):
    """
    Transforms coordinates from DDD.DDDDD° (float) to a string of
    DDD°MM'SS.SSS''
    @param value: value in DDD.DDDDD
    @type value: float
    """
    d = int(value)
    t = (value - d) * 60
    m = int(t)
    s = (t - m) * 60
    return f"{d:d}° {m:d}' {s:f}''"


def __degree_to_degree_minute_aux__(value):
    val = str(value).split('.')
    if val[0] == '':  # for cases like ".25" instead of "0.25"
        val[0] = 0
    if len(val) > 1:
        minute = float('0.' + val[1]) * 60.0
    else:
        minute = 0.0
    if minute < 10.0:
        return f"{int(val[0]):d}° 0{minute:f}'"
    else:
        return f"{int(val[0]):d}° {minute:f}'"


def __split_degree_minutes__(value):
    """ Transform DDDMM.MM to DDD, MM.MM """
    val = str(value).split('.')
    val_min = val[0][-2] + val[0][-1] + '.' + val[1]
    val_deg = ''
    for i in range(len(val[0]) - 2):
        val_deg = val_deg + val[0][i]

    return int(val_deg), float(val_min)


def format_lat_lon(latitude: float,
                   longitude: float,
                   coordinate_format: CoordinateFormat,
                   precision: int,
                   directional_suffix: bool,
                   display_symbols: bool):
    """
    Transforms coordinates from DDD.DDDDD° (float) to a string of
    the desired format (degrees, degrees minutes, or degrees minutes seconds).

    :param latitude: Latitude in decimal degrees.
    :type latitude: float
    :param longitude: Longitude in decimal degrees.
    :type longitude: float
    :param coordinate_format: Desired coordinate format.
        - CoordinateFormat.DEGREES: Decimal degrees (DDD.DDDDD°).
        - CoordinateFormat.DEGREE_MINUTE: Degrees and decimal minutes (DDD° MM.MMM').
        - CoordinateFormat.DEGREE_MINUTE_SECOND: Degrees, minutes, and decimal seconds (DDD° MM' SS.S").
    :type coordinate_format: CoordinateFormat
    :param precision: Number of decimal places for minutes and seconds.
    :type precision: int
    :param directional_suffix: Whether to include directional suffix ('N', 'S', 'E', 'W') at the end of the coordinates.
    :type directional_suffix: bool
    :param display_symbols: Whether to display degree (°), minute ('), and second (") symbols.
    :type display_symbols: bool

    :return: Formatted coordinates as a string.
    :rtype: str
    """
    ew_str = "E"
    if longitude < 0:
        ew_str = "W"
        longitude = -longitude
    ns_str = "N"
    if latitude < 0:
        ns_str = "S"
        latitude = -latitude
    if coordinate_format == CoordinateFormat.DEGREE_MINUTE:
        longitude_deg = int(longitude)
        latitude_deg = int(latitude)
        longitude_min = (longitude - longitude_deg) * 60
        latitude_min = (latitude - latitude_deg) * 60
        longitude = f"{longitude_deg:03d}° {longitude_min:0{precision+3}.{precision}f}'"
        latitude = f"{latitude_deg:02d}° {latitude_min:0{precision+3}.{precision}f}'"
    elif coordinate_format == CoordinateFormat.DEGREE_MINUTE_SECOND:
        longitude_deg = int(longitude)
        latitude_deg = int(latitude)
        longitude_min = int((longitude - longitude_deg) * 60)
        latitude_min = int((latitude - latitude_deg) * 60)
        longitude_sec = (longitude - longitude_deg - longitude_min / 60) * 3600
        latitude_sec = (latitude - latitude_deg - latitude_min / 60) * 3600
        longitude = f"{longitude_deg:03d}° {longitude_min:02d}' {longitude_sec:0{precision+3}.{precision}f}\""
        latitude = f"{latitude_deg:02d}° {latitude_min:02d}' {latitude_sec:0{precision+3}.{precision}f}\""
    else:
        longitude = f"{longitude:03.{precision}f}°"
        latitude = f"{latitude:02.{precision}f}°"

    if not display_symbols:
        symbol_to_remove = ['°', '\'', '\"', ' ']
        for symbol in symbol_to_remove:
            longitude = longitude.replace(symbol, '')
            latitude = latitude.replace(symbol, '')

    if directional_suffix:
        return [f"{str(latitude)}{ns_str}", f"{str(longitude)}{ew_str}"]
    else:
        is_west = "-" if ew_str == "W" else ""
        is_south = "-" if ns_str == "S" else ""
        return [f"{is_south}{str(latitude)}", f"{is_west}{str(longitude)}"]


def parse_latitude(lat):
    """
    Parse latitude in various formats with North (N) or South (S) at the end and return the result.

    :param lat: Latitude input in different formats.
    :return: Parsed latitude value.
    """
    # Remove any leading or trailing whitespaces
    lat_stripped = lat.strip()

    # Define regular expressions for different latitude formats
    lat_dd_mm_ss_pattern = re.compile(r'^-?(\d{1,2})(\d{2})(\d{2}\.?\d*)\s*[NS]?$')
    lat_dd_mm_pattern = re.compile(r'^-?(\d{1,2})(\d{2}\.?\d*)\s*[NS]?$')
    lat_dd_dddd_pattern = re.compile(r'^-?(\d{1,2}\.?\d*)\s*[NS]?$')

    # Extract the direction (N/S) if present
    direction = lat_stripped[-1] if lat_stripped and lat_stripped[-1] in ('N', 'S') else None
    lat_stripped = lat_stripped[:-1] if direction else lat_stripped

    # Check and parse latitude based on the format
    if lat_dd_mm_ss_pattern.match(lat_stripped):
        # Latitude format: DDMMSS.SSSS N/S
        groups = lat_dd_mm_ss_pattern.match(lat_stripped).groups()
        degrees = int(groups[0])
        minutes = int(groups[1])
        seconds = float(groups[2])
        parsed_latitude = degrees + minutes / 60.0 + seconds / 3600.0
    elif lat_dd_mm_pattern.match(lat_stripped):
        # Latitude format: DDMM.MMMM N/S
        groups = lat_dd_mm_pattern.match(lat_stripped).groups()
        degrees = int(groups[0])
        minutes = float(groups[1])
        parsed_latitude = degrees + minutes / 60.0
    elif lat_dd_dddd_pattern.match(lat_stripped):
        # Latitude format: D.DDDD N/S
        parsed_latitude = float(lat_stripped)
        # Adjust latitude based on the direction
        if direction == 'S':
            parsed_latitude *= -1
        return parsed_latitude
    else:
        # Handle invalid input or format
        raise ValueError("Invalid latitude format")

    # Adjust latitude based on the direction
    if direction == 'S' or (direction is None and lat_stripped.startswith('-')):
        parsed_latitude *= -1

    return parsed_latitude


def parse_longitude(lon):
    """
    Parse longitude in various formats with East (E) or West (W) at the end and return the result.

    :param lon: Longitude input in different formats.
    :return: Parsed longitude value.
    """
    # Remove any leading or trailing whitespaces
    lon_stripped = lon.strip()

    # Define regular expressions for different longitude formats
    lon_dd_mm_ss_pattern = re.compile(r'^-?(\d{2,3})(\d{2})(\d{2}\.?\d*)\s*[EW]?$')
    lon_dd_mm_pattern = re.compile(r'^-?(\d{2,3})(\d{2}\.?\d*)\s*[EW]?$')
    lon_dd_dddd_pattern = re.compile(r'^-?(\d{1,3}\.?\d*)\s*[EW]?$')

    # Extract the direction (E/W) if present
    direction = lon_stripped[-1] if lon_stripped and lon_stripped[-1] in ('E', 'W') else None
    lon_stripped = lon_stripped[:-1] if direction else lon_stripped

    # Check and parse longitude based on the format
    if lon_dd_mm_ss_pattern.match(lon_stripped):
        # Longitude format: DDMMSS.SSSS E/W
        groups = lon_dd_mm_ss_pattern.match(lon_stripped).groups()
        degrees = int(groups[0])
        minutes = int(groups[1])
        seconds = float(groups[2])
        parsed_longitude = degrees + minutes / 60.0 + seconds / 3600.0
    elif lon_dd_mm_pattern.match(lon_stripped):
        # Longitude format: DDMM.MMMM E/W
        groups = lon_dd_mm_pattern.match(lon_stripped).groups()
        degrees = int(groups[0])
        minutes = float(groups[1])
        parsed_longitude = degrees + minutes / 60.0
    elif lon_dd_dddd_pattern.match(lon_stripped):
        # Longitude format: D.DDDD E/W
        parsed_longitude = float(lon_stripped)
        if direction == 'W':
            parsed_longitude *= -1
        return parsed_longitude
    else:
        # Handle invalid input or format
        raise ValueError("Invalid longitude format")

    # Adjust longitude based on the direction
    if direction == 'W' or (direction is None and lon_stripped.startswith('-')):
        parsed_longitude *= -1

    return parsed_longitude


def convert_latitude_to_decimal_degrees(latitude: str) -> float:
    """
    Given a latitude in string format, try to convert to decimal degrees as a float type
    :param latitude: latitude in string format
    :type latitude: str
    :return:latitude
    :rtype: float
    """
    if latitude.count('º') > 1 or latitude.count('°') > 1:
        raise Exception(f"Invalid latitude format {latitude}")

    # replace 'º' and '°' with ' '
    latitude_filtered = latitude.replace("º", " ")
    latitude_filtered = latitude_filtered.replace("°", " ")
    latitude_filtered = latitude_filtered.replace("\"", "''")

    latitude_stripped = latitude_filtered.strip()
    split_lat = latitude_stripped.split()

    if len(split_lat) == 1:
        # Handle case with no minutes or seconds, just the decimal degrees and direction
        lat = str(parse_latitude(latitude_stripped))
    elif (len(split_lat) == 2
          and ((split_lat[1] == 'N' or split_lat[1] == 'S') and len(split_lat[1]) == 1)):
        # Handle case where direction 'N' or 'S' is included but no explicit minutes or seconds
        lat = str(parse_latitude(latitude_stripped))
    else:
        # Split lat into parts to add missing symbols if needed
        direction = None
        if latitude_stripped[-1] in ['N', 'S']:
            direction = latitude_stripped[-1]  # Extract the direction
            latitude_stripped = latitude_stripped[:-1]
        split_lat = latitude_stripped.split()

        if len(split_lat) > 1 and "'" not in split_lat[1]:
            split_lat[1] = split_lat[1] + "'"
        if len(split_lat) > 2 and "''" not in split_lat[2]:
            split_lat[2] = split_lat[2] + "''"
        lat = " ".join(split_lat)
        if direction:
            lat += " " + direction

    # validate coordinates
    state_lat = validate_extended_latitude(lat)
    if state_lat != QValidator.Acceptable:
        raise Exception(f"Invalid latitude format {latitude}")

    try:
        point_lat_lon = point.Point(f"{lat}, {0}")
    except Exception as e:
        # try to convert to decimal degrees
        logger.warning(f"{e}")
        lat = from_scientific_notation(lat)
        point_lat_lon = point.Point(f"{lat}, {0}")
    return point_lat_lon.latitude


def convert_longitude_to_decimal_degrees(longitude: str) -> float:
    """
    Given a longitude in string format, try to convert to decimal degrees as a float type.
    :param longitude: longitude in string format
    :type longitude: str
    :return: longitude
    :rtype: float
    """
    if longitude.count('º') > 1 or longitude.count('°') > 1:
        raise Exception(f"Invalid longitude format {longitude}")

    # replace 'º' and '°' with ' '
    longitude_filtered = longitude.replace("º", " ")
    longitude_filtered = longitude_filtered.replace("°", " ")
    longitude_filtered = longitude_filtered.replace("\"", "''")

    longitude_stripped = longitude_filtered.strip()
    split_lon = longitude_stripped.split()

    if len(split_lon) == 1:
        # Handle case with no minutes or seconds, just the decimal degrees and direction
        lon = str(parse_longitude(longitude_stripped))
    elif (len(split_lon) == 2
          and ((split_lon[1] == 'E' or split_lon[1] == 'W') and len(split_lon[1]) == 1)):
        # Handle case where direction 'E' or 'W' is included but no explicit minutes or seconds
        lon = str(parse_longitude(longitude_stripped))
    else:
        # Split lon into parts to add missing symbols if needed
        direction = None
        if longitude_stripped[-1] in ['E', 'W']:
            direction = longitude_stripped[-1]  # Extract the direction
            longitude_stripped = longitude_stripped[:-1]
        split_lon = longitude_stripped.split()

        if len(split_lon) > 1 and "'" not in split_lon[1]:
            split_lon[1] = split_lon[1] + "'"
        if len(split_lon) > 2 and "''" not in split_lon[2]:
            split_lon[2] = split_lon[2] + "''"
        lon = " ".join(split_lon)
        if direction:
            lon += " " + direction

    # validate coordinates
    state_lon = validate_extended_longitude(lon)
    if state_lon != QValidator.Acceptable:
        raise Exception(f"Invalid longitude format {longitude}")

    try:
        point_lat_lon = point.Point(f"{0}, {lon}")
    except Exception as e:
        # try to convert to decimal degrees
        logger.warning(f"{e}")
        lon = from_scientific_notation(lon)
        point_lat_lon = point.Point(f"{0}, {lon}")

    return point_lat_lon.longitude
