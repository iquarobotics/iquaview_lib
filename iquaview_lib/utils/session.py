from typing import Optional
import datetime

class SessionTime:
    """
    Maintain a date format to be used by all files that log data for the whole IQUAview session.
    """
    data: Optional[str] = None

    @staticmethod
    def get() -> str:
        if SessionTime.data is None:
            SessionTime.data = datetime.datetime.utcnow().strftime("%Y%m%d_%H%M%S")
        return SessionTime.data