from typing import Any, Callable
from PyQt5.QtCore import pyqtSignal


def reconnect_signal_to_slot(signal: pyqtSignal, callback: Callable[[Any], None]) -> None:
    """
    Helper to connect signals making sure that no duplicated connections exist.

    :param signal: Signal to connect to.
    :type signal: pyqtSignal
    :param callback: Function to be called.
    :type callback: Callable[[Any], None]
    """
    try:
        signal.disconnect(callback)
    except TypeError:
        # the connection did not exist previously
        pass
    signal.connect(callback)
