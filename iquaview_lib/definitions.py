# -*- coding: utf-8 -*-

# Copyright (c) 2023 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


ROSBRIDGE_SERVER_PORT = 9091
WGS84_EPSG_ID = 4326
