# Copyright (c) 2022 Iqua Robotics SL
#
# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.

"""
 Definitions of the configuration parameters for the entity drawers style.
"""

import sys
import math
from random import random

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QWidget

if sys.version_info >= (3, 8):
    from typing import TypedDict
else:
    from typing_extensions import TypedDict


class CanvasDrawerStyle:

    def check_dict(self, config_dict: TypedDict) -> bool:
        """
        Check if the provided dictionary corresponds to the one of the class.
        """
        raise NotImplementedError()

    def from_dict(self, config_dict: TypedDict) -> bool:
        raise NotImplementedError()

    def to_dict(self) -> TypedDict:
        raise NotImplementedError()

    def config_widget(self, parent: QWidget = None) -> QWidget:
        raise NotImplementedError()


class CanvasDrawerStyleConfigWidget(QWidget):

    def __init__(self, parent: QWidget = None) -> None:
        super().__init__(parent)

    @staticmethod
    def get_icon() -> QIcon:
        """
        Get icon
        :return: widget icon
        :rtype: QIcon
        """
        return QIcon(":/resources/mActionStyles.svg")

    @staticmethod
    def get_random_color() -> str:
        """
        Get a random color in hexadecimal format
        :return: random color
        :rtype: str
        """
        letters = '0123456789ABCDEF'
        color = '#'
        for i in range(0, 6):
            color += letters[math.floor(random() * 16)]
        return color

    def set_configuration(self, style: CanvasDrawerStyle) -> None:
        raise NotImplementedError()

    def get_configuration(self) -> CanvasDrawerStyle:
        raise NotImplementedError()

    def is_valid(self) -> bool:
        raise NotImplementedError()
