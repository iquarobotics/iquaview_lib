#!/usr/bin/env python
"""
Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.
"""

import sys
from dataclasses import dataclass
from enum import Enum
from typing import Optional, Tuple, Dict, Any

if sys.version_info >= (3, 8):
    from typing import Literal, TypedDict
else:
    from typing_extensions import Literal, TypedDict


class RegisterResponse(Enum):
    """ Response to a register request. """
    NewRegistration = 1
    AlreadyRegistered = 2
    OverwrittenRegistration = 3
    NotRegisteredGotNone = 4


@dataclass
class WebServerRegister:
    """ Register request for the web server. """
    name: str
    hex_color: str  # TODO: generate svg with proper color and save name
    has_orientation: bool
    # labels: Optional[List[str]] = None
    icon_detail_name: Optional[Literal["girona500", "sparus2", "vessel"]] = None  # vessel positioning?
    icon_detail_size: Optional[Tuple[float, float]] = None


class DataSource(TypedDict):
    """ Data source for the web server. """
    register_data: WebServerRegister
    icon: Tuple[str, Tuple[float, float]]  # simple round icon
    icon_detail: Tuple[str, Tuple[float, float]]  # specific detailed icon (or =icon_path if not available)
    color: str
    last_position: Optional[Tuple[float, float]]


@dataclass
class WebServerPoint:
    """ Point data for the web server. """
    name: str
    connected: bool
    latitude: float
    longitude: float
    orientation_deg: Optional[float] = None
    labels: Optional[Dict[str, str]] = None

    def to_dict(self) -> Dict[str, Any]:
        """ Convert to dictionary. """
        return {
            'name': self.name,
            'latitude': self.latitude,
            'longitude': self.longitude,
            'orientation': self.orientation_deg,
            'labels': self.labels,
        }
