"""
Copyright (c) 2020 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.
"""

import logging
from typing import Any, Dict
from PyQt5.QtWidgets import QWidget
from PyQt5.QtCore import QObject, pyqtSignal

logger = logging.getLogger(__name__)


class DriverBase(QObject):
    """
    Base class for a driver
    """

    error_signal = pyqtSignal(str)  # Will disconnect the entity
    ok_signal = pyqtSignal()  # Sent when all works correctly. Resets the warnings in the list
    warning_signal = pyqtSignal([str], [str, bool])  # Will add[/delete] a warning to the list of warnings.

    def connect(self) -> None:
        """Connect driver"""
        raise NotImplementedError

    def close(self) -> None:
        """Close the device"""
        raise NotImplementedError

    def get_configuration_widget(self) -> QWidget:
        """Return the configuration widget"""
        raise NotImplementedError

    def set_configuration(self, configuration: Dict[str, Any]) -> None:
        """Set the configuration"""
        raise NotImplementedError
