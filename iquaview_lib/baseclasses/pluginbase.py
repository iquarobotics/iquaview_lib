#!/usr/bin/env python
# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.


"""
    Base class for an iquaview plugin
"""
import logging
from importlib import util

from PyQt5.QtCore import QObject

logger = logging.getLogger(__name__)


class PluginBase(QObject):
    """
    PluginBase is the base class for all iquaview plugins. It defines the basic interface for a plugin.
    """

    def __init__(self, module_name: str = "empty", parent=None, **kwargs) -> None:
        super().__init__(parent)
        self.module_name = module_name
        self.module_found = util.find_spec(module_name) is not None

    @staticmethod
    def version() -> str:
        """Return the version of the plugin"""
        raise NotImplementedError

    @property
    def module_name(self) -> str:
        """
        :return: return module name
        :rtype: str
        """
        return self.__module_name

    @module_name.setter
    def module_name(self, module_name: str) -> None:
        """
        Set module name
        :param module_name: new module name
        :type module_name: str
        """
        self.__module_name = module_name

    def is_module_found(self) -> bool:
        """Return True if module is found in system, otherwise False"""
        return self.module_found

    def disconnect_module(self) -> None:
        """Disconnect module"""
        raise NotImplementedError

    def remove(self) -> None:
        """Remove module"""
        raise NotImplementedError
