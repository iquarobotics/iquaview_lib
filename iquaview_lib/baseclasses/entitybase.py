# Copyright (c) 2022 Iqua Robotics SL

# Distributed under the terms of the Modified BSD License (BSD 3-clause)
# see 'LICENSE.txt', which is part of this source code package.

import logging
from copy import copy
from dataclasses import dataclass
from typing import Dict, List, Optional, TextIO, Tuple, Union
import os

from PyQt5.QtCore import pyqtSignal, pyqtSlot, QObject, Qt, QSize
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QDialog, QVBoxLayout, QWidget, QMessageBox, QDialogButtonBox, QStackedWidget
from qgis.gui import QgsMapCanvas

from iquaview_lib.baseclasses.canvasdrawerstyle import CanvasDrawerStyleConfigWidget
from iquaview_lib.baseclasses.driverbase import DriverBase
from iquaview_lib.baseclasses.webserverbase import WebServerRegister
from iquaview_lib.canvasdrawer.canvasmarker import CanvasMarker
from iquaview_lib.canvasdrawer.canvastrack import CanvasTrack
from iquaview_lib.config import Config
from iquaview_lib.utils.session import SessionTime
from iquaview_lib.ui.ui_entity_options_widget import Ui_EntityOptionsWidget

ICON_SIZE = 32
FOLDER_LOGS_ENTITIES = os.path.expanduser("~/logs/iquaview/entities")
os.makedirs(FOLDER_LOGS_ENTITIES, exist_ok=True)

logger = logging.getLogger(__name__)


@dataclass
class LabelData:
    text: str
    color: QColor = QColor("black")
    label_name: Optional[str] = None

    def __ne__(self, other: "LabelData") -> bool:
        return (self.text, self.color) != (
            other.text,
            other.color,
        )


class EntityBase(QObject):
    entity_updated_signal = pyqtSignal([], [bool])  # bool: True if config changed
    entity_remove_signal = pyqtSignal()
    entity_save_signal = pyqtSignal(dict, Ui_EntityOptionsWidget)

    PROPERTIES_DRIVER = "driver"
    PROPERTIES_STYLE = "style"

    def __init__(self, **kwargs) -> None:
        parent: Optional[QObject] = kwargs["parent"] if "parent" in kwargs else None
        super().__init__(parent)
        kwargs.setdefault("permanent", None)

        self.dialog: Optional[QDialog] = None
        self.warnings_list: List[str] = []
        self.connection_failed_displayed: bool = False
        self.connected: bool = False

        self.canvas: QgsMapCanvas = kwargs["canvas"]
        self.config: Config = kwargs["config"]
        self.connect_when_starts: bool = kwargs["connect_when_starts"]
        self.driver: DriverBase = kwargs["driver"]
        self.name: str = kwargs["name"]
        self.permanent: str = kwargs["permanent"]
        self.visible: bool = kwargs["visible"]

        if "canvasdrawer" in kwargs:
            self.canvasdrawer: Union[CanvasTrack, CanvasMarker] = kwargs["canvasdrawer"]
        else:
            self.canvasdrawer = CanvasTrack(
                canvas=self.canvas,
                style=kwargs["style"],
                marker=CanvasMarker(self.canvas, kwargs["style"], orientation=kwargs.get("orientation", True)),
                parent=parent,
            )

        if issubclass(type(self.driver), QObject):
            self.driver.warning_signal[str].connect(self.set_warning)
            self.driver.warning_signal[str, bool].connect(self.set_warning)
            self.driver.error_signal.connect(self.connection_failed)
            self.driver.ok_signal.connect(self.clear_list)

        # info shown for the entity (key is the label and the tuple contains text and color)
        self.info_labels: Dict[str, LabelData] = {}

        # self.timer.timeout.connect(self.update_canvas)
        self.identifier: int = kwargs["id"]
        self.position: int = kwargs["position"]

        # data logging to csv
        self.datalog_headers: List[str] = kwargs["datalog_headers"] if "datalog_headers" in kwargs else []
        self.datalog_fh: Optional[TextIO] = None
        if self.datalog_headers:
            self.datalog_fh = self._get_datalog_fh_and_write_header(self.datalog_headers)

    def _get_datalog_fh_and_write_header(self, headers: List[str]) -> TextIO:
        filename = f"{SessionTime.get()}_{self.name.replace(' ', '_')}.csv"
        path = os.path.join(FOLDER_LOGS_ENTITIES, filename)
        fh = open(path, "w")
        fh.write(f"{','.join(headers)}\n")
        return fh

    def on_name_changed(self) -> None:
        if self.datalog_fh is not None:
            # close log and open with new name
            self.datalog_fh.close()
            self.datalog_fh = self._get_datalog_fh_and_write_header(self.datalog_headers)

    def datalog_record(self, values: List[str]) -> None:
        # check
        if not self.datalog_headers:
            logger.error("cannot record values with undefined headers")
            return
        elif len(self.datalog_headers) != len(values):
            logger.error(f"record values ({len(values)}) != headers ({len(self.datalog_headers)})")
            return
        elif self.datalog_fh is None:
            logger.fatal("datalog file handler is non-existing")
            return
        # record
        self.datalog_fh.write(f"{','.join(values)}\n")
        self.datalog_fh.flush()

    def set_label(
        self, identifier: str, label: Union[str, None] = None, value: str = "-", color: QColor = QColor("black")
    ) -> None:
        """Deprecated function, use set_label_data()"""
        self.set_label_data(
            identifier=identifier,
            label_data=LabelData(
                label_name=label,
                text=value,
                color=color,
            ),
        )

    def _save_label_data(self, identifier: str, label_data: LabelData) -> None:
        """Helper functions used by declare_label_data() and set_label_data()."""
        assert label_data.label_name is not None
        self.info_labels[identifier] = label_data
        self.entity_updated_signal.emit()

    def declare_label_data(self, identifier: str, label_data: LabelData) -> None:
        """Use in constructor, declare a new label for the entity."""
        self._save_label_data(identifier=identifier, label_data=label_data)

    def set_label_data(self, identifier: str, label_data: LabelData) -> None:
        """Change value of an already existing label of the entity."""
        if identifier in self.info_labels:
            if label_data != self.info_labels[identifier]:
                if label_data.label_name is None:
                    # reuse previous title
                    label_data.label_name = self.info_labels[identifier].label_name
                self._save_label_data(identifier=identifier, label_data=label_data)

    @property
    def identifier(self) -> int:
        """
        :return: return identifier
        :rtype: int
        """
        return self.__identifier

    @identifier.setter
    def identifier(self, identifier: int) -> None:
        """
        Set identifier
        :param identifier: new identifier
        :type identifier: int
        """
        self.__identifier = identifier

    @property
    def position(self) -> int:
        """
        :return: return position
        :rtype: int
        """
        return self.__position

    @position.setter
    def position(self, position: int) -> None:
        """
        Set position
        :param position: new position
        :type position: int
        """
        self.canvasdrawer.update_z_value(position)

        self.__position = position

    def is_connected(self) -> bool:
        return self.connected

    def warnings(self) -> List[str]:
        """Returns a list of driver warning messages. If all is OK, it returns an empty list."""
        return self.warnings_list

    def is_locked(self) -> bool:
        return self.canvasdrawer.locked

    def clear_list(self) -> None:
        """Empty the warnings list"""
        if self.warnings_list:
            self.warnings_list = []
            self.entity_updated_signal.emit()

    @pyqtSlot(str)
    @pyqtSlot(str, bool)
    def set_warning(self, warning: str, add: bool = True) -> None:
        """
        Add/remove warning from warning list.
        i.e. "Parsing line failed"
        """
        if add:
            if warning not in self.warnings_list:
                self.warnings_list.append(warning)
                self.entity_updated_signal.emit()
        else:
            if warning in self.warnings_list:
                self.warnings_list.remove(warning)
                self.entity_updated_signal.emit()

    def connection_failed(self, error: str) -> None:
        """
        Close entity.
        i.e. "Connection with Driver could not be established"
        """
        self.close()
        logger.error(error)
        if not self.connection_failed_displayed:
            self.connection_failed_displayed = True
            message_box = QMessageBox(
                QMessageBox.Warning,
                "Connection Failed",
                error,
                QMessageBox.Close,
                None,
            )
            message_box.setWindowModality(Qt.WindowModal)
            message_box.exec_()
            self.connection_failed_displayed = False

    def zoom_track(self) -> None:
        self.canvasdrawer.center_to_location()

    def clear_track(self) -> None:
        self.canvasdrawer.clear_track()

    def lock_track(self) -> None:
        self.canvasdrawer.lock_location()
        self.entity_updated_signal.emit()

    def export_track(self) -> None:
        self.canvasdrawer.export_track()

    def hide(self) -> None:
        self.canvasdrawer.hide()

    def show(self) -> None:
        self.canvasdrawer.show(self.is_connected)

    def edit(
        self, is_new_entity: bool = False, widget_to_view: Optional[QWidget] = None, parent: Optional[QObject] = None
    ) -> None:
        if self.dialog is None:
            self.dialog = QDialog(parent)
            self.dialog.setWindowTitle(self.name)
            vertical_layout = QVBoxLayout(self.dialog)
            self.dialog.setLayout(vertical_layout)

            entity_options_widget = QWidget()
            ui = Ui_EntityOptionsWidget()
            ui.saved = False
            ui.setupUi(entity_options_widget)
            ui.mOptionsListWidget.currentRowChanged.connect(
                lambda: self.set_current_page(ui.mOptionsListWidget.currentRow(), ui.mOptionsStackedWidget)
            )
            ui.mOptionsListWidget.setIconSize(QSize(ICON_SIZE, ICON_SIZE))

            vertical_layout.addWidget(entity_options_widget)

            properties = self.properties_widgets()
            for idx, item in enumerate(properties.items()):
                widget = item[1]
                ui.mOptionsListWidget.addItem(widget.windowTitle())
                ui.mOptionsListWidget.item(idx).setIcon(widget.get_icon())
                ui.mOptionsStackedWidget.addWidget(widget)
                if widget == widget_to_view:
                    ui.mOptionsStackedWidget.setCurrentWidget(widget)
                    ui.mOptionsListWidget.setCurrentRow(ui.mOptionsStackedWidget.currentIndex())

            ui.buttonBox.button(QDialogButtonBox.Close).clicked.connect(self.dialog.reject)
            if is_new_entity:
                self.dialog.rejected.connect(lambda: self.remove_if_not_saved(ui))
            ui.buttonBox.button(QDialogButtonBox.Save).clicked.connect(
                lambda state, x=properties: self.entity_save_signal.emit(x, ui)
            )
            self.dialog.rejected.connect(self.close_dialog)
            ui.buttonBox.button(QDialogButtonBox.Save).clicked.connect(self.close_dialog)

            self.dialog.show()

        self.dialog.activateWindow()

    def close_dialog(self) -> None:
        """Handles closing/rejecting of the entity's dialog"""
        self.dialog = None

    def remove_if_not_saved(self, ui: Ui_EntityOptionsWidget) -> None:
        """
        checks if the option widget settings have been previously saved
        :param ui: options widget
        :return: None
        """
        if not ui.saved:
            self.entity_remove_signal.emit()

    def set_current_page(self, item: int, stacked_widget: QStackedWidget) -> None:
        """
        Change item in Stacked widget
        :param item: position of the page
        :type item: int
        :param stacked_widget: stacked widget
        :type stacked_widget: QStackedWidget
        """
        stacked_widget.setCurrentIndex(item)

    def connect(self) -> None:
        pass

    def close(self) -> None:
        self.close_dialog()
        pass

    def open_configuration(self) -> None:
        pass

    def properties_widgets(self) -> Dict[str, QWidget]:
        pass

    def is_valid_data(self, driver_widget: QWidget, style_widget: QWidget) -> None:
        pass

    def remove(self) -> None:
        """Remove entity from configuration settings"""
        self.close_dialog()
        pass

    def save_basic_changes(self) -> None:
        """
        Save basic changes to config
        """
        if not self.config.settings["input_tracks"]:
            self.config.settings["input_tracks"] = {}

        if not self.config.settings["input_tracks"].get(self.position):
            self.config.settings["input_tracks"][self.position] = {}

        if not self.config.settings["input_tracks"][self.position].get("driver"):
            self.config.settings["input_tracks"][self.position]["driver"] = {}

        self.config.settings["input_tracks"][self.position]["name"] = copy(self.name)
        self.config.settings["input_tracks"][self.position]["id"] = copy(self.identifier)
        self.config.settings["input_tracks"][self.position]["visible"] = copy(self.visible)
        self.config.settings["input_tracks"][self.position]["permanent"] = copy(self.permanent)
        self.config.settings["input_tracks"][self.position]["connect_when_starts"] = copy(self.connect_when_starts)
        self.config.settings["input_tracks"][self.position]["canvasdrawer"] = self.canvasdrawer.to_dict()
        self.config.settings["input_tracks"][self.position]["driver"] = self.driver.to_dict()

        self.config.save()

        self.entity_updated_signal.emit()
        self.entity_updated_signal[bool].emit(True)

    def save_changes(self, widgets: Dict[str, QWidget]) -> bool:
        """
        Saves the changes made to the new configuration
        :param widgets: dictionary containing all configuration widgets
        :type widgets: Dict[str, QWidget]
        :return: return True if saved correctly, false otherwise
        :rtype: bool
        """
        try:
            self.close_dialog()
            saved = False
            config_style_widget: Optional[CanvasDrawerStyleConfigWidget] = widgets.get(self.PROPERTIES_STYLE)
            if config_style_widget is not None:
                self.canvasdrawer.set_style(config_style_widget.get_configuration())

            self.save_basic_changes()

            saved = True
        except Exception as e:
            logger.error("Entity not saved: {}".format(e))

        return saved

    def get_information(self, tag: str) -> LabelData:
        return self.info_labels[tag]

    def information_fields(self) -> List[Tuple[str, LabelData]]:
        return self.info_labels.items()

    def web_server_register(self) -> WebServerRegister:
        """
        Returns web server register
        :return: web server register
        :rtype: WebServerRegister
        """
        return self.canvasdrawer.webserver_register(self.name)
