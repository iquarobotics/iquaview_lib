# Changelog

All notable changes to iquaview lib are documented in this file.

## [24.1.4] - 2024-11-29

### Added

* Show chrony status if "chrony tracking" topic is in the configuration xml.

### Changed

* Updated measure tools SVGs.

## [24.1.3] - 2024-09-17

### Fixed

* Added missing end-of-line '\r\n' to the UDP socket in the data output connection.

## [24.1.2] - 2024-08-26

### Fixed

* Typo in log folder name for entities.

## [24.1.1] - 2024-02-22

### Added

* Common datetime definition for IQUAview session logs.

### Fixed

* Avoid closing entity log file on disconnection.
* Library pyOpenSSL>=24.0.0 required due to incompatibility between pyOpenSSL<=19.0.0 and cryptography>38.0.0 libraries.

## [24.1.0] - 2024-01-30

### Added

* Base class to configure the track appearance named CanvasDrawer.
* Functions to check if navigation is valid.
* Functions to convert coordinates to the specified coordinate display format.
* Functions to check the status of an action. For example to check the position keeping or enable thrusters before starting a mission.
* Move MissionStatus from iquaview to iquaview_lib as VehicleStatus.
* Possibility to log entities to disk in a csv. Implemented for vessel and gnss entities.

### Fixed

* Minor bugs.

## [22.10.2] - 2023-08-02

### Fixed

* CustomAcousticIOServer: bug on recv action.

## [22.10.1] - 2022-11-09

### Fixed

* Displays the specific error on the screen if an error occurs when saving the parameters of an entity.
* CustomAcousticIOServer: avoid adding newline character(\n) if base64 encoding is used.

## [22.10.0] - 2022-10-26

### Added

* Usbl sound velocity as a config parameter.
* GNSS: dead reckoning accepted.
* CustomAcousticIOServer is used to support the IOServer.
* The connection of the main antenna can be checked.

### Changed

* A structure of entities was created to organise the tracks.
* vessel position system inserted in the vessel entity.
* Entities are not displayed if they are not initialised.
* In the menu of the AUV configuration parameters, "Save" has been renamed "Apply & Save", to clarify the action to be taken.

### Removed

* IMU Port option on connection settings.
* Removed vessel shortcut (ctrl+v) because it caused problems when copying and pasteing.

### Fixed

* Minor bugs.

## [20.10.6] - 2021-11-19

### Fixed

* The actions to be called in the xml.

## [20.10.5] - 2021-09-06

### Added

* Support for Iqua Strobe Lights.

## [20.10.4] - 2021-05-31

### Added

* Support for Norbit WBMS Multibeam.

## [20.10.3] - 2021-04-08

### Fixed

* Increased the timeout in the method: send trigger service.

## [20.10.2] - 2021-03-30

### Fixed

* Some cola2 interface functions return a dictionary instead of the result as a boolean.

## [20.10.1] - 2021-03-26

### Added

* The timeout parameter added in some cola2 interface methods.

## [20.10.0] - 2020-10-23

* First Release version.
